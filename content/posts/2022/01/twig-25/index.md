---
title: "#25 The Big 1.0"
author: Felix
date: 2022-01-07
tags: ["gnome-clocks", "fragments", "libadwaita", "gnome-podcasts", "tracker"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from December 31 to January 07.<!--more-->
We wish everyone a Happy New Year 🎆! The new year starts right with a "Big 1.0", Libadwaita - an important cornerstone for GNOME apps, had its first stable release!

# Core Apps and Libraries

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) reports

> Libadwaita 1.0.0 has been released. Check out [the announcement](https://blogs.gnome.org/alexm/2021/12/31/libadwaita-1-0/)

### Tracker [↗](https://gitlab.gnome.org/GNOME/tracker/)

A filesystem indexer, metadata storage system and search tool.

[Sam Thursfield](https://matrix.to/#/@ssssam:matrix.org) says

> In Tracker Miners, there are big performance improvements thanks to Carlos Garnacho. The way that we generate identifiers for file contents is changing, for more information see: https://discourse.gnome.org/t/tracker-3-3-will-reindex-files-and-change-content-ids/8583

[Sam Thursfield](https://matrix.to/#/@ssssam:matrix.org) reports

> Also in Tracker Miners, there is a new filesystem monitoring backend using FANotify. This requires some kernel features recently added in Linux. FANotify scales to monitoring large filesystems much more easily.

### GNOME Clocks [↗](https://gitlab.gnome.org/GNOME/gnome-clocks)

A simple clock app which includes world clocks, alarms, a stopwatch and a timer.

[Maximiliano](https://matrix.to/#/@msandova:gnome.org) reports

> @yetizone and I ported Clocks to GTK 4 and libadwaita, featuring a modern style and dark mode support just in time for GNOME 42.
> ![](7ce15763b3eb10403ca54b5cc7953b1bb983ee45.png)

# Circle Apps and Libraries

### Podcasts [↗](https://gitlab.gnome.org/podcasts)

Podcast app for GNOME.

[nee](https://matrix.to/#/@nee:tchncs.de) says

> [Podcasts](https://wiki.gnome.org/Apps/Podcasts) [v0.5.1](https://gitlab.gnome.org/World/podcasts/-/tags/0.5.1) was released on [flathub](https://www.flathub.org/apps/details/org.gnome.Podcasts). The new 0.5 version includes:
> * a way to display the episode description
> * episodes will now resume playback where you last stopped
> * phones are now prevented from falling asleep during playback
> * a bug that could cause the phosh lock screen to lag multiple seconds was fixed
> * and a bunch of other small improvements in usability and fixes for some broken downloads
> 
> On another note there is a merge request for a [gtk4 port of Podcasts](https://gitlab.gnome.org/World/podcasts/-/merge_requests/199) that is currently being tested.
> ![](2d7e30b5317be14dc8c9d98f0abec0c60f65c52f.png)

### Fragments [↗](https://gitlab.gnome.org/World/Fragments)

Easy to use BitTorrent client.

[Felix](https://matrix.to/#/@felix:haecker.io) announces

> Fragments gained the ability for configuring a custom port, and can test if the port got opened/forwarded correctly.
> ![](WdhrFAZTBJVhALdxRBUmArvu.png)

# Third Party Projects

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) reports

> [flatpak-vscode](https://github.com/bilelmoussaoui/flatpak-vscode) got a new release with support of more of flatpak-builder options which makes it capable of building projects that makes uses of SDK extensions like llvm12. It's available on both [Open VSX Registry](https://open-vsx.org/extension/bilelmoussaoui/flatpak-vscode) and [Microsoft Visual Studio Code Marketplace](https://marketplace.visualstudio.com/items?itemName=bilelmoussaoui.flatpak-vscode).

# Miscellaneous

[Chris 🌱️](https://matrix.to/#/@brainblasted:gnome.org) announces

> The TWIG website now has styling for dark mode! If your browser has `prefers-color-scheme` set to dark, TWIG will detect that and use a new dark look.
> ![](f5c26313696d5456f154bc29d0bf020328bc498c.png)
> ![](6b979d0d7d144ae39ca7893a1e5a95b9c904442d.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
