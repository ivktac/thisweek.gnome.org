---
title: "#26 Contact Me"
author: Felix
date: 2022-01-14
tags: ["gtk-rs", "gaphor", "commit", "mutter", "gnome-contacts", "phosh", "gnome-builder", "fragments", "secrets", "gjs"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from January 07 to January 14.<!--more-->

# Core Apps and Libraries

### GNOME Contacts [↗](https://gitlab.gnome.org/GNOME/gnome-contacts)

Keep and organize your contacts information.

[nielsdg](https://matrix.to/#/@nielsdg:gnome.org) says

> GNOME Contacts has been ported to GTK 4 and libadwaita, making sure it nicely fits in with a lot of other core apps in GNOME 42.

### Mutter [↗](https://gitlab.gnome.org/GNOME/mutter/)

A Wayland display server and X11 window manager and compositor library.

[robert.mader](https://matrix.to/#/@robert.mader:gnome.org) says

> Thanks to Jonas Ådahl we now support the new Wayland dmabuf feedback protocol. The protocol (for communication between clients and Mutter) together with some improvements to Mutters native backend (communication between Mutter and the kernel) allows a number of optimizations. In Gnome 42 for example, this will allow us to use direct scanout with most fullscreen OpenGL or Vulkan clients. Something we already supported in recent versions, however only in very selective cases. You can think of this as a more sophisticated version of X11 unredirect, notably without tearing.
> What does this mean for users? The obvious part is that it will squeeze some more FPS out of GPUs when running games. To me, the even more important part is that it will help reduce energy consumption and thus increase battery life for e.g. video players. When playing a fullscreen video, doing a full size extra copy of every frame takes up a significant part of GPU time and skipping that allows the hardware to clock down.
> What does this mean for developers? Fortunately, support for this protocol is build into OpenGL and Vulkan drivers. I personally spent a good chunk of time over the last two years helping to make Firefox finally use OpenGL by default. Now I'm very pleased to get this efficiency boost for free. Similarly, if you consider porting your app from GTK3 to GTK4 (the later using OpenGL by default) this might be a further incentive to do so.
> What next? In future versions of Gnome we plan to support scanout for non-fullscreen windows. Also, users with multi-GPU devices can expect to benefit significantly from further improvements.
> ![](ff27a4b3f220d9cf35915add230f86e13ca69bd6.png)

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) reports

> [Builder](https://gitlab.gnome.org/GNOME/gnome-builder/-/merge_requests/491) and [Logs](https://gitlab.gnome.org/GNOME/gnome-logs/-/merge_requests/33) now support the upcoming dark style preference.

### GJS [↗](https://gitlab.gnome.org/GNOME/gjs)

Use the GNOME platform libraries in your JavaScript programs. GJS powers GNOME Shell, Polari, GNOME Documents, and many other apps.

[ptomato](https://matrix.to/#/@ptomato:gnome.org) announces

> In GJS this week:
> * Evan Welsh made GObject interfaces enumerable, so you can now do things like `Object.keys(Gio.File.prototype)` and get a list of the methods, like you can with other GObject types.
> * Evan also fixed a memory leak with callbacks.
> * Marco Trevisan and myself landed a large refactor involving type safety.
> * Chun-wei Fan kept everything buildable on Windows.
> * Thanks to Sonny Piers, Sergei Trofimovich, and Eli Schwartz for various other contributions.

### Cantarell [↗](http://cantarell.gnome.org)

[Jakub Steiner](https://matrix.to/#/@jimmac:gnome.org) says

> GNOME's UI typeface, Cantarell, has gotten a new minisite at [cantarell.gnome.org](http://cantarell.gnome.org). We finally have a canonical place for the font binary downloads, but the site also demos the extensive weight coverage of the variable font. I'm happy the typeface now has a respectable home for the amount of work Nikolaus Waxweiler has poured into it in the past few years. Thank you!
> ![](b8a533d088eaee7ad65b77ddcb0220a9ca14c0d1.png)

# Circle Apps and Libraries

### Secrets [↗](https://gitlab.gnome.org/World/secrets)

A password manager which makes use of the KeePass v.4 format.

[Maximiliano](https://matrix.to/#/@msandova:gnome.org) says

> Secrets, formerly known as Password Safe, version 6.0 was just released, featuring the recent GTK 4 port, libadwaita, and OTP support. Due to the rename now it is under [org.gnome.World.Secrets](https://flathub.org/apps/details/org.gnome.World.Secrets) in Flathub.
> ![](9d099c33a4b7aa2b77c50cec933b7379645a661f.png)

### gtk-rs [↗](https://gtk-rs.org/)

Safe bindings to the Rust language for fundamental libraries from the GNOME stack.

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) announces

> gtk4-rs has now a Windows MSVC CI pipeline. This will ensure the bindings builds just fine and avoid regressions for Windows users that want to build applications using GTK4 and Rust.

### Gaphor [↗](https://gaphor.org)

A simple UML and SysML modeling tool.

[Arjan](https://matrix.to/#/@amolenaar:matrix.org) announces

> In our upcoming release [Gaphor](https://gaphor.org), based on popular demand, we now support diagram types! If you create an activity diagram, for example, it adds diagram info to the upper left of the diagram and collapses the toolbox to only show the relevant tools for that diagram.
> ![](DQHsfQVZBklDLTgaktkyOnuI.png)

### Fragments [↗](https://gitlab.gnome.org/World/Fragments)

Easy to use BitTorrent client.

[Felix](https://matrix.to/#/@felix:haecker.io) announces

> I added context menus to Fragments to make common actions easier and faster to perform. These are primarily intended for desktop users, but can also be activated on touchscreens by long pressing and holding.
> ![](BXpyCZPItfpBxWvUITwFMGio.png)

### Commit [↗](https://github.com/sonnyp/Commit)

An editor that helps you write better Git and Mercurial commit messages.

[sonnyp](https://matrix.to/#/@sonnyp:matrix.org) announces

> [Commit message editor](https://apps.gnome.org/app/re.sonny.Commit/) now use GtkSourceView which allows for new features and improvements. It's also now [available to translate on Weblate](https://hosted.weblate.org/engage/commit/).
> ![](JHctHlBdscsvjjQVpNOuUBAU.png)

# Third Party Projects

[sonnyp](https://matrix.to/#/@sonnyp:matrix.org) announces

> Tobias Bernard and I started working on [Playhouse](https://github.com/sonnyp/Playhouse) an HTML/CSS/JavaScript playground for GNOME.
> 
> There is no release yet but contributions and feedback welcome.
> 
> Powered by GTK 4, GJS, libadwaita , GtkSourceView and WebKitGTK !
> ![](HrtNKYonTHOwfmzImlbBVibT.png)

[Corentin Noël](https://matrix.to/#/@tintou:matrix.org) announces

> We are happy to announce the first public alpha release of libshumate, the GTK4 Map widget library [announced in 2019](https://ml4711.blogspot.com/2019/01/starting-on-new-map-rendering-library.html). This first unstable release contains everything one need to embed a minimal Map view. This library completely replaces libchamplain which was using Clutter and now provides a native way to control maps in GTK4. Application developers are encouraged to use libshumate and report any issue that might occur or any missing feature to the library.
> ![](QeZORyapIXZlzHUtOjGjyQFd.png)

[flxzt](https://matrix.to/#/@followhollows:matrix.org) announces

> I have been working on it for a while, but now it is ready for an announcement: Rnote is a vector-based drawing app to create handwritten notes and to annotate pictures and PDFs. It features an endless sheet, different pen types with stylus pressure support, shapes and tools. It also has an integrated workspace browser and you can choose between different background colors and patterns. It can be downloaded as a flatpak [from flathub](https://flathub.org/apps/details/com.github.flxzt.rnote)
> ![](dzdHvBjDjfuuKowZVTdXsYqt.png)

[dabrain34](https://matrix.to/#/@dabrain34:matrix.org) announces

> GstPipelineStudio aims to provide a graphical user interface to the GStreamer framework. From a first step in the framework with a simple pipeline to a complex pipeline debugging, the tool provides a friendly interface to add elements to a pipeline and debug it.
> ![](UytQyKDMvTspTNWwuHYCnZRL.png)

### Phosh [↗](https://gitlab.gnome.org/World/Phosh/phosh)

A pure wayland shell for mobile devices.

[Guido](https://matrix.to/#/@guido.gunther:talk.puri.sm) says

> Panzer Sajt added support for non-numeric passwords to phosh. Some bits of Sam Hewitt's ongoing style refresh is also already visible in the video as is the new VPN indicator in the top-bar:
> {{< video src="jXQDGfuhXPDsQQfVKqrFHDFs.mp4" >}}

# Documentation

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) announces

> I merged the initial batch of [beginner tutorials](https://developer.gnome.org/documentation/tutorials/beginners/components.html) for the GNOME Developer Documentation website. They are meant to be used as a bridge between the HIG and API references, providing useful information about UI elements with code examples in multiple programming languages. More to come in the future!

# Miscellaneous

[Sophie Herold](https://matrix.to/#/@sophieherold:gnome.org) announces

> The app pages on [apps.gnome.org](https://apps.gnome.org) are now coming with a more exciting header design. Further, page rendering times have been optimized and a few issues with right-to-left scripts have been fixed. The latter surfaced with the newly added Hebrew translation.
> ![](706fbec9437cba6f3cb24a55e25444b438c90486.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

