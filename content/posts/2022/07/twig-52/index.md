---
title: "#52 Happy Birthday!"
author: Felix
date: 2022-07-15
tags: ["bottles", "libadwaita", "glib", "dialect", "crosswords", "gnome-builder", "vala", "nautilus", "gtk", "gjs", "gaphor", "geary"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from July 08 to July 15.<!--more-->

![](twig.png)

I am pleased to announce that TWIG is having its first anniversary! 52 weeks ago, 52 TWIG issues later, or to put it simply - one year ago I [launched TWIG](https://blogs.gnome.org/haeckerfelix/2021/07/16/introducing-this-week-in-gnome/)!

The first issue was published on July 16, 2021, and was named [_"#1 Scrolling in the Dark"_](https://thisweek.gnome.org/posts/2021/07/twig-1/).
After discussing the concept with some other GNOME contributors, we had to search for "news" in order to have enough material for the first issue.

Now, one year later, it gives me great pleasure to announce that we have set a new record for news on the anniversary. There has never been that many news as this week in the whole year!
I am cautiously optimistic, and claim the concept has now become established in the GNOME community, and is being used by more and more projects to quickly and easily announce news. 

What do you think about TWIG? Do you have any suggestions for improvements? Criticism? Compliments? Let us know! You can contact us in our Matrix [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) room anytime!

But now to the actual news - Next year is sure to be at least as exciting!

# GNOME Foundation

[Thib](https://matrix.to/#/@thib:ergaster.org) reports

> GUADEC is already happening! You can find [the complete timetable here](https://events.gnome.org/event/77/timetable/#20220720) and most information on https://guadec.org
> 
> This year the event is hybrid: some of us are lucky enough to be able to make it to Guadalajara, but for those who aren’t, the talks will be broadcasted online as well.
> 
> Register on https://guadec.org to get all the details on how to join the event remotely.
>
> ![](guadec.jpg)

# Core Apps and Libraries

### Files [↗](https://gitlab.gnome.org/GNOME/nautilus)

Providing a simple and integrated way of managing your files and browsing your file system.

[antoniof](https://matrix.to/#/@antoniof:gnome.org) reports

> Files 43.alpha has been released, [with the largest change set for an alpha release since version 3.29.90](https://gitlab.gnome.org/GNOME/nautilus/-/commit/3bb9c98d705571749e25ba933311b6237c19fc07)
>
> This is the first fully functional GTK4-based development release. Notably, drag-and-drop works again, courtesy of Corey Berla!
> ![](nautilus.png)

[Chris 🌱️](https://matrix.to/#/@brainblasted:gnome.org) reports

> Nautilus now uses AdwFlap for the sidebar. At small sizes the sidebar automatically hides, and a button to show it appears. The sidebar can also be shown and hidden with a swipe on touchscreens.
> {{< video src="8ec3fb6b380feb2a55f73ca1d62358f0c1842869.mp4" >}}

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Chris 🌱️](https://matrix.to/#/@brainblasted:gnome.org) says

> with the merging of AdwAboutWindow, I've submitted MRs to port multiple apps to the new window:
> 
> * Disk Usage Analyzer
> * Characters
> * Text Editor
> * Weather
> * Fonts
> * Files
> * Calendar
> * Logs
> * Music
> * Clocks
> * Calculator
> * Extensions
> 
> Alexander has also submitted a port for Contacts.
> ![](3f90873016c836288ae9ee81ca56151abdec7c74.png)

### GTK [↗](https://gitlab.gnome.org/GNOME/gtk)

Cross-platform widget toolkit for creating graphical user interfaces.

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) reports

> GTK 4.7.1 has been released! This is a new development snapshot towards the 4.8 release, which will be available in GNOME 43. A few of the latest changes:
> 
> * a new text widget, [GtkInscription](https://docs.gtk.org/gtk4/class.Inscription.html), which can be used inside list views and wherever you want the text to respond to the UI layout instead of the other way around
> * performance improvements for GtkListView, with culling of out of view rows
> * support for fractional [letter spacing in CSS](https://developer.mozilla.org/en-US/docs/Web/CSS/letter-spacing)
> * improvements in the accessibility of GtkStack and GtkTextView
> * improved touchpad support on Windows
> * multiple input fixes when using Wayland

### GNOME Builder [↗](https://gitlab.gnome.org/GNOME/gnome-builder)

IDE for writing GNOME-based software.

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) says

> Thanks to the massive amounts of work of Christian Hergert, Builder was ported to GTK4 and libadwaita. This port was merged this week, and the first unstable release with it (43.alpha0) was published. This version of Builder has a different app-id, `org.gnome.Builder.Devel`, and therefore needs to be manually installed. Some of the highlights include:
> 
>  * New tabbed editor which uses traditional tabs instead of Builder's document stack
>  * A new status bar at the bottom with contextual information, such as the git branch, language syntax options, and others
>  * Dark and light styles
>  * Improved flow for creating new projects
>  * Support for many other run options when running applications with Valgrind
>  * Deeper integration with the Sysprof profiler
>  * Run applications with specific accessibility settings, such as high contrast
>  * Reorganizable panels thanks to libpanel
>  * More powerful shortcuts management
>  * Command editor to add custom run commands to the pipeline
> 
> ... and a lot more. Many previously available features are still in the works, and anyone interested can help pushing Builder to the finish line by picking any task at [the GTK4 port checklist](https://gitlab.gnome.org/GNOME/gnome-builder/-/issues/1675).
> 
> Thanks again to Christian Hergert for this exciting and impressive work on Builder!
> ![](5cca47ee8df9a0ee6830e76163c92f174a200b3a.png)
> ![](e67a75862ff1fb89ef777140304f457f911d462f.jpg)
> ![](f20608da915bea36a89998f34d8ca0888dd09e95.png)
> ![](6ef3bea5d7cb054a98df1b5a5a3541e138400a13.png)
> ![](097ec2cdbcd23cb1b27d5af91f6bee44c678e37a.png)

### Vala [↗](https://gitlab.gnome.org/GNOME/vala)

An object-oriented programming language with a self-hosting compiler that generates C code and uses the GObject system

[colinkiama](https://matrix.to/#/@itsmune:matrix.org) announces

> This week, the new Vala website has been released: https://vala.dev
> 
> This will be the new starting point for newcomers to the language and hub for Vala-related resources across the web.
> 
> Also, we are currently working on a [new documentation website](https://lwildberg.pages.gitlab.gnome.org/vala-tutorial/). There is still quite a bit missing, so contributors are appreciated!
> 
> Furthermore, the Vala Reference Manual has been ported to a new format [here](https://lwildberg.pages.gitlab.gnome.org/vala-reference-manual/). The source code of both is easily accessible through [gitlab](https://gitlab.gnome.org/lwildberg/vala-tutorial).
> 
> Eventually all these will replace the current Vala wiki pages.
> ![](yyNAGpAvWHHzCzbZQghlbgnT.png)
> ![](vala2.png)
> ![](vala1.png)

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) says

> GLib has [ported from libpcre to libpcre2](https://gitlab.gnome.org/GNOME/glib/-/merge_requests/2529), which is a huge amount of work done by Aleksei Rybalkin. It shouldn’t cause any behaviour changes for `GRegex` users, though.

### GJS [↗](https://gitlab.gnome.org/GNOME/gjs)

Use the GNOME platform libraries in your JavaScript programs. GJS powers GNOME Shell, Polari, GNOME Documents, and many other apps.

[ptomato](https://matrix.to/#/@ptomato:gnome.org) says

> In GJS 1.73.1, Nasah Kuma landed a more intelligent output display for the interactive interpreter, which pretty-prints objects' properties and values based on their type. This improvement also applies to the `log()` and `logError()` functions.

[ptomato](https://matrix.to/#/@ptomato:gnome.org) says

> Also in GJS 1.73.1, thanks to Sergio Costas, DBus proxy classes now include methods named with the suffix `Async`, which perform async calls to DBus APIs and return Promises. This is in addition to the existing suffixes `Sync` (for blocking calls) and `Remote` (for async calls with callbacks.)

[ptomato](https://matrix.to/#/@ptomato:gnome.org) reports

> Another improvement from GJS 1.73.1 from Sonny Piers is an override for `Gio.ActionMap.prototype.add_action_entries()`. Previously this method wouldn't work because it required an array of `Gio.ActionEntry` objects, which are not possible to construct in GJS. Now it can be used with an array of plain objects. (e.g. `this.add_action_entries([{name: 'open', activate() { ... }}]);`

# Circle Apps and Libraries

[Sophie](https://matrix.to/#/@sophieherold:gnome.org) reports

> This week, [Citations](https://apps.gnome.org/app/org.gnome.World.Citations/) joined GNOME Circle. Citations allows you to manage your bibliographies using the BibTeX format. Congratulations!
> ![](d209954a014ac38aa342a5484967b778a8f529bc.png)

### Gaphor [↗](https://gaphor.org)

A simple UML and SysML modeling tool.

[danyeaw](https://matrix.to/#/@Yeaw:matrix.org) announces

> Version 2.11.0 of Gaphor, the simple modeling tool, is out! It adds support for adding elements to diagrams using double click, union types, SysML Enumerations as ValueTypes, and numerous bug fixes. We also greatly improved compatibility with GTK4, and we expect the next release will shift it to be the default version of GTK.

### Dialect [↗](https://github.com/dialect-app/dialect/)

Translate between languages.

[Rafael Mardojai CM](https://matrix.to/#/@rafaelmardojai:matrix.org) announces

> Dialect 2.0.0 has been released! You can get it from [Flathub](https://flathub.org/apps/details/app.drey.Dialect).
> 
> It features the following changes:
> *   Ported to GTK4 and libadwaita
> *   New in-app color scheme switcher
> *   Added Lingva Translate support
> *   Google Translate module rewritten from scratch to not rely on external libraries and improve reliability.
> *   GNOME Search Provider improvements
> *   APP ID changed to app.drey.Dialect
> *   Added API keys support for LibreTranslate
> *   Added translation suggestions support for LibreTranslate
> *   Fixed proxies by rewriting the http backend
> *   Character limit is now service-dependent
> *   Major code base refactoring
> ![](gnIqsUiPiinYLOfjrfDGrSKw.png)

# Third Party Projects

[Vojtěch Perník](https://matrix.to/#/@pervoj:matrix.org) announces

> The first version of the word guessing game [Blurble](https://flathub.org/apps/details/app.drey.Blurble) was released this week! Blurble is a GTK clone of Wordle written in Vala and made with localization in mind.
> ![](HArizHCARkmncNGhrVulHKdK.png)

[Chris 🌱️](https://matrix.to/#/@brainblasted:gnome.org) says

> Loupe has been updated to follow new mockups from Allan Day.
> ![](bd2d8b52f83b8226b3cbbf14438fb4791b0cebaa.png)

### Geary [↗](https://wiki.gnome.org/Apps/Geary)

Send and receive email.

[nielsdg](https://matrix.to/#/@nielsdg:gnome.org) says

> Geary is getting some love again, thanks to Cédric Bellegarde (gnumdk) stepping up to co-maintain it together with me

### Crosswords [↗](https://gitlab.gnome.org/jrb/crosswords)

A simple Crossword player and Editor.

[jrb](https://matrix.to/#/@jblandford:matrix.org) says

> Crosswords 0.3.3 was [released](https://blogs.gnome.org/jrb/2022/07/11/crosswords-0-3-3-double-dutch/). This is a simple Crossword game for GNOME. New in this version:
> * A preferences dialog to filter puzzle sets by language
> * Fully marked for translation.
>     * Dutch and Spanish translations are added
> * Dutch-language crosswords work with the 'IJ' cell
> * It doesn't grab focus when clicking on a row
> * Copy/Paste support
> * Undo/Redo support
> * Numerous bug fixes
> * Use the new libadwaita About dialog
> * Fixes to build and run on MacsOS
> 
> [Download on flathub](https://flathub.org/apps/details/org.gnome.Crosswords)
> ![](bqFhGikxCDTCLZqOvsaBReZj.png)

### Bottles [↗](https://usebottles.com/)

Easily run Windows software on Linux with Bottles!

[Hari Rana (TheEvilSkeleton)](https://matrix.to/#/@theevilskeleton:fedora.im) reports

> Bottles 2022.7.14 was released! We introduce a new Wine runner called Soda, which will be supported by the Bottles team. The aforementioned runner is based on Valve's Wine, with the inclusion of patches from Proton, TKG and GE.
> 
> We also bring in new installers UI to make the installation process prettier and more fun.
> 
> With that out of the way, here are all the changes available in the release page: https://usebottles.com/blog/release-2022.7.14 !
> ![](1c0b47ae5d7ac97892d954cf452d82ce2ef08f28.png)
> ![](1793aa7079c6701cc466a7614bf1c40eaa27b9cf.png)

[Hari Rana (TheEvilSkeleton)](https://matrix.to/#/@theevilskeleton:fedora.im) says

> Bottles was ported to AdwAboutWindow thanks to [axtlos](https://axtloss.github.io/)!
> ![](55503e99a3980eff0204d7915cc27511b6930c29.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

