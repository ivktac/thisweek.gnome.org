---
title: "#59 Beautiful Calls"
author: Felix
date: 2022-09-02
tags: ["pika-backup", "gnome-calls", "geopard", "phosh", "gradience", "komikku", "gdm-settings", "amberol"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from August 26 to September 02.<!--more-->

# Core Apps and Libraries

### Calls [↗](https://gitlab.gnome.org/GNOME/calls)

A phone dialer and call handler.

[Evangelos](https://matrix.to/#/@evangelos.tzaras:talk.puri.sm) says

> Sam Hewitt has redesigned the call display. The redesign prevents parts of the window overflowing when button labels extended to two lines (as happens in some locales).
> Apart from fixing the overflow it is also really beautiful \o/
> ![](qCEOnlrieymhJpGYjInCifSG.png)
> ![](qQWYZKuOwFjsESqThUqHqlYI.png)

# Circle Apps and Libraries

### Pika Backup [↗](https://apps.gnome.org/app/org.gnome.World.PikaBackup/)

Keep your data safe.

[Sophie](https://matrix.to/#/@sophieherold:gnome.org) announces

> Pika Backup now helps with excluding files that are unnecessary to save or too large for backups. The new dialog also provides the option to exclude a single file instead of a complete folder.
> ![](df55431039cd8b4b7309aad6d692ad47daf2db5c.png)

### Amberol [↗](https://gitlab.gnome.org/ebassi/amberol/)

Plays music, and nothing else.

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) reports

> [Amberol 0.9.1 is out](https://gitlab.gnome.org/World/amberol/-/releases/0.9.1), and available of Flathub!
> * Support for ReplayGain metadata in the audio files; Amberol allows you to automatically follow the volume recommendation for track and album if the metadata is available
> * Support for external cover art files in the same directory as a song
> * Shuffling now is more reliable, and adding songs to a shuffled playlist will not scramble the existing order
> * Lots of smaller UI paper cuts, metadata loading fixes, and translation updates
> ![](5140752b62160e1a8d5d85c94a0a66cb4d4211df.png)

# Third Party Projects

### Phosh [↗](https://gitlab.gnome.org/World/Phosh/phosh)

A pure wayland shell for mobile devices.

[Guido](https://matrix.to/#/@guido.gunther:talk.puri.sm) says

> Phosh 0.21.0 and Phoc 0.21.1 are out with lots of bug fixes and experimental support for lock screen widgets like upcoming events:
> ![](IlcfjqNQsLYfZoIEmtMvNcyz.png)
> ![](ZehnNnEOAVIwXjmRlrGysMlw.png)

### Komikku [↗](https://gitlab.com/valos/Komikku)

A manga reader for GNOME.

[Valéry Febvre (valos)](https://matrix.to/#/@valos:matrix.org) says

> After several months of effort, [Komikku](https://gitlab.com/valos/Komikku)'s porting to GTK4 and libadwaita is coming to an end.
> 
> Brave people can install the Flatpak beta version available in the [flathub-beta](https://flathub.org/beta-repo/flathub-beta.flatpakrepo) remote.
> 
> The highlights of this new major version:
> * Refreshing of the UI to follow the GNOME HIG as much as possible
> * Library has now two display modes: Grid and Compact grid
> * Faster display of the chapters list, whether there are few or many chapters
> * Full rewriting of the Webtoon reading mode
> * Modern ‘About’ window
> 
> Many thanks to Tobias Bernard for his many design tips.
> ![](SDbGkEUaSJrAreCBnscwDmBT.png)

### Gradience [↗](https://github.com/GradienceTeam/Gradience)

Change the look of Adwaita, with ease.

[Daudix UFO](https://matrix.to/#/@daudix_ufo:matrix.org) reports

> After more than a month of developement, we happy to announce that Gradience is now available on [Flathub](https://flathub.org/apps/details/com.github.GradienceTeam.Gradience) 🎉
> 
> Thanks to all contributers who maked it real 🚀
> 
> Some changes in v0.2.0 - v0.2.2
> 
> * App published on Flathub (We submitted it at moment of publication last TWIG)
> * Added perferences window for managing flatpak ovverides
> * Added backup functionality for gtk.css to prevent loss of users current settings
> * Various UI improvements
> 
> You can download v0.2.0 from [Flathub](https://flathub.org/apps/details/com.github.GradienceTeam.Gradience), or test newest versions by downloading [Nightly build](https://github.com/GradienceTeam/Gradience/actions/workflows/flatpak.yml)

### Geopard [↗](https://github.com/ranfdev/Geopard)

A Gemini client

[ranfdev](https://matrix.to/#/@ranfdev:matrix.org) reports

> A new [Geopard](https://flathub.org/apps/details/com.ranfdev.Geopard) release is ready!
> Geopard is a colorful gemini browser.
> In this release:
> * Added ability to reload the current page
> * Nicer list items formatting
> * Fixed annoying bug where selecting the text would sometimes transform a paragraph into a title, temporarily
> * Fixed crash when the app theme is overridden
> * Fixed unresponsiveness when a big page is loading
> * Complete rewrite of the gemini parser to make it more robust and improve handling of edge cases
> 
> Get it while it's hot, from [flathub](https://flathub.org/apps/details/com.ranfdev.Geopard)

### Login Manager Settings [↗](https://realmazharhussain.github.io/gdm-settings/)

A settings app for the login manager GDM.

[Mazhar Hussain](https://matrix.to/#/@realmazharhussain:matrix.org) announces

> [Login Manager Settings](https://realmazharhussain.github.io/gdm-settings) v1.0 beta is now available on [flathub-beta](https://flathub.org/beta-repo/flathub-beta.flatpakrepo) remote.

# Documentation

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) announces

> A new section has appeared in the GNOME Developers Documentation website: [useful tools](https://developer.gnome.org/documentation/tools.html) for GNOME contributors and application developers! Learn how to use Valgrind, Massif, and Sysprof, and if you have additional tools, feel free to contribute a page with documentation and examples.

# Miscellaneous

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) announces

> The GNOME implementation of the file chooser portal now remembers the last folder used by an app. This is a small improvement, but its impact and usefulness is disproportionate!
> {{< video src="06ac8d035c2fe220f4c273a4ee40b19a3d584062.mp4" >}}

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
