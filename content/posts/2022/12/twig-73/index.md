---
title: "#73 Removing Autotools"
author: Felix
date: 2022-12-09
tags: ["glib", "gnome-software", "gtk"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from December 02 to December 09.<!--more-->

# Core Apps and Libraries

### GTK [↗](https://gitlab.gnome.org/GNOME/gtk)

Cross-platform widget toolkit for creating graphical user interfaces.

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) announces

> After four years, the GTK maintainers are removing the Autotools build from the GTK 3.x branch; if you want to build or package GTK 3.x you will now have to use the Meson build system. The documentation has been updated accordingly. The resulting build artifacts have been checked for consistency, and the Meson build has been tested on different platforms and toolchains, but if you are experiencing regressions make sure to file an issue on the GTK issue tracker.

### Software [↗](https://gitlab.gnome.org/GNOME/gnome-software/)

Lets you install and update applications and system extensions.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) reports

> Adrien Plazas has been working hard at replacing uses of deprecated GTK APIs in gnome-software with modern replacements from GTK and libadwaita

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) announces

> Marco Trevisan has been carefully digging through complex interactions between different types of refcounting in `GObject`, and adding a lot of tests as he goes

# Third Party Projects

[Hari Rana (TheEvilSkeleton)](https://matrix.to/#/@theevilskeleton:fedora.im) announces

> Upscaler 1.1.0 was released! This release adds and changes a couple of features:
> 
> * Replace Upscaling dialog with page
> * Add Open With functionality
> * Improve appstream
> * Check algorithm output in case of failure
> * Add percentage
> * Improved icon
> * Suggest file name when selecting output location
> * Rename "Open File" to "Open Image" for consistency
> 
> With all of this, we also received a lot of translation!
> {{< video src="399f50c70a51d6563c5b13c6084e19ef2f7e7573.mp4" >}}

# Documentation

[Rafael Mardojai CM](https://matrix.to/#/@rafaelmardojai:matrix.org) says

> I have started working on a "[PyGObject Guide](https://gitlab.gnome.org/rafaelmardojai/pygobject-guide)". It has started as a port of the well known "Python GTK+ 3 Tutorial" to GTK4, with some restructuring and extending.
> 
> This is the perfect time if anybody wants to jump in to help or give feedback.
> 
> You can look the current state of the guide [here](https://rafaelmardojai.pages.gitlab.gnome.org/pygobject-guide/).

# Miscellaneous

[Sam Thursfield](https://matrix.to/#/@ssssam:matrix.org) reports

> We are running a [survey on how people use search in GNOME](https://discourse.gnome.org/t/survey-types-of-content-people-search-through/12646), with the goal of improving automated testing. Please share a sentence or two on what content you search through. Remember that if your use case has automated testing, it's more likely that bugs will be quickly found and fixed.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

