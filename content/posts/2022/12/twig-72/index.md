---
title: "#72 Automated Testing"
author: Felix
date: 2022-12-02
tags: ["mutter", "gtk", "gaphor", "blackfennec", "girens", "blueprint-compiler", "gnome-control-center"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from November 25 to December 02.<!--more-->

# Core Apps and Libraries

### Mutter [↗](https://gitlab.gnome.org/GNOME/mutter/)

A Wayland display server and X11 window manager and compositor library.

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) announces

> The Mutter & Shell team published an article about the recent developments on automating the testing of the compositor of the GNOME desktop. [Have a read!](https://blogs.gnome.org/shell-dev/2022/12/02/automated-testing-of-gnome-shell/)
> ![](04b4049357d3d9f34ab98164b8735a8ab8e5285a.png)

### GTK [↗](https://gitlab.gnome.org/GNOME/gtk)

Cross-platform widget toolkit for creating graphical user interfaces.

[Jordan Petridis](https://matrix.to/#/@alatiera:matrix.org) reports

> After more than [a year of work](https://gitlab.freedesktop.org/gstreamer/gst-plugins-rs/-/merge_requests/588), the rusty GStreamer Paintable Sink for GTK 4 has received support for GL Textures, reducing dramatically the CPU consumed (from 400-500% to 10-15% for the a 4k stream scenarios we tested) and allowing for zero-copy rendering when used with hardware decoders.

### Settings [↗](https://gitlab.gnome.org/GNOME/gnome-control-center)

Configure various aspects of your GNOME desktop.

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) announces

> GNOME Settings continues to receive polish, reviews, and new designs from various contributors. We're already stashing exciting improvements for the next release:
> 
>  * Continuous improvements to the Device Security panel have been added. These improvements range from better wording of the security features, new designs for the dialogs, and making the panel more actionable. Thanks to Kate Hsuan and Richard Hughes for diligently working on this.
>  * The Accessibility panel was redesigned. This is the first panel implementing a more modern navigation model in Settings. More panels should be redesigned for this navigation pattern in the future. Thanks to Sadiq for this.
>  * The Date & Time panel is now more mobile friendly, by using a two-column layout to the month selector. This change was also thanks to Sadiq.
>  * The Network & Wi-Fi panels now use libnma's own security widgets for managing connections. This is a massive cleanup in the codebase, and allows us to concentrate efforts on a single place. This change was made possible by Lubomir Rintel.
>  * Various polishes and smaller improvements to lots of panels, like Users, Wacom, Region and Language, and others
> ![](63b4f08b995d5fde6e840555f55e1ff031185262.png)
> ![](44b149c0af869f40ce7afa6e0e21a0133d26c643.png)

# Circle Apps and Libraries



### Gaphor [↗](https://gaphor.org)

A simple UML and SysML modeling tool.

[Dan Yeaw](https://matrix.to/#/@Yeaw:matrix.org) says

> [Gaphor](https://gaphor.org), the simple modeling tool, feature release version 2.13.0 is now out! It includes some great updates, including:
> 
> * Auto-layout for diagrams
> * Relations to actors can connect below actor name
> * Export to EPS
> * Zoom with Ctrl+scroll wheel works again
> 
> It is also got some great UI improvements, with GTK4 and libadwaita now the default for Linux and Windows.

# Third Party Projects

[lwildberg](https://matrix.to/#/@lw64:gnome.org) announces

> I am announcing the first release of the new app "Meeting Point". It is a video conferencing client using BigBlueButton in the background. At the moment the features exposed in the still experimental UI are:
> * join meetings (also with passwords) hosted by senfcall.de, a free BigBlueButton provider
> * watch webcam video streams of participants
> * read the public group chat
> * see a list of all participants
> * listen to audio (can be turned off)
> * delete the group chat history, if you are moderator
> 
> The source code is [here](https://gitlab.gnome.org/lwildberg/meeting-point). To try Meeting Point out, open it with GNOME Builder and hit the run button. New features will come soon! I will be also happy to guide anyone who is interested through the internals :)
> ![](b7739906d9d7ce75fa81d25e634791fa47b37304.png)
> ![](64184f5c28427924da4c9edc57ca64afcdc0c395.png)

[Khaleel Al-Adhami](https://matrix.to/#/@adhami:matrix.org) says

> Introducing Converter, a libadwaita GTK4 app that lets you convert and manipulate images in a sleek interface. It is built on top of imagemagick as well as other python libraries. 
> 
> Converter 1.1.0 was released with SVG support and resizing operations! It's available on flathub:
> https://flathub.org/apps/details/io.gitlab.adhami3310.Converter

### Girens for Plex [↗](https://flathub.org/apps/details/nl.g4d.Girens)

Girens is a Plex Gtk client for playing movies, TV shows and music from your Plex library.

[tijder](https://matrix.to/#/@tijder:matrix.org) says

> Girens version 2.0.1 just released. With this version the transcoding protocol is changed to [DASH](https://en.wikipedia.org/wiki/Dynamic_Adaptive_Streaming_over_HTTP). Thanks to changing the transcode protocol some resume playback bugs are fixed. 
> Also now if items are loaded from the server in the section view a loading icon is showed. The sidebar containing the titles of the section have now an icon next to it. Also the translations are updated. For more details about the changes see the [changelog](https://gitlab.gnome.org/tijder/girens/-/releases/v2.0.1).

### Blueprint [↗](https://jwestman.pages.gitlab.gnome.org/blueprint-compiler/)

A markup language for app developers to create GTK user interfaces.

[James Westman](https://matrix.to/#/@flyingpimonster:matrix.org) reports

> blueprint-compiler v0.6.0 is out! This is mostly a bugfix release, but it also adds a `typeof()` operator for specifying GType properties like `Gio.ListStore:item-type`. It also includes some internal refactoring that paves the way for some great features in the future, including alternate output formats!

### BlackFennec [↗](https://blackfennec.org/)

A beautiful and easy to use application for viewing and editing semi-structured data like JSON.

[Simon](https://matrix.to/#/@simon:yodabyte.ch) announces

> BlackFennec v0.10 is out!
> 
> This version introduces actions! It is now possible to execute functions on elements . Try out one of the new actions such as `to lower` on strings and `clear collection` on lists.
> 
> We have also added the ability to undo and redo any changes you made to your data; a handy feature if you accidentally cleared a collection 😉
> 
> My favorite new addition however is copy and paste. You can export or import any element to and from JSON via your clip board!
> ![](luVzSnwRYMkiGcthWaWbHAMh.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
