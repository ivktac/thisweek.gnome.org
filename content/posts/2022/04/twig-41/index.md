---
title: "#41 Italian Gestures"
author: Felix
date: 2022-04-29
tags: ["gjs", "commit", "furtherance", "gtk", "amberol", "shortwave", "gnome-shell"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from April 22 to April 29.<!--more-->

# Core Apps and Libraries

### GNOME Shell [↗](https://gitlab.gnome.org/GNOME/gnome-shell)

Core system user interface for things like launching apps, switching windows, system search, and more.

[verdre](https://matrix.to/#/@verdre:gnome.org) shows

> At LAS, we started working on two-dimensional gestures for gnome-shell, it will soon be possible to do workspace switching while doing the overview gesture
> {{< video src="two-dimensional-gestures.mp4" >}}

### GTK [↗](https://gitlab.gnome.org/GNOME/gtk)

Cross-platform widget toolkit for creating graphical user interfaces.

[Sophie Herold](https://matrix.to/#/@sophieherold:gnome.org) announces

> Issues where in some constellations the GTK4 file or folder chooser did not allow to save a file or select a folder, have been resolved thanks to some [very](https://gitlab.gnome.org/GNOME/gtk/-/commit/aa9cac695d820a48c735218a0259ba62a35964d4) [simple](https://gitlab.gnome.org/GNOME/gtk/-/commit/908661fee6a8275b40ef27f813543ef9f7a5aff5) fixes.

### GJS [↗](https://gitlab.gnome.org/GNOME/gjs)

Use the GNOME platform libraries in your JavaScript programs. GJS powers GNOME Shell, Polari, GNOME Documents, and many other apps.

[ptomato](https://matrix.to/#/@ptomato:gnome.org) says

> In GJS, Xi Ruoyao fixed a serious crash that occurred with newer versions of libffi.

# Circle Apps and Libraries

### Shortwave [↗](https://gitlab.gnome.org/World/Shortwave)

Internet radio player with over 30000 stations.

[Felix](https://matrix.to/#/@felix:haecker.io) announces

> I released Shortwave 3.0! Notables changes / new features:
> * Updated user interface that uses the new Adwaita design
> * Support for the new GNOME 42 dark mode
> * New option to add private stations to the library, which should not (or cannot) be available on radio-browser.info (e.g. local network/paid streams).
> * Display station bitrate information, which can also be used as a sorting option
> * New button on the search page that allows to sort the search results
> 
> If you want to know all changes, you should check out the complete changelog [on Flathub](https://flathub.org/apps/details/de.haeckerfelix.Shortwave), or my [Twitter](https://twitter.com/haeckerfelix/status/1517981128250073092) / [Mastodon](https://mastodon.social/web/@haeckerfelix/108183554004795402) thread.
> ![](nIkaqtqkuUDpieioFNiTKQod.png)

### Commit [↗](https://github.com/sonnyp/Commit)

An editor that helps you write better Git and Mercurial commit messages.

[sonnyp](https://matrix.to/#/@sonnyp:matrix.org) reports

> Version 3.2.0 of Commit message editor is out with
> 
> * Improve auto capitalization
> * Add support for gitconfig and hgrc
> * Support editing any file
> * Make the primary button label dynamic (Tag, Rebase, Commit, ...)
> * Use 2 spaces indentation
> * Add menu button
> * GNOME Platform 42
> * Various fixes
> ![](JwzCTCRzyYsxauqUWkqTNMNB.png)

# Third Party Projects

[Mazhar Hussain](https://matrix.to/#/@realmazharhussain:matrix.org) reports

> Version v0.5.2 of [Login Manager Settings](https://github.com/realmazharhussain/gdm-settings) was released. Compared to v0.4, Mouse settings were added, support for right-to-left languages was improved and more text got localization support. The application also got German, Occitan, and Spanish translations.
> Also, 1st [flatpak version of the app](https://flathub.org/apps/details/io.github.realmazharhussain.GdmSettings) was released on [FlatHub](https://flathub.org).
> ![](ejQaFTEdMYGseXEckJcQZiFZ.png)

### Furtherance [↗](https://github.com/lakoliu/Furtherance)

Track your time without being tracked

[rickykresslein](https://matrix.to/#/@rickykresslein:matrix.org) says

> [Furtherance](https://github.com/lakoliu/Furtherance) v1.2.0 was released and includes a Pomodoro-style countdown timer! The Pomodoro timer can be switched on in Preferences. Also, changing the date of a task when it is the only task in its group no longer causes the application to crash.
> ![](ijAGVHrRKYAQzGViqxKNSJtX.png)

### Amberol [↗](https://gitlab.gnome.org/ebassi/amberol/)

Plays music, and nothing else.

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) says

> New week, new release of Amberol! Some additional tweaks to the UI, with the playlist queue now on the left hand side of the main window thanks to [the design of Tobias Bernard](https://gitlab.gnome.org/World/amberol/-/issues/51); but, most importantly, the song loading is now more reliable and you should not experience any more unexpected crashes when switching between songs.
> ![](af7418f9e73646138b311da7838a4000500d19d2.png)
> ![](7786a649bee6d186bf89da2054b78b6a58103d5c.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
