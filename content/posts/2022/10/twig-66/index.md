---
title: "#66 Foundation Updates"
author: Felix
date: 2022-10-21
tags: ["gtk-rs", "hebbot", "epiphany", "identity", "tubeconverter", "tagger", "eyedropper", "krb5-auth-dialog", "flatseal", "glib"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from October 14 to October 21.<!--more-->

# GNOME Foundation

[Thib](https://matrix.to/#/@thib:ergaster.org) announces

> Some (good) news from the Foundation!
> 
> 🇧🇪 Foundation staff is busy with GNOME Asia and prep work for LAS. We've also asked for a stand at FOSDEM, and hope to see you in Brussels in February 
> 
> 🧑‍💼 Synchronising everyone in the ED Search Committee has been a little difficult over the summer. After a hiatus, we're making steady progress again. We're coming after you, next ED!
> 
> 🧹 The sysadmin team has also been busy getting rid of legacy services to reduce their maintenance load, focus on the essential ones, and give Flathub some love. Our mailing-lists won't accept nor distribute new mail and most of them will be moved to Discourse. The archives will remain online. All of the IRC bots have been decommissioned and are about to be replaced by [hookshot](https://github.com/matrix-org/matrix-hookshot), the multi-purpose Matrix bot that speaks GitLab. More on "getting rid of legacy services" soon.
> 
> 📈 Last, but definitely not least: you might remember the three initiatives we told you about a while ago? Newcomers, Local-First Apps, and Flathub Payments. There's one making outstanding progress: Flathub Payments. We're working on raising funds through some grant applications to cover the staff and operating costs, and have lawyers working on the compliance, corporate and governance matters we need in the background to support payments and donations in Flathub.
> 
> 💻️ On the more technical side of things, there are only a couple of tasks remaining for correctly generating invoices in Stripe before [Codethink's](https://www.codethink.co.uk/) final work phase is complete. We're going to start building a roadmap to launch the new features over the coming months. You can follow the progress [here](https://github.com/flathub/website/issues/256) and [here](https://github.com/orgs/flathub/projects/2/views/1).
> 
> 🤝 A special thank you to our president Robert McQueen in particular for sinking countless hours in orchestrating the Flathub Payments project (including hilarious paperwork and exhilarating legal stuff) and huge kudos to [Codethink](https://www.codethink.co.uk/) for being an amazing partner to work with. They have definitely been going above and beyond to support Flathub. We cannot stress enough how great they have been, thank you Codethink!


# Core Apps and Libraries

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) says

> Emmanuel Fleury has continued his campaign of tackling the oldest GLib bugs, by adding support for optimised `g_str_has_prefix()` and `g_str_has_suffix()` checks when passed static strings — the request for this feature was 18 years old (https://gitlab.gnome.org/GNOME/glib/-/issues/24)

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) reports

> Thomas Haller has dug into a race condition with `EINTR` handling and `close()` in `g_spawn_*()` and has fixed it and documented his findings (https://gitlab.gnome.org/GNOME/glib/-/merge_requests/2947)

### Web [↗](https://gitlab.gnome.org/GNOME/epiphany)

Web browser for the GNOME desktop.

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) says

> The [GTK4 port](https://gitlab.gnome.org/GNOME/epiphany/-/merge_requests/1073) of Epiphany has finally landed

# Circle Apps and Libraries

### Identity [↗](https://gitlab.gnome.org/YaLTeR/identity)

Compare images and videos.

[Ivan Molodetskikh](https://matrix.to/#/@yalter:gnome.org) announces

> I've published Identity 0.4.0! It's got a new Media Properties dialog showing some information about the current video. Opening files with drag-and-drop or copy-paste on Flatpak now also works thanks to the update to the GNOME 43 platform.
> ![](72875819ff28f71ed9bd2a15fa35a72b356a32c3.png)

### gtk-rs [↗](https://gtk-rs.org/)

Safe bindings to the Rust language for fundamental libraries from the GNOME stack.

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) reports

> A new [gtk-rs](https://gtk-rs.org) release is out with plenty of improvements and bug fixes. Details can be read at https://gtk-rs.org/blog/2022/10/18/new-release.html

# Third Party Projects

[sonnyp](https://matrix.to/#/@sonnyp:matrix.org) announces

> I published [Retro](https://beta.flathub.org/apps/details/re.sonny.Retro), a toy digital segment clock that can be customized with CSS.
> ![](WwtmVcxsvcqkdeZXyBNUdeRk.png)
> ![](uVWWJLnJgWVsKDjUunkAjyom.png)
> ![](nIwwlOydMEvoqrcncEgWlRdE.png)

### Tube Converter [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

An easy-to-use video downloader (yt-dlp frontend).

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) announces

> Tube Converter is now at V2022.10.3 and has seen many improvements and new features this week.
> Here's some of the changes:
> * Added a preference to embed metadata in a download
> * Added the ability to download subtitles for a video
> * Implemented proper stop function for download
> * 'New Filename' is now allowed to be empty. If it is empty, the video title will be used
> * Improved video url checking
> ![](MKSveEUQRwmgDGyEilubBaBo.png)

### Tagger [↗](https://flathub.org/apps/details/org.nickvision.tagger)

An easy-to-use music tag (metadata) editor.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) reports

> Tagger is now at V2022.10.4 and has seen many improvements and new user-requested features this week.
> Here are some of the changes:
> * Added an Advanced Search function to search through contents of files' tags to find properties that are empty or contain a certain value . Type `!` in the search box to activate it and learn more
> * Added 'Discard Unapplied Changes' action
> * Tagger now remembers user-filled tag properties waiting to be applied
> * Fixed ogg file handling
> * Improved closing and reloading dialogs
> ![](xGVtmkDhGLxijnXwdjAvkGWr.png)

### Hebbot [↗](https://github.com/haecker-felix/hebbot)

Hebbot is the bot behind TWIG that manages all the news.

[Felix](https://matrix.to/#/@felix:haecker.io) announces

> Thib has made Hebbot (aka TWIG-Bot ) [a little smarter](https://github.com/haecker-felix/hebbot/pull/69). Hebbot now scans your messages for keywords, and can automatically match them to appropriate projects.  A small, but nice improvement, which reduces the administration effort of TWIG again noticeably.
> 
> In concrete terms, this means that you no longer have to define the `usual_reporters` field in in the [bot configuration](https://gitlab.gnome.org/Teams/Websites/thisweek.gnome.org/-/blob/main/hebbot/config.toml).

### Flatseal [↗](https://github.com/tchx84/Flatseal)

A graphical utility to review and modify permissions of Flatpak applications.

[Martín Abente Lahaye](https://matrix.to/#/@tchx84:matrix.org) says

> Flatseal 1.8.1 is out! It brings support for the new `--socket=gpg-agent` permission, Tamil and Hebrew translations, use of different colors for override status icons, updated Flatpak icon, a few important bug fixes and [more](https://github.com/tchx84/Flatseal/blob/master/CHANGELOG.md#181---2022-10-15).
> 
> Get it on [Flathub](https://flathub.org/apps/details/com.github.tchx84.Flatseal)!
> ![](VhGSseLgXVvkIkrCysAkcyRc.png)

### Eyedropper [↗](https://flathub.org/apps/details/com.github.finefindus.eyedropper)

A powerful color picker and formatter.

[FineFindus](https://matrix.to/#/@finefindus:matrix.org) says

> Version 0.4 has of Eyedropper has been released. In addition to the features of the last few weeks, it is now possible to search by the color name. To view the full changelog, visit the [release page](https:github.com/FineFindus/eyedropper/releases) or download the newest version from Flathub.

### Kerberos Authentication [↗](https://gitlab.gnome.org/GNOME/krb5-auth-dialog)

An application to acquire and list Kerberos tickets.

[Guido](https://matrix.to/#/@guido.gunther:talk.puri.sm) says

> I've ported [krb5-auth-dialog](https://gitlab.gnome.org/GNOME/krb5-auth-dialog) to GTK4 and libadwaita making it usable on mobile phones too. While at that I fixed the PKINIT support with smart cards when using Heimdal Kerberos.
> ![](WatAlTszwmCoiPpJBCVtKjbl.png)
> ![](rnxbyXfQosyFEYipfYoTKJCq.png)

# GNOME Shell Extensions

[glerro](https://matrix.to/#/@glerro:matrix.org) says

> Hi i have created a new Gnome Shell extension that add a switch to the WiFi menu, in the GNOME system menu, that show a QrCode of the active connection. This can be useful for quickly connecting devices capable of reading QrCode and applying the settings to the system, without having to type in the name and the password of the WiFi. (e.g. Android Smartphone).
> ![](JoQmWisTSbjLxthPTTnAXIVd.png)

# Miscellaneous

[Sophie](https://matrix.to/#/@sophieherold:gnome.org) says

> [Apps for GNOME](https://apps.gnome.org/) received a [bunch of changes](https://gitlab.gnome.org/World/apps-for-gnome/-/merge_requests/24) in the background. These changes will not only help with the maintenance of the code base but will also allow sharing a joint base with other projects currently [in](https://sophie-h.pages.gitlab.gnome.org/app-overview/) [development](https://sophie-h.pages.gitlab.gnome.org/welcome/). If everything went well, this should not have changed anything in the website's appearance.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
