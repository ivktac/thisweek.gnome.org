---
title: "#44 Five Across"
author: Felix
date: 2022-05-20
tags: ["pika-backup", "crosswords", "geopard", "telegrand", "gnome-software", "amberol"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from May 13 to May 20.<!--more-->

# Core Apps and Libraries

### Software [↗](https://gitlab.gnome.org/GNOME/gnome-software/)

Lets you install and update applications and system extensions.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) announces

> gnome-software gained some nice polish from Milan Crha to format file size numbers and units in slightly different font sizes, to highlight the number: https://gitlab.gnome.org/GNOME/gnome-software/-/merge_requests/1319

# Circle Apps and Libraries

[Sophie Herold](https://matrix.to/#/@sophieherold:gnome.org) reports

> This week, [Warp](https://apps.gnome.org/app/app.drey.Warp/) joined GNOME Circle. Warp allows file transfers from one device to another, just by sharing a code. Congratulations!
> ![](ed5437a951fb5b2486c86aa394feffcea20a0b26.png)

### Pika Backup [↗](https://wiki.gnome.org/Apps/PikaBackup)

Simple backups based on borg.

[Sophie Herold](https://matrix.to/#/@sophieherold:gnome.org) says

> Pika Backup version 0.4 has been released. This release wraps up a year of development work and brings major features like scheduled backups, rule-based removal of older archives, and an updated interface using GTK 4 and Libadwaita.
> 
> You can get the full story in the [corresponding blog post](https://blogs.gnome.org/sophieh/2022/05/15/pika-backup-0-4-released-with-schedule-support/) or [just try it out](https://apps.gnome.org/app/org.gnome.World.PikaBackup/).
> ![](102fd47dc67fe59e9ee5b7b2eb8a0cca004a9e18.png)

# Third Party Projects

### Crosswords [↗](https://gitlab.gnome.org/jrb/crosswords)

A simple Crossword player and Editor.

[jrb](https://matrix.to/#/@jblandford:matrix.org) reports

> I've released Crosswords 0.3.0. This is the first version of this game that's available for download from [flathub](https://flathub.org/apps/details/org.gnome.Crosswords). New in this release:
> 
> * .puz file format support
> * Dark mode / Light mode support
> * Hint button that suggests words that fit a slot
> * External puzzle plugins support, including assorted newspaper downloaders
> 
> More information is available at the [release announcement](https://blogs.gnome.org/jrb/2022/05/14/crosswords-0-3/)
> ![](SYAKstmEqSnflfoibJrnqiNA.png)
> ![](UsniXztMfVGkgdQSjesaPlNU.png)
> ![](eJgrWQJHycfpdjOQZnuNguyh.png)

### Telegrand [↗](https://github.com/melix99/telegrand/)

A Telegram client optimized for the GNOME desktop.

[Marco Melorio](https://matrix.to/#/@melix99:gnome.org) reports

> Telegrand development has seen quite a number of improvements since the last report. The app is finally shaping up quite well and the light for a first release seems closer than ever! These are the highlights of some of the work that happened since the last report:
> 
> *  Implemented user actions reports (e.g. users typing or sending photos) by Marcus Behrendt
> * Implemented message event types (e.g. user joining a group) by Carlod
> * Implemented message photo sending
> * Improved the message entry look
> * Added phone country code selection in the login process by Marcus Behrendt
> * Added more ways to authenticate (e.g. via SMS, call or flash call) by Marcus Behrendt
> * Hide the message entry when not needed (e.g. while in a channel) by Carlod
> * Added ability to delete messages
> * Improved chat history scrolling (now default to the bottom)
> * Added ability to pin/unpin chats by Marcus Behrendt
> * Ability to open the relative chat by clicking a notification
> ![](33cff7e64e9aa6363217f254f8433bf37f1eeaa3.png)

### Geopard [↗](https://github.com/ranfdev/Geopard)

A Gemini client

[ranfdev](https://matrix.to/#/@ranfdev:matrix.org) announces

> I've released Geopard v1.1.0, the new version of my gemini client. This release brings:
> 
> * Improved design, with completely new pages for downloads, input requests, external links, errors
> * Added more spacing between links to make them easier to click on small screens
> * Added zoom functionality, via shortcuts or directly from the popover menu
> * Streaming button (alpha) for some audio/video file types
> 
> From the last announcement, I've also fixed various issues reported on github. Mainly, there was an issue with the handling of downloads, which is now fixed.
> ![](bcaYftlIZbcpPNUcEUnYvxJw.png)

### Amberol [↗](https://gitlab.gnome.org/ebassi/amberol/)

Plays music, and nothing else.

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) says

> New Amberol release, with lots of papercut fixes, improvement in the UI responsiveness when loading large directories, and accessibility fixes for both the UI and key navigation.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

