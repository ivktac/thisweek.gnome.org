---
title: "#45 Timeout!"
author: Felix
date: 2022-05-27
tags: ["amberol", "workbench", "glib", "nautilus", "furtherance"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from May 20 to May 27.<!--more-->

# Core Apps and Libraries

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) says

> New `g_idle_add_once()` and `g_timeout_add_once()` functions just landed in GLib, courtesy of Emmanuele Bassi. They make it simpler to add an idle or timeout callback which is only ever dispatched once.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) reports

> Also in GLib land, Thomas Haller’s [work to add automatic `NULL`-termination support](https://gitlab.gnome.org/GNOME/glib/-/merge_requests/1485) to `GPtrArray` has just landed

### Files [↗](https://gitlab.gnome.org/GNOME/nautilus)

Providing a  simple and integrated way of managing your files and browsing your file system.

[antoniof](https://matrix.to/#/@antoniof:gnome.org) reports

> Solid improvements are coming to the GTK 4 port of Files. This week, with the help of Corey Berla and advice from Alexander Mikhaylenko, I've enhanced the experience for mouse users without hindering future improvements for touch users. As a new feature, now the middle button can be used to open multiple selected files at once.

# Third Party Projects

### Workbench [↗](https://github.com/sonnyp/Workbench/)

A sandbox to learn and prototype with GNOME technologies.

[sonnyp](https://matrix.to/#/@sonnyp:matrix.org) says

> I added support for previewing templates and signals in Workbench as well as converting back and forth between XML and Blueprint
> ![](uyFOMaNtDPXzCVRytySxjAWV.png)

### Furtherance [↗](https://github.com/lakoliu/Furtherance)

Track your time without being tracked

[Ricky Kresslein](https://matrix.to/#/@rickykresslein:matrix.org) reports

> [Furtherance](https://github.com/lakoliu/Furtherance) v1.3.0 was released and now has the ability to autosave and automatically restore autosaves after an improper shutdown. Also, tasks can now be added manually, and task names can be changed for an entire group.
> ![](ebSbAiDporbBkZDEZSXxqZGc.png)

### Amberol [↗](https://gitlab.gnome.org/ebassi/amberol/)

Plays music, and nothing else.

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) says

> Various fixes to Amberol, including [a refresh of the main application icon](https://gitlab.gnome.org/World/amberol/-/merge_requests/66), and tweaks to the styling of elements such as the [waveform view](https://gitlab.gnome.org/World/amberol/-/merge_requests/65), [volume control, and loading progress bar](https://gitlab.gnome.org/World/amberol/-/merge_requests/63).
> ![](3f7062850e9bc0dc3146bc75c7f7b4f119fdf0d0.png)

# Miscellaneous

[Thib](https://matrix.to/#/@thib:ergaster.org) says

> GNOME is participating to GSoC this year again, and we have no less than 9 interns this year! Health, Chromecast Support, Pitivi (video editor), Nautilus (files manager), Fractal and engagement websites: such are the projects that will receive great contributions this summer!
> 
> You can read all the details, welcome the interns and thank the mentors [in this post](https://discourse.gnome.org/t/announcement-gnome-will-be-mentoring-9-new-contributors-in-google-summer-of-code-2022/9918). Many thanks to contributing to a vibrant project and community.

# GNOME Shell Extensions

[aunetx](https://matrix.to/#/@aunetx:matrix.org) reports

> [Blur my Shell](https://github.com/aunetx/blur-my-shell) has recently seen some big changes, and a lot more are probably coming soon!
> 
> To keep you updated:
> * a color and a noise effect have been added, they can help to make the blur more legible and prevent color banding on low-resolution screens -- or to simply have have some fun :p
> * a lot of the internal preferences have been changed -- this probably resulted in some of you having Blur my Shell preferences reset, sorry about this :(
> * translation have been added to different languages, including French, Chinese, Italian, Spanish, Norwegian and Arabic: if you want to help the project, you can translate the preferences in you own language [using Weblate](https://hosted.weblate.org/engage/blur-my-shell/)!

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
