---
title: "#48 Adaptive Calendar"
author: Felix
date: 2022-06-17
tags: ["telegrand", "decoder", "amberol", "gnome-calendar"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from June 10 to June 17.<!--more-->

[Neil McGovern](https://matrix.to/#/@nmcgovern:matrix.org) reports

> GNOME mourns the loss of Marina Zhurakhinskaya - https://www.outreachy.org/blog/2022-06-14/remembering-and-honoring-marina-zhurakhinskaya-founder-of-outreachy/

‎‎‎‏‏‎ ‎‏‏‎ ‎

---

# Core Apps and Libraries

### Calendar [↗](https://gitlab.gnome.org/GNOME/gnome-calendar/)

A simple calendar application.

[Adrien Plazas](https://matrix.to/#/@adrien.plazas:gnome.org) says

> Calendar [received a new sidebar](https://gitlab.gnome.org/GNOME/gnome-calendar/-/merge_requests/226) containing a date chooser and an agenda view, which replaced the year view and the navigation arrows. It is the first step implementing its new adaptive design, but please note the application isn't adaptive yet.
> ![](1a041b94057baa76407222e813d05321ce5e3432.png)

# Circle Apps and Libraries

[Fina](https://matrix.to/#/@felinira:matrix.org) announces

> Warp 0.2.0 was released: It features many design improvements, lots of new translations, support for mobile devices, improved error handling and much more. Happy file transferring :)
> ![](SmsnteymlTXqIaGadejgBGPn.png)

### Decoder [↗](https://gitlab.gnome.org/World/decoder/)

Scan and Generate QR Codes.

[Maximiliano](https://matrix.to/#/@msandova:gnome.org) reports

> Decoder 0.3.0 is out, some highlights:
> 
> * QR codes are always black on white for maximum compatibility
> * See the text contents of a newly scanned code
> * Scanned codes are automatically stored in history
> ![](fceba2762fe4e52bed3fdecf7db990c4e892cc70.png)

### Amberol [↗](https://gitlab.gnome.org/ebassi/amberol/)

Plays music, and nothing else.

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) announces

> Amberol 0.8.0 is out: you can now search for songs in your playlist just by starting to type, even when in selection mode. Plus: Amberol can now run in the background through the sandbox portal; cover art is shared across songs in the same album, to reduce memory use; and the window size is correctly restored across sessions. Bonus news: if you use macOS, you can now build and run Amberol using dependencies from the Homebrew project.
> ![](2cf9b373101d8afba98d25e66eb6449107064ce3.png)

# Third Party Projects

[noëlle](https://matrix.to/#/@jannvier:matrix.org) announces

> Bottles 2022.6.14 was released, bringing GTK4+Libadwaita, performance enhancements, and many smaller interface improvements.
> ![](CGpcztlSrlAItHeDTjrjTEhi.png)

[xjuan](https://matrix.to/#/@xjuan:gnome.org) says

> Cambalache 0.10.0 is out! - Adwaita, Handy, inline objects, special child types, and [more...](https://blogs.gnome.org/xjuan/2022/06/15/cambalache-0-10-0-is-out/)
> ![](d429a6e3898e7416945ecbfbf5186abac25b2e59.gif)

### Fractal [↗](https://gitlab.gnome.org/GNOME/fractal)

Matrix messaging app for GNOME written in Rust.

[Marco Melorio](https://matrix.to/#/@melix99:gnome.org) says

> Hi there! I'm Marco Melorio and I'm participating in this year's Google Summer of Code, under the GNOME Foundation. I'm working on Fractal, the GNOME matrix client, with the help of my mentor Julian Sparber. More specifically, I'm working on implementing a media history viewer to the app.
> 
> To follow my progress on the project you can check out my blog [here](https://melix99.wordpress.com/). I've already published a small [introduction post](https://melix99.wordpress.com/2022/06/07/introduction/) about me and a first [update post](https://melix99.wordpress.com/2022/06/17/gsoc-update-1-planning/) which includes a mockup and milestones about the project.

# GNOME Foundation

[Neil McGovern](https://matrix.to/#/@nmcgovern:matrix.org) reports

> Microsoft has awarded GNOME $10,000 for winning its FOSS fund #20 https://twitter.com/sunnydeveloper/status/1536744475979939841

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

