---
title: "#68 New Dialogs"
author: Felix
date: 2022-11-04
tags: ["money", "endeavour", "gtk"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from October 28 to November 04.<!--more-->

# Core Apps and Libraries

### GTK [↗](https://gitlab.gnome.org/GNOME/gtk)

Cross-platform widget toolkit for creating graphical user interfaces.

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) reports

> There's going to be a whole new API available in GTK 4.10 for dialogs:
> 
> * [GtkFileDialog](https://docs.gtk.org/gtk4/class.FileDialog.html) replaces GtkFileChooserDialog
> * [GtkColorDialog](https://docs.gtk.org/gtk4/class.ColorDialog.html) replaces GtkColorChooserDialog
> * [GtkFontDialog](https://docs.gtk.org/gtk4/class.FontDialog.html) replaces GtkFontChooserDialog
> * [GtkAlertDialog](https://docs.gtk.org/gtk4/class.AlertDialog.html) replaces GtkMessageDialog
> 
> All these new classes are not widgets, and are designed around asynchronous calls instead of signal emissions. Once you call a method to perform the desired action, you'll get a callback when the user closes the dialog window. Head over to the [GTK development blog](https://blog.gtk.org/2022/10/30/on-deprecations/), for an article on the newly introduced API and its design.
> 
> This new API allows us to deprecate the following classes and interfaces:
> 
> * GtkFileChooserDialog, GtkFileChooserWidget, and GtkFileChooser
> * GtkColorChooserDialog, GtkColorChooserWidget, GtkColorButton, and GtkColorChooser
> * GtkFontChooserDialog, GtkFontChooserWidget, GtkFontButton, GtkFontChooser
> * GtkMessageDialog
> 
> GtkDialog itself has been deprecated. You should derive your own GtkWindow for any sort of custom dialog in your application; if you use libadwaita you can already use ready-made dialogs like AdwPreferencesWindow and AdwMessageDialog.

# Third Party Projects

### Money [↗](https://flathub.org/apps/details/org.nickvision.money)

A personal finance manager.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) says

> I'm happy to announce the release of [Money](https://github.com/nlogozzo/NickvisionMoney) and it's availability on [Flathub](https://beta.flathub.org/apps/details/org.nickvision.money) . Money is a personal finance manager for GNOME with an easy-to-use and beautiful interface.
> ![](SRQMJJyqSxOHYwbjuxEzHlbv.png)

### Endeavour [↗](https://gitlab.gnome.org/World/Endeavour)

An intuitive and powerful application to manage your personal tasks.

[Jamie](https://matrix.to/#/@itsjamie9494:matrix.org) reports

> Endeavour version 43 has been released! This release featured lots of bug squashing and slight UI improvements, for a more stable experience. Endeavour can be downloaded from Flathub

# Documentation

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) reports

> gi-docgen, the introspection-based documentation generator used (among others) by GTK to publish its [API reference](https://docs.gtk.org) has gained the ability to show if a symbol, type, signal, or property is currently unstable and will be available in the next stable release. This should help visually distinguish newly added API in the reference generated directly from bleeding edge sources. The same style is also used to present when a symbol was introduced, and when it was deprecated.
> ![](doc01.png)
> ![](doc02.png)

# GNOME Shell Extensions

[Cleo Menezes Jr.](https://matrix.to/#/@cleomenezesjr:matrix.org) reports

> The first version of **Weather O'Clock** has been released.
> An extension to display the current Weather on the left side of the clock without de-centering it from the panel.
> GNOME Weather is required for this extension to function.
> 
> [Install it from extensions.gnome.org.](https://extensions.gnome.org/extension/5470/weather-oclock/)
> ![](HVBIctGqibtUHFuqbMNLMXIc.png)
> ![](XsDwHXoIMRHKNoMIVhzuctbQ.png)

# GNOME Foundation

[Thib](https://matrix.to/#/@thib:ergaster.org) says

> GNOME is moving away from GIMPnet! You can read the full announcement here: https://discourse.gnome.org/t/gnome-moves-away-from-gimpnet/12046
> 
> If you are in charge of a project and you have a room listed in our [#community:gnome.org](https://matrix.to/#/#community:gnome.org) Space (reproduced [here](https://discourse.gnome.org/t/gnome-moves-away-from-gimpnet/12046/8)), and want to use that opportunity to explicitly unbridge, please let us know in this thread.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

