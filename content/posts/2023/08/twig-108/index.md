---
title: "#108 Sandboxed Images"
author: Felix
date: 2023-08-11
tags: ["gnome-contacts", "tagger", "phosh", "gtk", "parabolic", "newsflash", "gaphor", "gjs", "cavalier", "ashpd", "denaro", "loupe", "gnome-maps", "daikhan"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from August 04 to August 11.<!--more-->

# GNOME Core Apps and Libraries

### Image Viewer (Loupe) [↗](https://apps.gnome.org/app/org.gnome.Loupe/)

Browse through images and inspect their metadata.

[Chris 🌱️](https://matrix.to/#/@brainblasted:gnome.org) announces

> Loupe has been officially added as part of the GNOME core app set as the new default image viewer.

[Sophie (she/her)](https://matrix.to/#/@sophieherold:gnome.org) says

> We have landed the two last significant changes for GNOME 45. The image loading via [glycin](https://gitlab.gnome.org/sophie-h/glycin) is now fully sandboxed, including SVGs. And the print dialog got updated to a new design by Allan Day.
> ![](27fed53eb10c4ee142bab1b46667eb3caf69e4ac.png)

### GTK [↗](https://gitlab.gnome.org/GNOME/gtk)

Cross-platform widget toolkit for creating graphical user interfaces.

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) reports

> Early GTK 4.12 release following GUADEC; lots of fixes and long awaited features:
> * list views gained support for sections, using a new [`GtkSectionModel`](https://docs.gtk.org/gtk4/iface.SectionModel.html) interface; various models in GTK implement this interface
> * also in list views: you can now programmatically scroll [`GtkListView`](https://docs.gtk.org/gtk4/method.ListView.scroll_to.html), [`GtkGridView`](https://docs.gtk.org/gtk4/method.GridView.scroll_to.html), and [`GtkColumnView`](https://docs.gtk.org/gtk4/method.ColumnView.scroll_to.html) using a fine-grained API
> * finally, focusing on list views is more accurate and reliable
> * tons of fixes for the accessibility support, with better name and description computation following the ARIA specification; improved handling of composite button widgets; and a whole accessibility overlay in the GTK inspector, showing missing names and descriptions in your UI
> * the Vulkan renderer has seen major improvements; Vulkan support is still marked as experimental, but it's definitely less of a science project
> * multiple fixes in the GL renderer when it comes to mipmaps and texture filtering
> * Wayland, Windows, and macOS improvements and bug fixes
> * there are a few new deprecations, mainly related to GdkPixbuf
> The GTK team is already hard at work on the next GTK 4.14 development cycle, so stay tuned!

### Maps [↗](https://wiki.gnome.org/Apps/Maps)

Maps gives you quick access to maps all across the world.

[mlundblad](https://matrix.to/#/@mlundblad:matrix.org) reports

> Maps now has an experimental vector layer, using the new vector map support in libshumate. Currently it is using the OSM Liberty style. Longer-term the plan is to develop GNOME style style sheets supporting light and dark variant, using icons for the markers based on the GNOME Icon Library. At this time this showcases horizontally aligned labels (regardless of map rotation) and sharp rendering, also at fractional zoom levels. Some things that are not yet implemented includes being able to directly click on labels and markers, and localized names.
> ![](tRLSBSMXWSnrrsAjpCQIVfUT.png)
> ![](heADXJoEASnRTPhgRtDhgyuS.png)

### GNOME Contacts [↗](https://gitlab.gnome.org/GNOME/gnome-contacts)

Keep and organize your contacts information.

[nielsdg](https://matrix.to/#/@nielsdg:gnome.org) reports

> GNOME Contacts 45.beta is out, now powered by the GtkListView/GtkSectionModel APIs. This decreased memory usage of Contacts locally with about 20% and helps save some precious CPU cycles! It should also enable some nice-to-have behaviors like being able to select multiple contacts at once with shift-click.

### GJS [↗](https://gitlab.gnome.org/GNOME/gjs)

Use the GNOME platform libraries in your JavaScript programs. GJS powers GNOME Shell, Polari, GNOME Documents, and many other apps.

[ptomato](https://matrix.to/#/@ptomato:gnome.org) says

> GJS released a new version for GNOME 45 beta. There are several bug fixes and documentation improvements. Highlights of the release are performance improvements by Marco Trevisan, and the upgrade of the JS engine to SpiderMonkey 115 thanks to Xi Ruoyao.
> New things that you will be able to use in your JS code in GNOME 45 are the new `findLast()`, `findLastIndex()`, `with()`, `toReversed()`, `toSorted()`, and `toSpliced()` array methods, and the ability to create an array from an async iterable with `Array.fromAsync()`.

# GNOME Circle Apps and Libraries

### NewsFlash feed reader [↗](https://gitlab.com/news-flash/news_flash_gtk)

Follow your favorite blogs & news sites.

[Jan Lukas](https://matrix.to/#/@jangernert:matrix.org) reports

> NewsFlash got a new application id on flathub. It moved from `com.gitlab.newsflash` to `io.gitlab.news_flash.NewsFlash`. The flatpak CLI and Gnome software should ask you if you want to migrate to the new id.
> This was done to be able to verify the app on flathub and in preparation for a big feature update. So stay tuned.

### Gaphor [↗](https://gaphor.org)

A simple UML and SysML modeling tool.

[Arjan](https://matrix.to/#/@amolenaar:matrix.org) says

> Gaphor development is continuing at full pace. This week Dan Yeaw released Gaphor 2.20.0. Support for SysML modeling has been improved with new elements and the ability to assign types to model elements.
> ![](xkrsKtcUxCAUlZkgsihXsNQL.png)

### ashpd [↗](https://github.com/bilelmoussaoui/ashpd)

Rust wrapper around freedesktop portals.

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) announces

> finally updated [ashpd demo](https://flathub.org/apps/com.belmoussaoui.ashpd.demo) to follow the new libadwaita designs along with a new ashpd release
> ![](3d260ae04619aef2b62991c3ba752b9a5076e3a7.png)

# Third Party Projects

### Tagger [↗](https://flathub.org/apps/details/org.nickvision.tagger)

Tag your music.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) reports

> Tagger [V2023.8.2](https://github.com/NickvisionApps/Tagger/releases/tag/2023.8.2) is here! This release is packed with new features and fixes that make Tagger a lot more powerful to use. Read about them below :)
> 
> Changelog:
> 
> * Added support for managing a file's lyrics
> * Tagger will now provide suggestions while typing a genre
> * Fixed an issue where downloading MusicBrainz metadata would fail even if metadata was available
> * Fixed an issue where Web Services were disabled even though network connection was available
> * Fixed an issue where back album art was not saved correctly
> * An Info button will appear when MusicBrainz lookup fails and will provide more information about why the process failed
> * Improved tag panel design
> * Updated translations (Thanks everyone on Weblate!)
> ![](xurAQmffpMEnAxalulxyQIds.png)

### Phosh [↗](https://gitlab.gnome.org/World/Phosh/phosh)

A pure wayland shell for mobile devices.

[Guido](https://matrix.to/#/@agx:sigxcpu.org) announces

> [Phosh](https://gitlab.gnome.org/World/Phosh/phosh) and [phoc](https://gitlab.gnome.org/World/Phosh/phoc) have been released a couple of days ago. Besides not downscaling screenshots any longer phosh switched to newer versions of [libgnome-volume-control](https://gitlab.gnome.org/GNOME/libgnome-volume-control) and gmobile. The later was updated to support more notches (those of the Fairphone 4 and Poco F1) which were added to the 0.0.2 release of [gmobile](https://gitlab.gnome.org/guidog/gmobile) (also tagged last week).
> 
> Phoc got updated to newer wlroots allowing us to add support for newer xdg-shell versions but most importantly it fixes a long standing issues with GTK4 popups and on screen keyboard interaction.
> 
> The screenshot shows phoc reneding the rounded corners and the bounding box of a notch while phosh uses the same information to move the clock out of the center.
> ![](LqdyzETKgaxUwDcSUySwzcQA.png)

### Parabolic [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

Download web video and audio.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) announces

> Parabolic [V2023.8.1](https://github.com/NickvisionApps/Parabolic/releases/tag/2023.8.1) is here! This release is packed with new features, improvements, and bug fixes. Read about them below :)
> 
> Changelog:
> 
> * Added the option to prevent suspend while using Parabolic
> * Added the ability to skip the password dialog when unlocking Keyring
> * Added the ability to reset the Keyring when locked
> * Improved bitrate used for audio-only downloads with the best quality
> * Parabolic will now prefer videos with the h264 codec when downloading in the mp4 format. If space is a concern, users are recommended to download in the webm format which uses the vp9/vp8/av1 codecs
> * If overwriting files is off and a download's file name exists, a numbered suffix will be attached to the end of the file name to prevent error
> * Fixed an issue where downloads with specific timeframes would download incorrectly
> * Fixed an issue where opus downloads would sometimes fail
> * Fixed an issue where Parabolic would be unusable on systems without NetworkManager installed
> * Fixed an issue where Parabolic would say no active internet connection even though there was a connection established
> * Updated translations (Thanks everyone on Weblate!)
> ![](wckLZyoylbznvZgMkEyDKSuV.png)

### Denaro [↗](https://flathub.org/apps/details/org.nickvision.money)

Manage your personal finances.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) says

> Denaro [V2023.8.0-beta2](https://github.com/NickvisionApps/Denaro/releases/tag/2023.8.0-beta2) is here!!
> 
> This is the release that **finally introduces graphs** into the Account view as well as exported PDFs! Check it out now :)
> 
> Here's the full V2023.8.0 changelog so far:
> * Added graph visuals to the Account view as well as in exported PDFs
> * Added the option to select the entire current month as the date range filter
> * Improved the transaction description suggestion algorithm with fuzzy search
> * Fixed an issue where the help button in the import toast was not working
> * Fixed an issue where Denaro would crash if an account had incorrect formatted metadata 
> * Fixed an issue where docs were not available when running Denaro via snap
> * Updated translations (Thanks to everyone on Weblate)!
> ![](XRHZIIyjtrMVGzAbBHDxUFPi.png)
> ![](hIcaSfrTtgcpBDZBPRDnRwgr.png)
> ![](HJJbfSMfkBMgCBLKqzBsUUZQ.png)

### Daikhan [↗](https://flathub.org/apps/io.gitlab.daikhan.stable)

Play Videos/Music with style.

[Mazhar Hussain](https://matrix.to/#/@realmazharhussain:matrix.org) reports

> Last week, I announced [Daikhan](https://flathub.org/apps/io.gitlab.daikhan.stable) media player without mentioning any features of the app. Well, one of its features is that it remembers what selections the user has made (e.g. selected audio/subtitle languages, etc.) for each opened file and when the user opens any of those files again, it restores those selections.
> 
> This week, the "Remember Selections" feature received superpowers. Now, the player can recognize files by their content. So, it can restore selections made by the user even if files have been renamed, moved to a new location or both.
> 
> Additionally,
> 
> * Hardware acceleration support was added to the flatpak
> * Screenshots were updated
> * Access to files is only granted when you open them. Previously, the app had full access to the filesystem.
> 
> NOTE: The app is still in early access. So, expect some bugs and beware that big changes (even backward-incompatible ones) might occur in the app's interface and behavior.
> ![](mYpgjWogrYTcCNUMrWZzTKJr.png)

### Cavalier [↗](https://flathub.org/apps/details/org.nickvision.cavalier)

Visualize audio with CAVA.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) announces

> Cavalier [V2023.8.1](https://github.com/NickvisionApps/Cavalier/releases/tag/2023.8.1) is here! Tired of existing 6 drawing modes? Well, now there are 11, because every mode except Splitter got Circle variant! We also fixed the issue that caused incorrect drawing with display scaling enabled, and added a cute easter egg 👀
> 
> Here's the full changelog:
> * All drawing modes except Splitter now have Circle variants
> * Added an easter egg (run the program with --help to find how to activate it)
> * Fixed an issue where the app wasn't drawing correctly with >100% display scaling
> * Added welcome screen that is shown on start until any sound gets detected
> * Added Cairo backend that can be used in case of problems with OpenGL. To activate, run the program with environment variable CAVALIER_RENDERER=cairo
> * CAVA was updated to 0.9.0
> * Pipewire is now used as default input method, you can still change back to Pulse Audio using environment variable CAVALIER_INPUT_METHOD=pulse
> * Updated translations (Thanks everyone on Weblate!)
> ![](IBrrWVOjhwYHJBzdSMHeBWwM.png)
> ![](sJUiMdPrtEKYAyEnZxZAwViZ.png)

# Miscellaneous

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) reports

> I released a new version of all the core design tooling apps, with some very small subtle changes & updates of the rust bindings
> ![](f1ca73daf13a2581fbe4b4e4a99ea2999c83db87.png)

# GNOME Foundation

[Rosanna](https://matrix.to/#/@zana:gnome.org) says

> August is traditionally a slow month for the Foundation, as staff tends to take a small break after another successful GUADEC. Somehow, this week was chock full of meetings jammed in between folks who just got back from holidays and those about to leave for the same. Of particular note were: 
>  * the first regular Board meeting of the new term — we covered a lot of topics and I am looking forward to working with this new group for the next year
>  * code of conduct committee meeting — amongst other things, we are working on our processes and transparency reports
>  * executive director search committee — interviews are ongoing and we met to touch base and make sure there aren't any hiccups
> 
> In addition, I have been catching up on the bills that accumulated on my virtual desk during GUADEC, doing various HR tasks (including digging up paperwork to give references), finishing the filing of our 2022 tax forms, and making sure the staff get the support they need. I see travel committee requests starting to come in… that will be on my todo list next week!
> 
> Do you have a talk about GNOME you want to give in a new location? The CFP for GNOME Asia is open! More information available here — https://foundation.gnome.org/2023/08/08/gnomeasia-2023-cfp/

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
