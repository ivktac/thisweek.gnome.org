---
title: "#91 Inverted Titles"
author: Felix
date: 2023-04-14
tags: ["phosh", "gnome-software", "gtk-rs", "pika-backup", "libadwaita", "gdm-settings", "tubeconverter", "video-trimmer", "denaro"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from April 07 to April 14.<!--more-->

# GNOME Core Apps and Libraries

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alex (they/them)](https://matrix.to/#/@alexm:gnome.org) reports

> libadwaita has 2 new types of boxed list rows now:
> 
> * [Spin rows](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.SpinRow.html) contain an embedded GtkSpinButton
> * [Property rows](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/style-classes.html#property-rows) invert their title and subtitle style same way as Nautilus and Loupe properties have been doing
> ![](7fc30668003995bdd59854ceb03ab63a681c9541.png)

### Software [↗](https://gitlab.gnome.org/GNOME/gnome-software/)

Lets you install and update applications and system extensions.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) says

> Milan Crha has been re-triaging old GNOME Software issues, fixing problems in PackageKit, and smoothing off a lot of sharp edges ready for GNOME Software 44.1.

# GNOME Circle Apps and Libraries

[Hugo Olabera](https://matrix.to/#/@hugolabe:matrix.org) announces

> This week I released version 2 of Wike, which brings a huge revamp to the user interface and also introduces some new features. These are the main changes:
> 
> * Migration to GTK4 + Libadwaita.
> * The search input is now always visible. It also includes an indication of the selected language and a button that gives access to the search settings.
> * Type to search. Just start typing to find articles.
> * New side panel that provides access to the article table of contents, languages, bookmarks and history. It can be used in floating or pinned mode.
> * Bookmarks with multiple lists.
> * New view menu that allows you to change the theme, zoom, typography and more.
> * Content view now defaults to system font.
> * Responsive design. The user interface now adapts to small screens, like phones.
> * Improved status pages (new tab, article not found...).
> * Use of flag icons to help identify languages. This can be turned off.
> * New printing option. This also allows export articles using the "Print to PDF" option.
> * New app icon.
> * Updated translations. Thanks to all translators!
> ![](TiYdxGRfLfOytXxQQWRRLOQX.png)
> ![](qkoWGUzwsaQlXhoMOEnnCuAB.png)

### Video Trimmer [↗](https://gitlab.gnome.org/YaLTeR/video-trimmer)

Trim videos quickly.

[Ivan Molodetskikh](https://matrix.to/#/@yalter:gnome.org) announces

> I published Video Trimmer v0.8.1. This is a small release that adds keyboard shortcuts, fixes a crash with some videos and updates the platform to GNOME 44.

### Pika Backup [↗](https://apps.gnome.org/app/org.gnome.World.PikaBackup/)

Keep your data safe.

[Sophie 🏳️‍🌈 🏳️‍⚧️ ✊](https://matrix.to/#/@sophieherold:gnome.org) reports

> A new version of Pika Backup is out. Part of the more than 20 additions and changes in version 0.6 are:
> 
> * Add status information to the list of background apps.
> * Automatically reconnect and retry backups in more situations than before.
> * Fix issues with not freeing up space when deleting older archives.
> * Correctly handle some rare situations like moved backup repositories.
> * Document that restoring from backup via Files does not preserve access rights.
> 
> One of our many goals for the next version is a dedicated restore feature. You can support the development with a [donation on Open Collective](https://opencollective.com/pika-backup) or [GitHub sponsors](https://github.com/sponsors/pika-backup/).

### gtk-rs [↗](https://gtk-rs.org/)

Safe bindings to the Rust language for fundamental libraries from the GNOME stack.

[Julian 🍃](https://matrix.to/#/@julianhofer:gnome.org) says

> The [properties macro](https://gtk-rs.org/gtk-rs-core/stable/latest/docs/glib/derive.Properties.html) of `gtk-rs-core` has been published two months ago.
> Now that the rough edges have been polished, I've updated the [gtk-rs book](https://gtk-rs.org/gtk4-rs/stable/latest/book/g_object_properties.html) too.
> In the case of the book, this let me [remove](https://github.com/gtk-rs/gtk4-rs/pull/1363/files) more than 300 lines.
> If you want to adapt your own app, you can also have a look at our [examples](https://github.com/gtk-rs/gtk4-rs/tree/master/examples).
> `custom_editable`, `custom_layout_manager`, `custom_orientable`, `expressions`, `list_box_model`, `rotation_bin` and `squeezer_bin` all take advantage of this macro.
> Thanks goes to ranfdev for doing most of the initial work and Bilal Elmoussaoui for getting it over the finish line.

# Third Party Projects

[gregorni](https://matrix.to/#/@gregorni:hackliberty.org) says

> This week, I released version 1.2.0 of ASCII Images on [Flathub](https://flathub.org/apps/details/io.gitlab.gregorni.ASCIIImages)! In this release, French, Russian, Occitan and Italian translations were added, the app now remembers window size and state when closed, and the file manager can now open files with ASCII Images.
> ![](CesqofDIXXhsEqOSPFulEGAM.png)

### Tube Converter [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

An easy-to-use video downloader (yt-dlp frontend).

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) reports

> Tube Converter [V2023.4.1](https://github.com/nlogozzo/NickvisionTubeConverter/releases/tag/2023.4.1) is here! 
> 
> This week's release includes many many fixes for various crashes across both the GNOME and WinUI platforms, the application should be a lot more stable now. 
> 
> We also added the ability to open a download file directly, as well as the save directory :)
> ![](SJcemilDLpEozQwsMOmLoxzG.png)
> ![](HGKEraEQBdbaZdleubJHyyXP.png)

### Phosh [↗](https://gitlab.gnome.org/World/Phosh/phosh)

A pure wayland shell for mobile devices.

[Guido](https://matrix.to/#/@agx:sigxcpu.org) says

> We've merged support for a menu that appears when long pressing the power button on your phone when using phosh. The emergency call support didn't land yet, hence that button is dimmed. But the merge request for that is already pending and the gnome-calls side is in place too.
> ![](VMgIBWhUPmsqULsYdvizVxPh.png)

### Login Manager Settings [↗](https://gdm-settings.github.io)

Customize your login screen.

[Mazhar Hussain](https://matrix.to/#/@realmazharhussain:matrix.org) reports

> Login Manager Settings has moved from my user profile to a dedicated GitHub organization. The new website is [https://gdm-settings.github.io](https://gdm-settings.github.io/) and the new repo is https://github.com/gdm-settings/gdm-settings.git. It also has [a collective](https://opencollective.com/gdm-settings) on OpenCollective now.
> 
> Login Manager Settings [v3.0](https://github.com/gdm-settings/gdm-settings/releases/tag/v3.0) was released. It adds
> 
> * An "Always Show Accessibility Menu" option
> * Option to change cursor size
> * "What's new" section in 'About' window
> * A one time donation request dialog
> * A 'Donate' option in app menu
> 
> Also, proper names of themes are presented instead of their directory names.
> 
> Bugs that were fixed include
> 
> * One could choose a cursor theme as an icon theme.
> * The app failed to run on PureOS.
> 
> Login Manager Settings [v3.1](https://github.com/gdm-settings/gdm-settings/releases/tag/v3.1) was also released. Some translations were accidentally not included in v3.0. So, this is a very small point release, released only to include those translations.

### Denaro [↗](https://flathub.org/apps/details/org.nickvision.money)

A personal finance manager.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) announces

> Your favorite pizza man is here, announcing Denaro [V2023.4.0](https://github.com/nlogozzo/NickvisionMoney/releases/tag/2023.4.0) 🥳
> 
> This version includes many many new features and improvements for managing your accounts. First, there is a new Dashboard page that allows users to see information about all accounts at a quick glance. We also added the ability to assign colors to groups and customize decimal and group separators used per account. Many issues were also fixed, including issues importing information OFX and QIF files and random GTK crashes users were experiencing.
> 
> I'd like to thank @fsobolev , @DaPigGuy , and all of the translators and contributors who continue to make Denaro better every day ❤️
> ![](WXplQGoSsbkCfkNuEWDueEGD.png)
> ![](FUjawZRTJcYEWLcxEiFTmSkL.png)
> ![](ppuhnHQiOnJbcfLgViuSUVEs.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

