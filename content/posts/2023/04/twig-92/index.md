---
title: "#92 Image Printing"
author: Felix
date: 2023-04-21
tags: ["denaro", "pika-backup", "loupe", "cartridges", "workbench", "phosh", "authenticator", "tubeconverter", "graphs"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from April 14 to April 21.<!--more-->

# GNOME Incubating Apps

### Loupe [↗](https://gitlab.gnome.org/Incubator/loupe)

A simple and modern image viewer.

[Sophie 🏳️‍🌈 🏳️‍⚧️ ✊](https://matrix.to/#/@sophieherold:gnome.org) announces

> We have landed all features planned to make Loupe a suitable image viewer for GNOME Core. As a next step, we will primarily focus on removing bugs and polishing what we have.
> 
> While basic image editing features are very frequently requested, it's very unlikely it will make it into GNOME 45. Much work is needed on different levels, especially in the libraries that handle image formats. I want to work on that in the future, but this is nothing that should be done under time pressure. Please let me know if you are experienced with image formats and/or rust and want to help!
> 
> Since our last update, we have:
> 
> * Landed image printing.
> * Added ICC color profile support for JPEG, PNG, HEIC, and AVIF.
> * Filled an upstream fix for ICC profiles in PNGs.
> * Made the overlay buttons hide after inactivity.
> * Let Loupe correctly advertise to the system what images it supports.
> * Made dragging the image also work with the middle mouse button.
> * Optimized SVG rendering, showing a complete image more quickly when zooming for huge SVGs.
> * Allow to drag and drop multiple images into Loupe to browse through those images.
> * And finally made our CI builds three times faster on average.
> 
> Thanks to everyone who helped with contributions, bug reports, and advice.
> ![](e0b2d20afffad8b4f84a7f3926675d25759b9519.png)

# GNOME Circle Apps and Libraries

### Workbench [↗](https://github.com/sonnyp/Workbench/)

A sandbox to learn and prototype with GNOME technologies.

[Sonny](https://matrix.to/#/@sonny:gnome.org) announces

> Workbench 44 is out. Highlights are:
> 
> * Use GNOME platform/SDK 44
> * Fully sandboxed and now considered Safe by GNOME Software
> * An improved previewer
> * 18 new library entries
> 
> Thank you to all new contributors, specially GSoC and Outreachy applicants.
> ![](b854c5c8669eaf28e00fd4beb04587de527db04b.png)
> ![](ae97a26b628f50611aa1519cb528c95f714e8aa3.png)

### Pika Backup [↗](https://apps.gnome.org/app/org.gnome.World.PikaBackup/)

Keep your data safe.

[Sophie 🏳️‍🌈 🏳️‍⚧️ ✊](https://matrix.to/#/@sophieherold:gnome.org) says

> Pika Backup 0.6.1 is now available with some distribution specific bug fixes. We have fixed that archives could not be mounted on distributions that don't link `fusermount` to `fusermount3`, and removable drives can now be included in backups on all distributions again.

### Authenticator [↗](https://gitlab.gnome.org/World/Authenticator)

Simple application for generating Two-Factor Authentication Codes.

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) says

> [Authenticator](https://flathub.org/apps/details/com.belmoussaoui.Authenticator) 4.3.0 is out with one of the biggest code cleanup we have done since the port to GTK 4 & Rust. The end user shouldn't notice that many changes though other then a finally very accurate TOTP implementation.
> 
> * Fix compatibility with https://2fas.com/check-token/
> * backup: Add FreeOTP+ JSON format support
> * Allow importing from an image file containing a QR code
> * Add tests for all the supported backup formats
> ![](9072f7ef2edd92ca8d3c8a407696f657d2fd2a66.png)
> ![](e01b5cdae7f2fc174a96dec67c9d2715726c11f1.png)

# Third Party Projects

[cwunder](https://matrix.to/#/@ovflowd:gnome.org) reports

> Support for Chromecast on GNOME Network Displays finally landed and it's scheduled to be released with GNOME 45.
> 
> More details about the Merge Request: [https://gitlab.gnome.org/GNOME/gnome-network-displays/-/merge_requests/171](https://gitlab.gnome.org/GNOME/gnome-network-displays/-/merge_requests/171).

[Sonny](https://matrix.to/#/@sonny:gnome.org) announces

> Dino, a modern chat and video call app is now [available on Flathub](https://flathub.org/apps/details/im.dino.Dino).
> 
> Thanks to [Aman9das](https://github.com/Aman9das), [Link Mauve](https://linkmauve.fr/) for the manifest and [bl00mber](https://github.com/bl00mber) for [`--socket=gpg-socket`](https://github.com/flatpak/flatpak/pull/4958).
> 
> As a side note, thanks to its isolation, Flatpak is the first distribution of Dino to include fix for a CVE in one of it's dependency.
> ![](6e0008c8542b0b64017e9f39a24d0e3323d53c31.png)

[Aleks Rūtiņš](https://matrix.to/#/@aleksrutins:matrix.org) reports

> This week, I released version 0.1 of Kaste, a stupidly simple abstraction for data storage in GNOME applications. [Check it out!](https://gitlab.gnome.org/aleksrutins/kaste)

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) says

> Nickvision apps (Denaro, Tube Converter, Tagger) have a new home, the [NickvisionApps GitHub Organization](https://github.com/NickvisionApps). This organization helps us keep all of ours apps in one place and manage our team as a whole. New website soon to follow :)
> ![](SMTVbHmTziIeyLyXTkathBVU.jpg)

### Tube Converter [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

Get video and audio from the web.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) announces

> Tube Converter [V2023.4.2](https://github.com/NickvisionApps/TubeConverter/releases/tag/2023.4.2) is here! 
> 
> This release includes some new features that make the downloader even more customizable. First, we added a configurable speed limit that can be used to limit individual download speeds. Furthermore, we added support for using aria2 as the downloader backend if users choose. 
> 
> We also fixed some issues, including an issue where not all logs were shown for a download.
> ![](HuupVuGVjYoKwoEulSvVHcsG.png)

### Denaro [↗](https://flathub.org/apps/details/org.nickvision.money)

Manage your personal finances.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) reports

> Your favorite pizza man is here with a new release of Denaro! [V2023.4.1](https://github.com/NickvisionApps/Denaro/releases/tag/2023.4.1)
> 
> First off, a new Notes field was added to transactions to allow users to write whatever extra notes they may choose for a transaction. We also added support for choosing to export all account information or just the transactions shown in the current filtered and sorted view. Support for displaying native digits instead of just Latin ones when working with numeric amounts was also added.
> 
> Besides these new features, we also fixed many issues including an issue where custom separators would not parse correctly and an issue where CSV files were not exported in the correct format.
> 
> Denaro Docs, available from the Help action in the main menu, are also now available to be [translated via Weblate!](https://hosted.weblate.org/projects/nickvision-money/docs/)
> ![](eoBmugXBjjwYtjWGwVuKFXVG.png)

### Phosh [↗](https://gitlab.gnome.org/World/Phosh/phosh)

A pure wayland shell for mobile devices.

[Guido](https://matrix.to/#/@agx:sigxcpu.org) says

> This week we finally merged [Thomas](https://gitlab.gnome.org/CoderThomasB) support for handling emergency calls into Phosh.  Whenever gnome-calls reports available emergency numbers you can now either select them from an emergency contact list or dial them via the keypad (doesnt' matter if your phone is locked or not). You currently have to opt in via a configuration option as we want to get more testing feedback before enabling it by default - it would be bad if that feature failed you in a real emergency. Apart from that we landed some visual tweaks related to notifications, system modal dialogues and screenshots.
> ![](fXaoBODEuGbbgSqbMbqgXnqB.png)
> ![](DZfnIzrfRyBYltbkiLUOPaTn.png)
> ![](tFyByFFSvboAsfuvzPxhCdiL.png)

### Graphs [↗](https://graphs.sjoerd.se)

Plot and manipulate data in a breeze!

[Sjoerd Stendahl](https://matrix.to/#/@sjoerdb93:matrix.org) says

> I am happy to announce that we have finally released Graphs 1.5! Graphs is a simple, yet powerful tool that allows you to plot and manipulate your data with ease. Data can be imported from column-files stored in most formats, or generated using an equation. You can easily manipulate or transform the data in various ways such as cutting, translations, multiplications, derivatives, fourier transforms and more. Some major highlights of this release include:
> 
> * A major overhaul of the UI: Graphs now tries to follows the GNOME HIG, giving a nice and consistent experience in GNOME
> * A fully featured plot style editor: The style of your plot can now be saved in our now style editor where setting such as plot colours, ticks, the grid and even the default color cycle are all stored. A new plot style can easily be applied on an open project to quickly change your graph styling for a specific purpose
> * Saving projects: Projects can now be saved as a single file, which can be opened later for further use. Allowing you to continue where you left off.
> * Improved clipboard behaviour: Adding new data no longer resets the clipboard. And more actions are now included in the clipboard as well, so actions such as changing line colours, adding data, removing data and changing line names can all be undone/redone using the clipboard features.
> * Tons of bug fixes and changes under the hood. Most code has been updated since the previous release.
> * And many more changes, such as support for Panalytical .xrdml files, the ability to set the canvas limits from Plot Settings, the ability to reorder data by drag-and-drop within the side-panel, and a bunch of additional toast pop-ups to give the user feedback on specific occasions.
> 
> Thanks to everyone involved in helping out. Specifically [@cmkohnen](https://github.com/cmkohnen), who's been co-developing Graphs with me, and I'd also want to thank Tobias Bernard  for his useful feedback and help with the user interface! The latest release of Graphs can be found on [Flathub](https://flathub.org/apps/details/se.sjoerd.Graphs).
> ![](HXhfZLYGBovJPlmlrOeAKfwN.png)
> ![](XccVsYXElVYuUgBdFvDpxEma.png)
> ![](pholRlGfLeNTRIckQmjKmAyl.png)

### Cartridges [↗](https://github.com/kra-mo/cartridges)

Launch all your games

[kramo](https://matrix.to/#/@kramo:matrix.org) announces

> I just released Cartridges [1.4](https://github.com/kra-mo/cartridges/releases/v1.4)!
> 
> This update brings:
> * Support for animated covers
> * A redesigned details view
> * The ability to search for games on various databases
> 
> Check it out on [Flathub](https://flathub.org/apps/details/hu.kramo.Cartridges)!
> {{< video src="VawnqnHBFuJIHjfXihtmymCb.mp4" >}}

# Documentation

[lwildberg](https://matrix.to/#/@lw64:gnome.org) announces

> This week dark mode support for valadoc.org was merged! Check it out and enjoy looking up the beautiful API references with even more comfort! This was a long standing effort by colinkiama and others and now its finally there!
> ![](06376f082b7a67a36e87a115445ce605d5e75069.png)

# GNOME Foundation

[Kristi Progri](https://matrix.to/#/@kristiprogri:gnome.org) reports

> Linux App Summit (LAS) is just a few days away and we’re excited to welcome you to Brno on April 22-23. We’ve been working hard to make sure the conference runs smoothly, so if you’re around, please feel free to join us. There’s still time to register at https://conf.linuxappsummit.org/event/5/registrations/.
> 
> In addition to LAS, we’re also busy organizing our big upcoming event, GUADEC. We’ll be publishing the full schedule and opening registration soon, so stay tuned!
> 
> I’m also looking for someone to assist with leading the annual report project. If you’re interested in helping out, please reach out to me at kprogri@gnome.org. Your support would be greatly appreciated.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

