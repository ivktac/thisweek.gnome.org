---
title: "#100 One Hundred"
author: Felix
date: 2023-06-16
tags: ["graphs", "paper-plane", "dev-toolbox", "gradience", "vala", "gnome-control-center", "metronome", "nautilus", "tubeconverter", "eartag", "libadwaita", "rnote", "weather-oclock", "denaro", "escambo", "eyedropper"]
categories: ["twig"]
draft: false
---

One hundred weeks ago, on Friday 16 July 2021, **"This Week in GNOME"** was launched - the first post was ["#1 Scrolling in the Dark"](https://thisweek.gnome.org/posts/2021/07/twig-1/).

Since then TWIG has grown into a vibrant community, and has become a weekly ritual for many people—both for developers who share their work, and for curious readers who want to follow the development of GNOME.

![](twig100.png)

TWIG is not the result of a few individuals, but of a large, diverse, dedicated, motivated, fantastic group of contributors who are all behind the GNOME project!

The picture above shows the various projects people have been working on over the last few years. Since the beginning, over a thousand news submissions have been made to the Matrix room [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org). A big thank you to each and every contributor ❤️! Can you recognise your own project in the picture?

- You can download the anniversary image in full resolution from [here](twig100.png).

In line with the anniversary, we have also set a new record. Never before have there been so many submissions as this week. Let's celebrate this success together! And now, as usual, to the happenings across the GNOME Project in the week from 09 June to 16 June!

# GNOME Core Apps and Libraries

### Files [↗](https://gitlab.gnome.org/GNOME/nautilus)

Providing a simple and integrated way of managing your files and browsing your file system.

[antoniof](https://matrix.to/#/@antoniof:gnome.org) reports

> Files was an early adopter of the new AdwOverlaySplitView and AdwToolbarView widgets, featuring updated visual style, courtesy of Chris 🌱️ .
> 
> Work on improving file searching experience continues on full steam. When searching from grid, it no longer changes to a list. The goal was to show the location of recursive results, but now the grid view has grown the ability to display the path itself. This avoids the abrupt change of view from grid to list.
> ![](826c36f2e79f5fddfa0f82a417d7bff8ea5c9c04.png)

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alice (she/they)](https://matrix.to/#/@alexm:gnome.org) says

> Now that all the replacements are in, the old adaptive widgets in libadwaita have been deprecated:
> 
> * `AdwLeaflet`
> * `AdwFlap`
> * `AdwSqueezer`
> * `AdwViewSwitcherTitle`
> 
> I've published a blog post detailing the rationale and the replacements: https://blogs.gnome.org/alexm/2023/06/15/rethinking-adaptivity/

### Settings [↗](https://gitlab.gnome.org/GNOME/gnome-control-center)

Configure various aspects of your GNOME desktop.

[Chris 🌱️](https://matrix.to/#/@brainblasted:gnome.org) says

> The Default Apps panel in Settings got a new look, using libadwaita's list widgets.
> ![](4bc46bbb7f1951f717e227aa21347ad0d3192506.png)

### Vala [↗](https://gitlab.gnome.org/GNOME/vala)

An object-oriented programming language with a self-hosting compiler that generates C code and uses the GObject system.

[lwildberg](https://matrix.to/#/@lw64:gnome.org) reports

> Since a week the new nightly version of the Vala Sdk is published and everyone can use it! Its hosted on the GNOME nightly flatpak remote. Its the same as if you use the stable Sdk, except the id is `org.freedesktop.Sdk.Extension.vala-nightly`. Currently there is a `22.08` branch, which works with the latest gnome and freedesktop runtime. The nightly Sdk includes the git version of the Vala compiler and the Vala language server updated every day. So if you want to try out new features or need a recent bug fix, use the nightly Sdk! You can read more also on [discourse](https://discourse.gnome.org/t/vala-moves-from-the-base-org-gnome-sdk-to-a-separate-sdk-extension/15665). Great thank you to Jordan Petridis for setting everything up!

# GNOME Development Tools

[Chris 🌱️](https://matrix.to/#/@brainblasted:gnome.org) reports

> If you're interested in using Typescript for GNOME apps, you can now use the new [Typescript SDK Extension](https://github.com/flathub/org.freedesktop.Sdk.Extension.typescript). I've also published a [Typescript Template](https://gitlab.gnome.org/BrainBlasted/gnome-typescript-template/) you can use as a starting point for your apps.
> ![](c82c1707f290f91c0ee61380b8bdccd31fa8c47b.png)

# GNOME Circle Apps and Libraries

### Eyedropper [↗](https://flathub.org/apps/details/com.github.finefindus.eyedropper)

Pick and format colors.

[FineFindus](https://matrix.to/#/@finefindus:matrix.org) announces

> Due to the large number of changes since the last release, I've released a beta for the next version of Eyedropper. 
> Highlights of this release include
>  * Visual feedback when entering a colour format
>  * Support for input of all formats
>  * Allow searching for colours in the activity overview
>  * Choice of file format when exporting palettes
>  * Visual fixes
>  
> Visit the [GitHub release](https://github.com/FineFindus/eyedropper/releases/tag/v0.7.0-beta) for the full changelog or download the beta from Flathub and share your feedback.
> ![](wnUjOkfAnGpmKKWxZLgFTKaI.png)

### Metronome [↗](https://gitlab.gnome.org/aplazas/metronome)

Practice music with a regular tempo.

[FineFindus](https://matrix.to/#/@finefindus:matrix.org) reports

> Metronome 1.3.0 has been released! This version fixes a bug with off-beat ticks and allows to change the BPM while playing. We've also added tooltips to several buttons. It is available on [Flathub](https://flathub.org/apps/com.adrienplazas.Metronome).

### Ear Tag [↗](https://gitlab.gnome.org/knuxify/eartag)

Edit audio file tags.

[knuxify](https://matrix.to/#/@knuxify:cybre.space) says

> [Ear Tag 0.4.0](https://gitlab.gnome.org/knuxify/eartag/-/releases/0.4.0) (and its subsequent bugfix release, [0.4.1](https://gitlab.gnome.org/knuxify/eartag/-/releases/0.4.1)) have been released! This new version brings three new features:
> 
> * Renaming files using a specific pattern
> * Identifying files using AcoustID
> * The option to set a full release date (YYYY-MM-DD) instead of just the year
> 
> You can get the latest version from [Flathub](https://flathub.org/apps/app.drey.EarTag).
> ![](CMXXRDfPVYrVOaCWuXxKMlxN.png)
> ![](dPAhdtGlgXoblyOxSwlhWkOw.png)

# Third Party Projects

[Daniel Wood](https://matrix.to/#/@dubstar_04:matrix.org) reports

> This week Design, the 2D CAD appication gets support for:
> 
> * Line Types (Dotted, Dashed, Center etc...)
> * Exporting to alternative DXF versions
> 
> Thank you to the GNOME Design community for their support.
> 
> Design 44-alpha4 is available from Flathub:
> https://flathub.org/apps/details/io.github.dubstar_04.design
> ![](NtNoosSVHHKdJBQyHAlFTxbx.png)

[Diego Iván](https://matrix.to/#/@dimmednerd:matrix.org) says

> This week, I also worked on a new release of Flowtime. Flowtime is a productivity app for the time management method with the same name, especially useful for the tasks you love, as other methods like Pomodoro may interrupt you when you are at your most concentrated. Flowtime lets you work all the time you want, to then take a break that lasts a fifth of the time you worked. It has various useful features, which include:
> 
> * Statistics: Keep track of the time you've worked, the time you've taken a break and which is you most productive day of the week.
> * Small View: Reduce the clutter in the Flowtime window by activating a mode that shows just the timer.
> * Run in the background: Close the window to reduce distractions while your timer is still running, check your progress in GNOME Shell's background apps panel.
> * Customizable factor to compute break time: If a fifth of the time you've worked does not fit your work flow, you can change it from a half of the time up to a tenth.
> 
> You can download it from [Flathub](https://flathub.org/apps/io.github.diegoivanme.flowtime)!
> ![](mgDosHuFwWiKrxIgbetZaJlP.png)
> ![](ATqBaNxxdtSiKNIafnbZqKlh.png)

[Iman Salmani](https://matrix.to/#/@imansalmani:matrix.org) reports

> [IPlan](https://github.com/iman-salmani/iplan) 1.5.0 is now out!
> 
> Changes this week contains:
> * The task row shows the description, subtasks, due date, and reminder
> * New design for task rows
> * Add task button below tasks list
> * One week per calendar page
> * Today and No Date buttons on top of the date picker
> * Click on a searched task will open the task window
> * Auto start (if the user lets the app run in the background) and checking for reminders in the startup
> * Czech, French and Brazilian Portuguese Translations Added or updated thanks to Amerey.eu, Irénée Thirion, and Fúlvio Leo
> * Code refactoring, Bug fixes, and UI improvements
> 
> you can get it from [Flathub](https://flathub.org/apps/ir.imansalmani.IPlan).
> ![](pqJjGYFByyBHrYSwFSvbCfHD.png)
> ![](FequkINMUXfcaZxfVgliYwsU.png)

[Khaleel Al-Adhami](https://matrix.to/#/@adhami:matrix.org) reports

> Introducing Footage; a tool to rotate, flip, crop, trim, mute, and export your video to a format of your choice! It's available on [Flathub](https://flathub.org/apps/io.gitlab.adhami3310.Footage)
> ![](wvmaLCRwzoKSuhaBvPGhbuKJ.png)

[Khaleel Al-Adhami](https://matrix.to/#/@adhami:matrix.org) says

> New Impression release: 2.0. We decided to drop the major versions to simplify versioning. This release includes visual enhancements, automatic updates of the available drives list, explicit drive selection before flashing, and more translations, making Impression available in a total of 8 languages. Huge thanks to Brage for helping out so much with the app! It's now available on [Flathub](https://flathub.org/apps/io.gitlab.adhami3310.Impression)

[Aleks Rutins](https://matrix.to/#/@aleksrutins:matrix.org) announces

> Today, I implemented shared buckets in Kaste. Check it out at https://gitlab.gnome.org/aleksrutins/kaste!

[0xMRTT](https://matrix.to/#/@oxmrtt:vern.cc) reports

> Bavarder 0.2.4 has been released with a few improvements
> 
> * Faster baichat
> * Better internal structure wich is more modular
> * Improved translations
> * Better UI

[dlippok](https://matrix.to/#/@dlippok:matrix.org) says

> This week I released version 1.0.0 of Photometric Viewer, a utility software for viewing EULUMDAT and IESNA photometric files. Photometric files are used in the lighting industry, CAD and 3D graphics for describing properties and light distributions of lamps and luminaires. The software is available on Snapcraft and Flathub.
> 
> Project page: https://flathub.org/apps/io.github.dlippok.photometric-viewer
> ![](zRGTPsIkzqSRLUwtwxTTEABc.png)

[mrvladus](https://matrix.to/#/@mrvladus:matrix.org) reports

> This week I've released List 44.6. It's a simple To Do application for your daily tasks. You can add tasks and sub-tasks, edit them, move around and mark as completed. That is pretty much it!
> This version includes:
> * History of deleted tasks
> * Button to undo deletion
> * UI and Accessibility improvements
> It is available on [Flathub](https://flathub.org/apps/io.github.mrvladus.List)
> ![](JBbGsNkDNzYXecGiyxDyBQQS.png)

[xjuan](https://matrix.to/#/@xjuan:gnome.org) reports

> [Cambalache](https://gitlab.gnome.org/jpu/cambalache) 0.12.0 released!
> New features:
>  - User Templates: use your templates anywhere in your project
>  - Workspace CSS support: see your CSS changes live
>  - GtkBuildable Custom Tags: support for styles, items, etc
>  - Property Bindings: bind your property to any source property
>  - XML Fragments: add any xml to any object or UI as a fallback
>  - Preview mode: hide placeholders in workspace
>  - WebKit support: new widget catalog available
>  - External objects references support
>  - Add support for GdkPixbuf, GListModel and GListStore types
> 
> You can read more about in [here](https://blogs.gnome.org/xjuan/2023/06/16/cambalache-0-12-0-released/)
> ![](016df590facc944468b772442cbd579a846451fb.gif)
> ![](4204be3876a3c8f3d46a26e153264da6c2e65049.gif)

[Diego Iván](https://matrix.to/#/@dimmednerd:matrix.org) reports

> This week, and after a three month hiatus in development, I published the second major release of [Paper Clip](https://flathub.org/apps/io.github.diegoivan.pdf_metadata_editor) (formerly PDF Metadata Editor), with various improvements since it was last featured in TWIG, which include:
> 
> * New icon! Thanks to Brage Fuglseth for the neat icon!
> * Fix many UI paper cuts to better fit the GNOME HIG. Thanks to Tobias Bernard for his recommendations!
> * The main view now has a thumbnail of the document instead of a symbolic icon
> * The details window has been removed and its contents have been moved to the bottom of the main view
> ![](WkQukRlUMnVNxZqfkmFxJnpT.png)

[Felipe Kinoshita](https://matrix.to/#/@fkinoshita:gnome.org) reports

> This week I released [Wildcard](https://flathub.org/apps/io.github.fkinoshita.Wildcard), a simple development utility for testing/practicing regular expressions!
> ![](ba7eddf44a959f64337b27bfb917e3601796418d.png)

### Tube Converter [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

Get video and audio from the web.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) reports

> Tube Converter [V2023.6.1](https://github.com/NickvisionApps/TubeConverter/releases/tag/2023.6.1) is here! This release is packed with new features and fixes that make Tube Converter faster, more customizable, and easier to use. 
> 
> Continued thanks to @fsobolev and @DaPigGuy who continuously work with me to implement and test all of these features. @soumyaDghosh for managing snap and all other contributors, feature requesters, bug testers, and translators who continue to make Tube Converter the best that it can be :) ❤️
> 
> Here's the full changelog:
> * Added authentication options when adding a download if needed
> * Added a Keyring to store credentials that can be used when authenticating
> * Added the advanced download option to specify a specific timeframe of a media to download
> * Added the ability to embed chapters in a download
> * Added the ability to turn on automatically cropping audio thumbnails
> * Added the ability to specify a comma-separated list of language codes for subtitle downloading
> * Improved the speed of playlist validation
> * The "Number Titles" switch's state will now be remembered and used again for future downloads
> * The previous "Video Resolution" will be remembered and pre-selected if available for future downloads
> * Comment, Description, Synopsis and Purl fields will no longer be embedded in metadata
> * If a download fails and was not stopped, Tube Converter will automatically retry it one more time
> * Fixed an issue where some websites were not validated
> * Fixed an issue where the incorrect file extension was sometimes shown for generic downloads
> * Updated translations (Thanks everyone on Weblate!)
> ![](DkCEhKkacCUgbpFsLFAmmTre.png)

### Rnote [↗](https://github.com/flxzt/rnote)

Sketch and take handwritten notes.

[flxzt](https://matrix.to/#/@flxzt:matrix.org) reports

> A new version "v0.7.0" for [Rnote](https://github.com/flxzt/rnote), a vector-based sketching and handwriting app is out! Notable new features: a focus mode that hides the toolbars for a better experience on devices with lower resolution, inertial touch-scrolling, a dedicated zoom-tool and a lot of [other improvements](https://github.com/flxzt/rnote/releases/tag/v0.7.0)

### Paper Plane [↗](https://github.com/paper-plane-developers/paper-plane)

Chat over Telegram on a modern and elegant client.

[marhkb](https://matrix.to/#/@marcusb86:matrix.org) reports

> For the 100th issue of TWIG, we are pleased to announce that Telegrand has been renamed to [Paper Plane](https://github.com/paper-plane-developers/paper-plane). In addition, we have now started distributing releases on [Flathub Beta](https://github.com/paper-plane-developers/paper-plane#flathub-beta).
> 
> Among the recent changes are:
> 
> * The chat history now has a background image, which is animated when new messages arrive, thanks to [yuraiz](https://github.com/yuraiz).
> * A bug was fixed that lead to an application crash if selecting non-existing chats from the contacts window, thank to [alissonlauffer](https://github.com/alissonlauffer).
> * Avatars of deleted accounts now have a ghost icon set, thanks to [karl0d](https://github.com/karl0d).
> * The application will now be built with more aggressive compiler options, which should result in a smaller and maybe faster binary.
> * Translation updates
> ![](nRAiXqESJvXZwyGILsRnTlqn.png)

### Graphs [↗](https://graphs.sjoerd.se)

Plot and manipulate data in a breeze!

[Sjoerd Stendahl](https://matrix.to/#/@sjoerdb93:matrix.org) reports

> This week we released version 1.6.0 of Graphs, which spots a number of bugfixes as well as some quality of life changes. Some highlights of this release include:
> 
> * Full localization support, with full translations available in Dutch, Turkish (thanks to [sabriunal](https://github.com/sabriunal)) and Swedish
> * Action dialogs now follow the GNOME HIG
> * The behavior of the axes limits have been improved, with fewer unnecessary resets
> * The axis limits are now persistent when loading saved projects
> * Regular and advanced import have been merged to offer a single mode to add new data
> * Graphs now functions properly on high DPI displays with scaled resolutions
> * And many general bug fixes as well as refactors under the hood
> * Graphs is now officially available as [Snap](https://snapcraft.io/graphs) package as well (thanks [soumyaDghosh](https://github.com/soumyaDghosh)).
> 
> The latest version of Graphs can be found on [Flathub](https://flathub.org/apps/se.sjoerd.Graphs) or on [Snapcraft](https://snapcraft.io/graphs)!
> While there is no time schedule, planned features for coming updates include the ability to add objects to the plots themselves, and functionality for data regression using arbitrary equations. If you want to contribute to Graphs or have any issues or feedback, please file a ticket at  [the Github page](https://github.com/Sjoerd1993/Graphs/issues).
> 
> Thanks to everyone involved in making this release possible. Not the least to my co-developer [cmkohnen](https://github.com/cmkohnen). 
> 
> And congratulations to the whole community with the 100th issue of TWIG! Just a few weeks ago we reached another milestone with a total of 1000 submissions. 
> With our cumulative efforts, there's no denying the data that the year of the Linux desktop will soon be upon us ;-)
> ![](XoruMwkfLbNBIGLkUjCfUVZL.png)

### Gradience [↗](https://github.com/GradienceTeam/Gradience)

Change the look of Adwaita, with ease.

[Daudix UFO](https://matrix.to/#/@daudix_ufo:matrix.org) reports

> Gradience Team is pleased to announce the first beta release of the upcoming 0.8.0 version after 4+ months (128 days). This release brings the long-awaited feature of GNOME Shell theming 🎉 along with several enhancements:
> 
> * 🔄 Added ability to use JSDelivr instead of GitHub for downloading presets
> * ✨ Added mnemonics to view switchers
> * ⌨️ Added keyboard shortcuts
> * 🖼️ Small tweaks to the icon
> * 🌐 Updated translations
> * 🐛 Bug fixes
> 
> A list of blocking issues for the release of version 0.8.0 can be found in the [Gradience milestone on GitHub](https://github.com/GradienceTeam/Gradience/milestone/12). All contributions are welcome!
> 
> The beta version will soon be available on Flathub Beta for testing. Stay tuned!
> ![](lTmezKRZoLwJRCuOOUxTGEtK.png)

### Escambo [↗](https://github.com/CleoMenezesJr/escambo)

Test and develop APIs

[Cleo Menezes Jr.](https://matrix.to/#/@cleomenezesjr:matrix.org) announces

> In celebration of TWIG's 100th issue, **Escambo** releases version 0.1.2.
> 
> Since the last version **Escambo** has received some improvements such as:
> 
> * Fix inability to select response code
> * Update to Blueprint to 0.8.1
> * Update Brazilian Portuguese translation
> * Add German and Turkish translation
> * Be able to create Header with empty value
> * Check for duplicate data keys
> * Add new way to add new Body Form Data
> * Add new flat design
> * Fixed some toast notifications
> and many more...
> 
> New contributors:
> [Pablo Emídio S.S.](https://github.com/PabloEmidio), [FineFindus](https://github.com/FineFindus), [0xMRTT](https://github.com/0xMRTT),  [Sabri Ünaal](https://github.com/sabriunal), [gregorni](https://github.com/gregorni).
> 
> New sponsors:
> [baarkerlounger](https://github.com/baarkerlounger),  [Jorge O. Castro](https://github.com/castrojo).
> 
> _Escambo is undergoing a redesign process and great news is coming in the future._
> 
> [Get it on Flathub](https://flathub.org/apps/io.github.cleomenezesjr.Escambo)

### Dev Toolbox [↗](https://github.com/aleiepure/devtoolbox)

Development tools at your fingertips.

[Aleie000](https://matrix.to/#/@aleiepure:matrix.org) reports

> Dev Toolbox v1.1 has landed on [Flathub](https://flathub.org/it/apps/me.iepure.devtoolbox)! This release upgrades the platform to GNOME 44 and introduces many new tools and improvements. Thank you for all the suggestions I received since the last release and for the translators' work on [Weblate](https://hosted.weblate.org/engage/devtoolbox/).
> 
> Here's a list of everything that has changed:
> * Upgraded to GNOME 44
> * Now follows the system theme
> * Timestamp: ISO8601/RFC3339 output format and timezone support
> * Ported UIs to blueprint
> * Improved icon
> * New tool: JS Formatter
> * New tool: Certificate Parser
> * New tool: CSR Generator
> * New tool: Random Generator
> * New tool: CRON generator
> * New tool: chmod calculator
> * New tool: QR-code generator
> * New tool: JSON validator
> * New tool: CSS Formatter
> * New translations: Czech (@Amereyeu), Spanish (Óscar Fernández Díaz on Weblate and @gallegonovato), Norwegian bokmål (@comradekingu) and Portuguese (@SantosSi)
> ![](UKyVTaKZhVomURxOxOkQUtXf.png)

### Denaro [↗](https://flathub.org/apps/details/org.nickvision.money)

Manage your personal finances.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) announces

> Denaro [V2023.6.0](https://github.com/NickvisionApps/Denaro/releases/tag/2023.6.0) is here! This release features many new feature, fixes, and design improvements giving Denaro a better look!
> 
> Here's the full changelog:
> * Added a new account setup dialog to make it easier to configure new accounts
> * Added the ability to remove recent accounts from the list
> * Denaro will now suggest autocompletions for transaction descriptions
> * Moved deleting groups and transactions from their rows to their dialogs
> * Changed the default sorting order of new accounts to last to first by date
> * Amounts shown in the sidebar will now reflect that of the transactions shown in view
> * Fixed an issue importing CSV files
> * Fixed an issue causing Denaro to crash when working with a large number of transactions
> * Improved UI/UX
> * Updated translations (Thanks to everyone on Weblate)!
> ![](UaMzWJgVpmOYCTzllsUubVNe.png)

# Shell Extensions

[Just Perfection](https://matrix.to/#/@justperfection:gnome.org) says

> Thanks to Yuri Konotopov, [GNOME extensions website](https://extensions.gnome.org/) now allows users to delete their account from the [account settings page](https://extensions.gnome.org/accounts/settings). User account will be deleted in 7 days after the request.

[kramo](https://matrix.to/#/@kramo:matrix.org) says

> The [One-Thing GNOME Extension](https://github.com/one-thing-gnome/one-thing) now supports GNOME 44!

### Weather O'Clock [↗](https://extensions.gnome.org/extension/5470/weather-oclock/)

Display the current weather inside the pill next to the clock.

[Cleo Menezes Jr.](https://matrix.to/#/@cleomenezesjr:matrix.org) announces

> [Weather O'Clock](https://extensions.gnome.org/extension/5470/weather-oclock/) and [Auto Activities](https://extensions.gnome.org/extension/5500/auto-activities/) extensions received links for donations.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!


