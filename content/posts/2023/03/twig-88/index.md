---
title: "#88 Forty-four!"
author: Felix
date: 2023-03-24
tags: ["commit", "loupe", "identity", "share-preview", "dialect", "libadwaita", "gdm-settings", "cawbird", "auto-activities", "geopard"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from March 17 to March 24.<!--more-->

**This week we released GNOME 44!**

![](44_banner.png)

This new major version of GNOME is full of exciting changes, such as major improvements to the Settings app, a better quick settings menu, a streamlined Software app - and of course much more. More information can be found in the [GNOME 44 release notes](https://release.gnome.org/44/).

Readers who have been following this site for a few weeks will already know some of the new features. If you want to follow the development of GNOME 45 (Fall 2023), keep an eye on this page - we'll be posting exciting news every week!

# GNOME Core Apps and Libraries

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Jamie](https://matrix.to/#/@itsjamie9494:gnome.org) announces

> AdwAboutWindow has a [`new_from_appdata`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/ctor.AboutWindow.new_from_appdata.html) constructor now, allowing creation of AdwAboutWindow from valid AppStream metadata
> ![](181a5a37d6a7890e9a01e449f397db3690112e85.png)

# GNOME Incubating Apps

### Loupe [↗](https://gitlab.gnome.org/Incubator/loupe)

A simple and modern image viewer.

[Sophie 🏳️‍🌈 🏳️‍⚧️ ✊](https://matrix.to/#/@sophieherold:gnome.org) says

> Loupe now supports showing animated GIFs, PNGs, and WebPs. The HEIC image format (x265 codec) can now be separated into an extension that is automatically loaded if available. This is primarily due to [pending software patents](https://en.wikipedia.org/wiki/High_Efficiency_Video_Coding#Patent_licensing) that might apply in some countries. Last but not least, the image format detection has been unified. Loupe will now, in many situations, be able to load images with the wrong extension and show the actual image format in the properties.
> {{< video src="9b47e4fa67af897f2889f9cba36682b9b12e2cd1.webm" >}}

# GNOME Circle Apps and Libraries

### Share Preview [↗](https://github.com/rafaelmardojai/share-preview)

Test social media cards locally.

[Rafael Mardojai CM](https://matrix.to/#/@rafaelmardojai:matrix.org) announces

> Share Preview 0.3.0 has been released in time for GNOME 44 and is available from [Flathub](https://flathub.org/apps/details/com.rafaelmardojai.SharePreview).
> 
> It includes the following changes:
> 
> * New scraping logger feature
> * Remember last selected social platform
> * Design improvements
> * Updated to use latest libadwaita features
> * Many small bug fixes
> * Updated translations
> ![](zbylChHZLocDLQvDruqpYcjc.png)

### Identity [↗](https://gitlab.gnome.org/YaLTeR/identity)

Compare images and videos.

[Ivan Molodetskikh](https://matrix.to/#/@yalter:gnome.org) says

> I released Identity v0.5. In this version I implemented the long-awaited side-by-side comparison mode! Images and videos can be arranged in a row or a column, and their position and zoom remain fully synchronized.
> 
> I also reworked mouse gestures for zooming, panning, and drag-and-drop, taking heavy inspiration from [Loupe](https://gitlab.gnome.org/Incubator/loupe). They make Identity much more pleasant to use.
> ![](e1d467972e77ea7f0b2ea40a3b18bb3033cea7fb.png)
> {{< video src="65d75612c9deaf06f505fd79b3bfec28a59fbcd7.mp4" >}}

### Dialect [↗](https://github.com/dialect-app/dialect/)

Translate between languages.

[Rafael Mardojai CM](https://matrix.to/#/@rafaelmardojai:matrix.org) announces

> This week we merged some new things on Dialect. The preferences window has been redesigned a bit. Now you can choose from multiple text-to-speech providers, currently Google and Lingva. We also added new translation providers for Bing and Yandex.
> ![](KfYEGXFoYrbCPEDrZaJKDIFK.png)

### Commit [↗](https://github.com/sonnyp/Commit)

An editor that helps you write better Git and Mercurial commit messages.

[Sonny](https://matrix.to/#/@sonny:gnome.org) says

> Commit message editor 4.0 is out
> 
> * GNOME 44
> * Ask for confirmation before dismissing changes
> * Show the name of the repository in the title bar
> * Prevent shortcut from saving empty commit (by axtloss)
> * Improve tooltips (by Felipe Kinoshita)
> * Design improvements

# Miscellaneous

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) reports

> After a few months, I finally got the time to do a release of the various design tools! The releases mostly contain bug fixes, translations updates & code cleanups for:
> * [Contrast](https://flathub.org/apps/details/org.gnome.design.Contrast)
> * [App IconPreview](https://flathub.org/apps/details/org.gnome.design.AppIconPreview)
> * [Icon Library](https://flathub.org/apps/details/org.gnome.design.IconLibrary)
> 
> Additionally [Symbolic Preview](https://flathub.org/apps/details/org.gnome.design.SymbolicPreview) now supports exporting the css classes used by symbolic icons.

# Third Party Projects

[swanux](https://matrix.to/#/@swanux:matrix.org) reports

> This week I [released](https://github.com/swanux/hbud/releases/tag/v0.4.2) version 0.4.2 of my simple mediaplayer HBud. As the version number indicates, it hasn't reached its final form yet, but with this version many problems have been ironed out. HBud is not intended to compete with any of the "big ones" (this being a one-man show, wouldn't even be possible), the goal is to provide a simple and convenient alternative for those with similar preferences to me. Check it out on [Flathub](https://flathub.org/apps/details/io.github.swanux.hbud).
> ![](hGMHjOBPzLPvRRvMWAqJOawJ.png)
> ![](fFgbwrHnNdPhMaJfixVeWqpQ.png)

### Geopard [↗](https://github.com/ranfdev/Geopard)

A Gemini client.

[ranfdev](https://matrix.to/#/@ranfdev:matrix.org) says

> ﻿Geopard has finally received a new release!
> Take a look at the changes:
> * Updated to latest libadwaita and GTK to improve performance and compatibility, including the use of the new Adw.TabOverview widget for improved tab management on small screens. 
> * Added support for opening files and gemini mime types from the command line. 
> * Added tab history menu on right click over arrows for easier navigation. 
> * Fixed issue with links without whitespace not working properly. 
> * Moved scrollbar to the edge of the window for a cleaner UI. 
> * Added tooltips for items in header bar to provide more information to users. 
> * Added more information to the About window to give users a better idea of the project's. 
> * Various fixes and refactorings. 
> 
> Download it from [flathub](https://flathub.org/apps/details/com.ranfdev.Geopard)

### Login Manager Settings [↗](https://realmazharhussain.github.io/gdm-settings/)

A settings app for the login manager GDM.

[Mazhar Hussain](https://matrix.to/#/@realmazharhussain:matrix.org) reports

> [Login Manager Settings](https://realmazharhussain.github.io/gdm-settings) [v3.beta.0](https://github.com/realmazharhussain/gdm-settings/releases/tag/v3.beta.0) was released.
> 
> Changes compared to the previous alpha release are:
> 
> * The flatpak runtime has been updated to version 44.
> * Fixed a bug where the app could not launch on PureOS.
> * A few code improvements were made.

### Cawbird [↗](https://ibboard.co.uk/cawbird/)

A native Twitter client for your Linux desktop.

[CodedOre](https://matrix.to/#/@coded_ore:matrix.org) reports

> After an overhaul of the system responsible for managing timelines, Cawbird 2.0 can now show and mark pinned posts on a users detail page.
> ![](bXmXMEFzamyMtTfpidusxige.png)

# Documentation

[Allan Day](https://matrix.to/#/@aday:gnome.org) reports

> With Libadwaita 1.3 officially released, GNOME's Human Interface Guidelines were updated to include [guidance on the new banner widget](https://developer.gnome.org/hig/patterns/feedback/banners.html). This replaces the guidance on info bars, which have been deprecated.

# Shell Extensions

### Auto Activities [↗](https://extensions.gnome.org/extension/5500/auto-activities/)

Show activities overview when there are no windows, or hide it when there are new windows.

[Cleo Menezes Jr.](https://matrix.to/#/@cleomenezesjr:matrix.org) announces

> In this new version, Auto Activities receives significant improvements, some of which are:
> 
> * Add preference to ignore last workspace
> * Hide New Window option is enabled by default.
> * Revamp preferences window
> ![](AwpxPzywYmVQoNAWDOqJWjYd.png)
> {{< video src="ZVciHQMmiiQhThynpGHWwDBC.mp4" >}}

# GNOME Foundation

[Rosanna](https://matrix.to/#/@zana:gnome.org) reports

> Work at the Foundation is humming along. For me, this week was a paperwork week. This involved:
> * Filing some paperwork with the state of California to keep us in good standing
> * Checking on the status of staff health insurance policy
> * Updating payroll
> * Working with our 401k accountants on some tax forms
> * Updating the employee handbook
> * Reimbursing some travel expenses
> * Forwarding some paperwork to the bank
> * Updating the accounting books
> * Invoicing some corporate sponsors
> * etc.
> 
> These are the types of things required to do on a regular basis to keep our foundation running.
> 
> In addition, some of the staff got together this week to start compiling a document on all the programming we currently do to address diversity. We hope of getting a better idea of where we currently stand and what we can do to improve.
> 
> Reminder: Get your talks for GUADEC submitted now! The deadline is March 27. More details at https://foundation.gnome.org/2023/02/24/guadec-2023-cfp/

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

