---
title: "#97 GNOME Latam 2023"
author: Felix
date: 2023-05-26
tags: ["pods", "denaro", "cartridges", "amberol", "gnome-calendar", "libadwaita", "tubeconverter"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from May 19 to May 26.<!--more-->

[Martín Abente Lahaye](https://matrix.to/#/@tchx84:matrix.org) reports

> Today, Friday May 26th, and tomorrow, Saturday May 27th, come [join us](https://events.gnome.org/event/136) at GNOME Latam 2023! This is a two days event celebrating our Latin GNOME community with speakers from all over the Americas.
> ![](LAFVhdEbvBlPdyOJOZepOrjU.png)

# GNOME Core Apps and Libraries

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alice (she/they)](https://matrix.to/#/@alexm:gnome.org) announces

> I merged [`AdwNavigationView`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.NavigationView.html). This is a widget that implements page-based navigation with an easier to use API than `AdwLeaflet`, and will eventually replace it
> {{< video src="f82e48cb6836c9dfcaeee1faec51d7c526598ab9.mp4" >}}

### Calendar [↗](https://gitlab.gnome.org/GNOME/gnome-calendar/)

A simple calendar application.

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) says

> GNOME Calendar just received a small facelift on top of the new widgets provided by libadwaita. It now features a better delineated sidebar, and the views have a uniform look too.
> ![](b0aba4f3e7bfba75af7d6ad60215f3ea20e70ea6.png)

# GNOME Circle Apps and Libraries

[Sonny](https://matrix.to/#/@sonny:gnome.org) reports

> This week, [Cartridges](https://apps.gnome.org/app/hu.kramo.Cartridges/) joined GNOME Circle. Cartridges is a simple game launcher for all of your games. Congratulations!
> ![](cc93854808c461503e48353f1ca4dea4ac94c241.png)

### Cartridges [↗](https://github.com/kra-mo/cartridges)

Launch all your games

[kramo](https://matrix.to/#/@kramo:matrix.org) says

> I just released Cartridges 1.5!
> 
> Extra Steam libraries are now detected and added automatically, executables are now passed directly to the shell, allowing for more complex arguments, and a lot of UX improvements have been made.
> 
> Oh yeah, and the app is now part of GNOME Circle!
> 
> Check it out on [Flathub](https://flathub.org/apps/hu.kramo.Cartridges)!
> ![](oRilyQhXldSmZSofMImUktrj.png)

### Amberol [↗](https://gitlab.gnome.org/ebassi/amberol/)

Plays music, and nothing else.

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) reports

> [Amberol 0.10.3](https://gitlab.gnome.org/World/amberol/-/releases/0.10.3) is now out! Not a lot of changes, but two nice bug fixes: the waveform for short songs is now appropriately sized; and the cover art with a portrait orientation does not get squished any more. Plus, as usual, lots of translation updates.

# Third Party Projects

[Iman Salmani](https://matrix.to/#/@imansalmani:matrix.org) reports

> [IPlan 1.2.0](https://github.com/iman-salmani/iplan) is now out!
> Changes:
> Subtasks, Bug fixes, and UI improvements
> * Task Window which contains task info, subtasks, and records
> * Translation to Persian and Turkish languages. thanks to Sabri Ünal
> * New toast message for moving back task from the tasks done list
> * Users are now able to pick emojis for their projects in the edit window
> * Projects and Tasks now can have descriptions
> ![](QzxvcoWrnoxEtFFXdSIzXcOO.png)
> ![](pnsMXMZjsfdJOqSEroBnPLjY.png)

[0xMRTT](https://matrix.to/#/@0xmrtt:envs.net) reports

> [Imaginer](https://imaginer.codeberg.page) 0.2.2 has been released with the ability to use [stable diffusion running locally](https://imaginer.codeberg.page/help/local/). It's also now possible to customize the filename and some bugs has been fixed.
> ![](7154c5a5b3b1e59d680af4efa3ac83051240a881.png)
> ![](14f0b49797da1c4429c4ef7cfe30b8634a6b15af.png)

[0xMRTT](https://matrix.to/#/@0xmrtt:envs.net) says

> [Bavarder](https://bavarder.codeberg.page) 0.2.3 has been released with the ability to use [a custom model](https://bavarder.codeberg.page/help/local/) which enable you to use either a model running on your computer or a custom API providing the model. The loading mechanism is now faster and some bugs has been fixed. 
> 
> You can download Bavarder from [Flathub](https://flathub.org/apps/io.github.Bavarder.Bavarder) or from either [GitHub](https://github.com/Bavarder/Bavarder) or [Codeberg](https://codeberg.org/Bavarder/Bavarder).
> ![](fc8ee260c2237f51c9ab36fbf8a28586c4809903.png)

[tfuxu](https://matrix.to/#/@tfuxu:matrix.org) says

> I've released Halftone, a simple app for lossy image compression using quantization and dithering techniques. Give your images a pixel art-like style and reduce the file size in the process with Halftone. You can check it out on [Github](https://github.com/tfuxu/Halftone).
> ![](UZwlGtdIxmMTuAGgXZlMnFmc.png)
> ![](TQHGxpbtnqucAFNgVNMUKMJs.png)
> ![](OcUvtnxbycxJqfxhXxUWUffE.png)

[JumpLink](https://matrix.to/#/@JumpLink:matrix.org) reports

> I am pleased to announce the release of [ts-for-gir](https://github.com/gjsify/ts-for-gir) v3.0.0 🚀 ts-for-gir is a powerful tool for generating TypeScript type definitions for GJS and GObject Introspection-based libraries.
> 
> In this release, I have focused on introducing NPM packages 📦. These packages contain pre-generated TypeScript types that can be easily integrated into your GJS projects. By utilizing these types, you can benefit from TypeScript's strong typing and improved code navigation, whether you are working with JavaScript or TypeScript.
> 
> The pre-generated NPM packages can be accessed directly on [GitHub](https://github.com/gjsify/types), or you can find them on NPM, such as the [Gtk-4.0](https://www.npmjs.com/package/@girs/gtk-4.0) package.
> 
> I encourage you to explore ts-for-gir / the NPM packages and provide your valuable feedback. Your input is greatly appreciated! 🤗️
> ![](hKnIrsHuBURtqKBKAHwkYwYT.png)

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) reports

> I have added multi windows support to [Contrast](https://flathub.org/apps/org.gnome.design.Contrast) because why not. Along with other fixes in v0.0.8
> ![](671674a5c3569750374c620410870896f7306449.png)

### Tube Converter [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

Get video and audio from the web.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) says

> Tube Converter [V2023.5.0](https://github.com/NickvisionApps/TubeConverter/releases/tag/2023.5.0) is here! This week's release features a brand new backend that makes Tube Converter much more stable!
> Besides the new backend, we added the ability to stop all downloads and retry all failed downloads, as well as clear all queued downloads. This release also introduces the feature to crop the thumbnail of a download as square (useful for downloading music) and the ability to choose specific resolutions for video downloads instead of the qualities Best, Good, Worst. 
> Here's the full changelog:
> 
> * Added the per-download option to crop the thumbnail of a video as a square
> * Added the ability to stop all downloads
> * Added the ability to retry all failed downloads
> * Added the ability to clear queued download
> * When downloading a video, the quality options will be the specific resolutions of the video instead of Best, Good, Worst
> * Fixed an issue where some downloads could not be stopped and retried
> * Fixed an issue where some users would experience random crashing when downloading
> * Updated translations (Thanks everyone on Weblate!)
> ![](suFDjLRFzmZvcawbtWQHxVrs.png)

### Pods [↗](https://github.com/marhkb/pods)

A podman desktop application.

[marhkb](https://matrix.to/#/@marcusb86:matrix.org) says

> I have released version 1.2.0 of [Pods](https://github.com/marhkb/pods) with the following new features:
> 
> * Usabilty and UX has been improved in many places.
> * Pruning of containers and pods is now possible.
> * Container terminals are now detachable and can be used in parallel.
> * Images can be pushed to a registry.
> * The CPU utilization now takes the number of cores into account.
> ![](svpdUyKQvTCLQMDsVdqghqXT.png)

### Denaro [↗](https://flathub.org/apps/details/org.nickvision.money)

Manage your personal finances.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) says

> Your favorite pizza man is back with another Denaro release! 🍕
> Denaro [V2023.5.0](https://github.com/NickvisionApps/Denaro/releases/tag/2023.5.0) is here! This week's release features many fixes for bugs users were experiencing across the app. Denaro will also now show error messages if it attempts to access inaccessible files instead of crashing.
> Here's the full changelog:
> * Fixed an issue where Denaro would crash on systems with unconfigured locales
> * Fixed an issue where PDF exporting failed for accounts with many receipts
> * Fixed an issue where a group's filter was reactivated when a transaction was added to that group
> * Error messages will be shown if Denaro attempts to access inaccessible files instead of crashing
> * Updated translations (Thanks to everyone on Weblate)!
> ![](jrbaZkCGaWWylOUrynDzYYvr.png)

# Documentation

[Sonny](https://matrix.to/#/@sonny:gnome.org) reports

> libmanette dev documentation is back online and will be included in `org.gnome.Sdk.Docs`
> https://gnome.pages.gitlab.gnome.org/libmanette/
> 
> > libmanette offers painless access to game controllers, from any programming language and with little dependencies.

# GNOME Foundation

[Kristi Progri](https://matrix.to/#/@kristiprogri:gnome.org) says

> After we successfully wrapped up LinuxAppSummit with a great team, we are moving forward with GUADEC preparations.
> 
> The schedule is out now; you can check it at https://events.gnome.org/event/101/timetable/#20230726
> We are pleased to inform you that the call for registrations is now open as well https://events.gnome.org/event/101/registrations/, get your ticket now and keep up with all the latest news and announcements we make.
> 
> You can still submit your Bof/workshop even if you missed the original deadline at: https://events.gnome.org/event/101/surveys/13
> 
> If you would like to volunteer and help us, consider signing up as a volunteer during the event time https://events.gnome.org/event/101/surveys/14

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

