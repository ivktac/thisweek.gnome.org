---
title: "#105 Legendary Saturday Edition"
author: Felix
date: 2023-07-22
tags: ["loupe", "gir.core", "parabolic", "blueprint-compiler", "workbench", "tagger"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from July 15 to July 22.<!--more-->

# GNOME Incubating Apps

### Loupe [↗](https://gitlab.gnome.org/Incubator/loupe)

A simple and modern image viewer.

[Sophie (she/her)](https://matrix.to/#/@sophieherold:gnome.org) announces

> Loupe is now GNOME’s Core app for viewing images. It's not yet available in GNOME OS due to a [dependency issue](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/merge_requests/2242). But everyone can help with testing by trying out the [nightly builds](https://nightly.gnome.org).
> 
> This week we have landed a new design for indicating that an image is dragged over the window. Thanks to FineFindus for implementing the groundwork for this. Chris 🌱️ has tackled the task to equip Loupe with a set of help pages.
> ![](6861b4400867a972d0a6971bf403a8e7948b166c.png)

# GNOME Circle Apps and Libraries

[Diego Iván](https://matrix.to/#/@dimmednerd:matrix.org) announces

> [Paper Clip](https://flathub.org/apps/io.github.diegoivan.pdf_metadata_editor) v3.2 is here! With a bunch of small bug fixes and improvements:
> 
> * Fixed a bug in which the File Dialog would show all files instead of only PDF documents.
> * The app has improved its cache usage.
> * Document thumbnails are now scaled down using `Gtk.Snapshot.append_scaled_texture`, creating nicer-looking images. This can be specially noticed with PDF documents with transparent background.
> ![](pWnOsbuvZpoxaLYgfOSfqMWv.png)

### Workbench [↗](https://github.com/sonnyp/Workbench/)

A sandbox to learn and prototype with GNOME technologies.

[Sonny](https://matrix.to/#/@sonny:gnome.org) reports

> Multi windows/sessions support landed in Workbench. You'll be able to consult Library demos while working on your prototype.
> This is also a requirement for future on-disk folder projects support in Workbench.
> Followin up Bilal Elmoussaoui footsteps, here is a totally unnecessary screenshot of all Library demos open.
> ![](0400406d5e4d999c9e627d96e386f66f14b3fb77.png)

# Third Party Projects

[Paulo](https://matrix.to/#/@raggesilver:matrix.org) announces

> Black Box 0.14 is out. This update brings many exciting features, UI improvements, and bug fixes.
> 
> * New Adwaita and Adwaita Dark color schemes
> * Updated look for tabs
> * Customizable working directory for new tabs
> * Confirmation dialog when closing tabs/windows with commands running
> * Desktop notifications when a command completes in the background
> * Context-aware header bar: the header bar turns red or purple when running sudo or ssh
> * Tabs can be renamed via the new tab right-click menu or a keyboard shortcut
> * Quickly switch between system/light/dark with the new style switcher in the main menu
> 
> You can check out the full release notes [here](https://gitlab.gnome.org/raggesilver/blackbox/-/releases/v0.14.0).
> ![](RWsuGammSzIFPZGenhcFdOzg.png)
> ![](UIfGXgjevjeavoCXeWvbpbzb.png)
> ![](HZligvXUZMbjOzXLgEcejaYt.png)

### Tagger [↗](https://flathub.org/apps/details/org.nickvision.tagger)

Tag your music.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) says

> Tagger [V2023.7.1-beta3](https://github.com/NickvisionApps/Tagger/releases/tag/2023.7.1-beta3) is here!
> 
> Compared to beta 2, this release features a brand new backend for reading and writing tag metadata (TagLib# -> ATL.NET). With this change, Tagger now supports a wide-range of file types and can even manage corrupted files that previous TagLib-based Tagger could not. We also added support for custom, user-defined properties that can be added to any file's tag and we greatly improved our documentation pages available through the Help menu item.
> 
> **Because the new backend is such a big change, we urge all willing users to give this beta a shot and make sure everything still works correctly :)**
> 
> Here's the full changelog:
> 
> * Replaced taglib backend for tag metadata with atldotnet. This change added support for a lot more file types
> * Added support for the following tag properties: Composer, Description, Publisher
> * Added support for custom, user-defined properties
> * Added help documentation with yelp-tools, accessible from the Help menu action
> * Added more file sorting options
> * Improved album art design and added support for managing back cover art and exporting album art
> * Fixed an issue where corrupted music files would crash the app. Tagger will now display a dialog to warn the user of corrupted files
> * Improved UI
> * Updated translations (Thanks everyone on Weblate!)
> ![](pvxKFmeJKycfCVwQzZycZwEX.png)

### Parabolic [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

Download web video and audio.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) reports

> Parabolic [V2023.7.3-beta1](https://github.com/NickvisionApps/Parabolic/releases/tag/2023.7.3-beta1) is here! In this release we improved the design of the Keyring dialog making it  cleaner and more user friendly.
> 
> Behind the scenes, we upgraded to gir.core V0.4.0 which brings many improvements to using GTK and asynchronous programming in C#. As a result of this update, our code is a lot cleaner and easier to read.
> 
> **However, because this is such a big change, we urge all willing users to give this beta a shot and make sure everything still works correctly :)**
> 
> Here's the full changelog:
> * Improved Keyring Dialog design
> * Updated translations (Thanks everyone on Weblate!)
> ![](IyFDiDNJmLCbOpoRTeOKwvpn.png)

### Gir.Core [↗](https://gircore.github.io/)

Gir.Core is a project which aims to provide C# bindings for different GObject based libraries.

[badcel](https://matrix.to/#/@badcel:matrix.org) reports

> Gir.Core 0.4.0 was released. Most prominent features in this release include:
> 
> * Addition of GtkSourceView to the supported libraries
> * Update to the latest versions of Gtk, LibAdwaita, WebKit
> * Support for `Span<T>` to avoid memory duplication while doing native interop
> * Improved exception handling for callbacks
> * Automatic dispatching of code into the MainLoop if `async` code is written
> 
> All details can be found in the [release notes](https://github.com/gircore/gir.core/releases/tag/0.4.0).

### Blueprint [↗](https://jwestman.pages.gitlab.gnome.org/blueprint-compiler/)

A markup language for app developers to create GTK user interfaces.

[James Westman](https://matrix.to/#/@flyingpimonster:matrix.org) announces

> Blueprint 0.10.0 is out! The biggest change is that the `bind-property` keyword is gone, since it was a bit confusing when to use it instead of `bind`. Also in this release are some improvements to the on-hover documentation. https://gitlab.gnome.org/jwestman/blueprint-compiler/-/releases/v0.10.0

# GNOME Foundation

[barthalion](https://matrix.to/#/@barthalion:matrix.org) announces

> Update from the infrastructure team:
> 
> GNOME Gitlab has gained two brand-new CI runners, donated by Canonical. Both are hosted at Hetzner, sporting an Intel Core i5-13500, 64GB of RAM, and 2 NVMe disks configured as RAID1 for faster disk reads.
> 
> The deployment of the new CI runners was a good opportunity to revisit our deployment automation. All runners were upgraded to the latest distribution releases, and also reworked to use podman instead of Docker, which now runs fully unprivileged for a reduced attack surface.
> 
> We have also improved the tool used to clean up the old Docker images to act more aggressively. As a result, there should be fewer issues caused by limited or no disk space.
> 
> Outside of CI/CD changes, we're working on a replacement for Discourse e-mail integration we're using instead of the late Mailman. Currently, it can be a little wonky as it applies Discourse permissions if the sender already has an account, causing issues with the delivery to the Board and Code of Conduct team. The new solution will be deployed shortly after GUADEC.
> 
> Speaking of GUADEC, I will be talking about short- and long-term plans for the infrastructure on Wednesday at 9:40 AM CEST. Drop by if you want to know where we're at!

[Martín Abente Lahaye](https://matrix.to/#/@tchx84:matrix.org) says

> Do you know someone doing outstanding work for GNOME? Find out how to nominate them for the 2023 Pants of Thanks Award! 
> 
> Learn more and find out how to submit a nomination [here](https://discourse.gnome.org/t/nominations-for-2023-pants-of-thanks-award/16151).

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

