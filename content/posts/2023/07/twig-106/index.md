---
title: "#106 GUADEC 2023"
author: Felix
date: 2023-07-28
tags: ["cartridges", "parabolic", "tagger"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from July 21 to July 28.<!--more-->

# GUADEC 2023

![](guadec_2023.jpg)

[Michael Downey](https://matrix.to/#/@michael:downey.net) reports

> During this year's Annual General Meeting on Friday, the annual Community Appreciation Award (a.k.a. Pants of Thanks) was awarded to someone who cares deeply about GNOME, helps out app developers (almost every single day), plays the guitar (but just one song), holds controversial opinions (some even turn out to be right), and who is a climate advocate and activist ... Tobias Bernard! A standing ovation was offered along with a flattering pair of red and blue plaid pajama pants. Congratulations, Tobias!
> ![](IvZgvjwIRGmUWUpeehtejFtI.png)

[mwu](https://matrix.to/#/@mwu:gnome.org) reports

> Greatings from Riga, Latvia!  GUADEC 2023 is in full swing.  We’ve had great turnout and support and wonderful presentations. Its fantastic to see so many GNOME users in one place. Special thanks to  Kristi Progri, Caroline Henriksen and all the speakers, volunteers and sponsors for their amazing work and sponsorship.   
> 
> For those of you who are not here in person or want to review the presentations, you can see the tracks here: [https://www.youtube.com/@GNOMEDesktop](https://www.youtube.com/@GNOMEDesktop)
> 
> Excited the AGM is coming up and we will have some exciting announcements coming soon – stay tuned!
> 
> Thank you to our sponsors – Endless, Ubuntu, ARM, Igalia, openSUSE, Redhat, centricular, Codethink, and Woodlyn Travel for helping to make all of this possible!!!

# GNOME Circle Apps and Libraries

### Cartridges [↗](https://github.com/kra-mo/cartridges)

Launch all your games

[kramo](https://matrix.to/#/@kramo:matrix.org) reports

> Cartridges 2.1 is out, with support for Amazon Games in Heroic.

# Third Party Projects

[Fyodor Sobolev](https://matrix.to/#/@fsobolev:matrix.org) reports

> It's been half a year since the previous stable release of Cavalier, an audio visualizer based on CAVA — but now it's revived! 🕺 Completely rewritten in C#, it joined Nickvision apps family, and got some new features, including new drawing mode, mirror setting and ability to change drawing direction.
> Download from [Flathub](https://flathub.org/ru/apps/org.nickvision.cavalier) or [Snap Store](https://snapcraft.io/cavalier).
> ![](IJdxeDonLqwLZgjTXAjnnJaA.png)
> ![](GBFRkETRjuBwNzHxLgNtIDqw.png)

[Jan-Willem](https://matrix.to/#/@jwharm:matrix.org) reports

> During the past year, I have been working on [Java-GI](https://jwharm.github.io/java-gi), a tool to generate Java language bindings for GNOME, GStreamer and other GObject-Introspection-based libraries, based on the new "Panama" preview API of OpenJDK 20 (JEP-434) for interconnecting the JVM and native code. The goal of Java-GI is to generate a pleasant API in which Java developers feel right at home: camel-case method names, automatic cleanup of resources, and Javadoc-style formatting and cross-references.
> 
> The latest Java-GI release 0.6.1 publishes Java bindings for Gtk4, LibAdwaita, GStreamer, GtkSourceview and WebkitGtk, and custom-developed bindings for Cairo. It is also possible to define your own GObject-derived classes in Java, and to create Gtk composite template classes. Visit the [Java-GI website](https://jwharm.github.io/java-gi), or check out the [examples](https://github.com/jwharm/java-gi-examples) for more information.
> 
> Until now, I've mostly been working on Java-GI on my own. I'd very much appreciate if interested Java developers would try it out, send feedback on [#java-gi:matrix.org](https://matrix.to/#/#java-gi:matrix.org) or [github](https://github.com/jwharm/java-gi), and become involved in further development.

### Tagger [↗](https://flathub.org/apps/details/org.nickvision.tagger)

Tag your music.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) announces

> Tagger [V2023.7.1](https://github.com/NickvisionApps/Tagger/releases/tag/2023.7.1) is here! This release comes after a month of hard work, adding many new features, design enchantments, and overall improvements to the experience of the app.
> 
> There are just too many changes to describe here, so please read the full changelog below :)
> 
> A huge thanks to all contributors, translators, feature requesters, and testers ❤️
> 
> Here's the full changelog:
> * Replaced taglib backend for tag metadata with atldotnet. This change added support for a lot more file types
> * Added support for the following tag properties: Composer, Description, Publisher
> * Added support for custom, user-defined properties
> * Added support for custom, user-defined format strings for conversion
> * Added help documentation with yelp-tools, accessible from the Help menu action
> * Added more file sorting options
> * Improved album art design and added support for managing back cover art and exporting album art
> * Improved advanced search algorithm to use fuzzy searching
> * Fixed an issue where corrupted music files would crash the app. Tagger will now display a dialog to warn the user of corrupted files
> * Improved UI
> * Updated translations (Thanks everyone on Weblate!)
> ![](BInssCmnoCYZWvopNehaajjh.png)

### Parabolic [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

Download web video and audio.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) announces

> Parabolic [V2023.7.3](https://github.com/NickvisionApps/Parabolic/releases/tag/2023.7.3) is here!
> 
> In this release we improved the design of the Keyring dialog making it cleaner and more user friendly. We also added a History dialog that allows users to manage previously downloaded videos!
> 
> Behind the scenes, we upgraded to gir.core V0.4.0 which brings many improvements to using GTK and asynchronous programming in C#. As a result of this update, our code is a lot cleaner and easier to read.
> ![](EPaRwNaMRhtxFxEunAzeuEeN.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

