---
title: "#112 New Circle Apps"
author: Felix
date: 2023-09-08
tags: ["crosswords", "tagger", "gtk-rs", "phosh", "denaro"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from September 01 to September 08.<!--more-->

# GNOME Circle Apps and Libraries

[Brage Fuglseth](https://matrix.to/#/@bragefuglseth:gnome.org) reports

> This week [Forge Sparks](https://apps.gnome.org/ForgeSparks) by Rafael Mardojai was accepted into GNOME Circle. Forge Sparks lets you receive Git forges notifications on your desktop. Congratulations!
> ![](4eb3092d2348ab11592b8956455ba40f02d6da6c.png)

[Brage Fuglseth](https://matrix.to/#/@bragefuglseth:gnome.org) says

> The second app to join GNOME Circle this week was [Impression](https://apps.gnome.org/Impression/) by Khaleel Al-Adhami. Impression lets you create bootable drives by flashing disk images to them. Congratulations!
> ![](7d7eb4d7534a45cdc1295dbdf24f6477cf712018.png)

### gtk-rs [↗](https://gtk-rs.org/)

Safe bindings to the Rust language for fundamental libraries from the GNOME stack.

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) announces

> It took a bit too long but a new release of the GTK Rust bindings is officially out. Details can be found at https://gtk-rs.org/blog/2023/08/28/new-release.html

# Third Party Projects

[xjuan](https://matrix.to/#/@xjuan:gnome.org) announces

> Cambalache 0.14.0 Released!
> 
> I am pleased to announce a new Cambalache version.
> Cambalache is a new RAD tool for Gtk 4 and 3 with a clear MVC design and data model first philosophy.
> 
> 
> Release Notes:
> 
>  - Add GMenu support
>  - Add UI requirements edit support
>  - Add Swedish translation. Anders Jonsson
>  - Updated Italian translation. Lorenzo Capalbo
>  - Show deprecated and not available warnings for Objects, properties and signals
>  - Output minimum required library version instead of latest one
>  - Fix output for templates with inline object properties
>  - Various optimizations and bug fixes
>  - Bump test coverage to 66%
> 
> Read more about it in this [blog post](https://blogs.gnome.org/xjuan/2023/09/07/cambalache-0-14-0-released/)
> 
> Where to get it:
>  - [Flathub](https://flathub.org/apps/ar.xjuan.Cambalache)
>  - [Gitlab](https://gitlab.gnome.org/jpu/cambalache/)
> ![](c50e97479f512fb804d057898d2699cb838469d1.png)

[Felicitas Pojtinger](https://matrix.to/#/@pojntfx:matrix.org) says

> I just released [Multiplex](https://github.com/pojntfx/multiplex), an app to watch torrents together. It provides an experience similar to Apple's SharePlay and Amazon's Prime Video Watch Party, except for any torrent instead of a specific streaming service. Thanks to WebRTC, it doesn't require a central server to synchronize playback positions, and allows the use of a central HTTP to BitTorrent gateway if a viewer can't use the BitTorrent protocol on their own device. It's written in Go with Adwaita and available on [Flathub](https://flathub.org/apps/com.pojtinger.felicitas.Multiplex).

[fabrialberio](https://matrix.to/#/@fabrialberio:matrix.org) announces

> This week I've released a new version of PinApp, an app that lets you create and edit application shortcuts in an elegant way, without having to bother with `.desktop` files.
> 
> This update brings new libadwaita 1.4 widgets, support for Snap apps as well as translations to german and spanish.
> 
> PinApp is available on [Flathub](https://flathub.org/apps/io.github.fabrialberio.pinapp).
> ![](wuQDNyILKXGVegKfPYsvqUcD.png)

### Gir.Core [↗](https://gircore.github.io/)

Gir.Core is a project which aims to provide C# bindings for different GObject based libraries.

[badcel](https://matrix.to/#/@badcel:matrix.org) says

> Gir.Core 0.5.0-preview.1 got released.
> 
> It features much improved support for opaque boxed records. More details can be found in the [release notes](https://github.com/gircore/gir.core/releases/tag/0.5.0-preview.1).

### Tagger [↗](https://flathub.org/apps/details/org.nickvision.tagger)

Tag your music.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) reports

> Tagger [V2023.9.0](https://github.com/NickvisionApps/Tagger/releases/tag/2023.9.0) is here! This release includes many new features and improvements to Tagger's lyrics system, file handling, and user experience. 
> 
> Here's the full changelog:
> * Added a new web service to download lyrics for a song
> * Added support for opening a folder with Tagger from the file manager
> * Added a "Select All Files" button next to the search bar
> * Redesigned the Lyrics dialog in which only unsynchronized or synchronized lyrics can be used for a file and not both
> * Fixed an issue where some album art for some OPUS files were not read
> * Fixed an issue where Tagger would crash on Tag to Filename if the tag contained an invalid file name character
> * Fixed an issue where Tagger would marked files as unsaved even if there were no changes
> * Updated translations (Thanks everyone on Weblate!)
> ![](KDtkKLDjqBIDyNqHFHOHwaII.png)

### Phosh [↗](https://gitlab.gnome.org/World/Phosh/phosh)

A pure wayland shell for mobile devices.

[Guido](https://matrix.to/#/@agx:sigxcpu.org) announces

> Anton Lazarev ported [libcall-ui](https://gitlab.gnome.org/World/Phosh/libcall-ui/) (a library with common user interface parts for call handling) to GTK4 and libadwaita. Applications using GTK3/libhandy can still do so using the [0.1.x branch](https://gitlab.gnome.org/World/Phosh/libcall-ui/-/tree/libcall-ui-0.1.x).
> ![](deScOSLfwcLTmnOwPqPGEEzE.png)

### Denaro [↗](https://flathub.org/apps/details/org.nickvision.money)

Manage your personal finances.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) announces

> Denaro [V2023.9.1](https://github.com/NickvisionApps/Denaro/releases/tag/2023.9.1) is here! This release includes new features for dealing with accounts with different currencies as well as some bug fixes :)
> 
> Here's the full changelog:
> * Added a currency conversion tool into Denaro, accesable from the main menu. This tool is also integrated into the Transfer dialog when currency conversion is required
> * Fixed an issue where dates in the IncomeExpenseOverTime graph would overlap
> * Fixed an issue where accessing account settings would sometimes crash
> * Updated and added translations (Thanks to everyone on Weblate)!
> ![](CohboEgGRejPTGrJYzuBQCYG.png)
> ![](MjoCAsqmtxtoPfgQxJYRXIiG.png)

### Crosswords [↗](https://gitlab.gnome.org/jrb/crosswords)

A crossword puzzle game and creator.

[jrb](https://matrix.to/#/@jblandford:matrix.org) reports

> Crosswords 0.3.11 has been released this is the first version to feature a standalone Crossword Editor. Read all the details [here](https://blogs.gnome.org/jrb/2023/09/03/crosswords-0-3-11-acrostic-panels/). Highlights this cycle:
> 
> * Initial Crossword Editor release [available for download on flathub](https://flathub.org/apps/org.gnome.Crosswords.Editor)! This release uses libpanel from GNOME Builder to achieve a similar look and feel.
> * Support for authoring Barred-style crossword puzzles in the editor.
> * The Crosswords player now supports [acrostic puzzles](https://en.wikipedia.org/wiki/Acrostic_(puzzle)). This was the result of a successful GSOC project!
> Numerous critical bug fixes and improvements.
> * The core crossword parsing library has a new home at [https://libipuz.org](https://libipuz.org/).
> ![](zBVFzGsjEAYgeODXeVfAszhk.png)
> ![](kAgTUeieZUWucDehksfIPcVy.png)
> ![](DZiHLCewuiZWHZuSZSYJdgJX.png)

# Shell Extensions

[glerro](https://matrix.to/#/@glerro:matrix.org) reports

> I have updated my Gnome Shell Extension for Gnome 45:
> https://extensions.gnome.org/extension/2296/looking-glass-button/
> and
> https://extensions.gnome.org/extension/5416/wifi-qrcode/

[sereneblue](https://matrix.to/#/@serenedev:matrix.org) announces

> I've released updates for extensions [Colosseum](https://extensions.gnome.org/extension/4207/colosseum/) and [krypto](https://extensions.gnome.org/extension/1913/krypto/) to support GNOME 45. Colosseum now supports showing scores for 16 sports leagues and 5 tournaments.

# Miscellaneous

[Brage Fuglseth](https://matrix.to/#/@bragefuglseth:gnome.org) announces

> I’ve created a Matrix room for chatting about GNOME in the Scandinavian languages: Norwegian, Swedish and Danish. These languages are spoken by more than 20 million people combined worldwide, and are similar enough that speakers of one language can understand the other two. If you speak one of the languages, natively or not, and would like to be a part of a thriving Scandinavian GNOME subcommunity, don’t hestitate to join [#scandinavia:gnome.org](https://matrix.to/#/#scandinavia:gnome.org) and introduce yourself! You don’t need to be a regular contributor; the room is open to anyone interested in GNOME 🇳🇴🇸🇪🇩🇰

[Sophie (she/her)](https://matrix.to/#/@sophieherold:gnome.org) reports

> URLs for pages on [Apps for GNOME](https://apps.gnome.org/) are now considerably shorter. For example, the Maps app is now available under [apps.gnome.org/Maps](https://apps.gnome.org/Maps).
> 
> The old URLs, that contain the complete app ID, will keep working. The app URLs are generally guaranteed to be a safe place to link to. Even if an app gets removed, the page will be redirected to an external page decided on by the app maintainer.

# Events

[Kristi Progri](https://matrix.to/#/@kristiprogri:gnome.org) says

> September has been a busy month so far. I had a chance to present at Open Source Conference Albania (https://oscal.openlabs.cc/) and talk about GNOME and how people can contribute.
> We were also present with a booth and many people where interested to know more.
> I will have the same presentations in Debconf as well, where I am planning to get more in touch with locals and get new contributors.
> 
> Talking about conferences, please keep in mind that GNOME ASIA call for papers is still open (https://events.gnome.org/event/170/abstracts/#submit-abstract) until September 10th.
> Don't miss your chance to join us in Nepal.

[Deepesha Burse](https://matrix.to/#/@deepeshaburse:matrix.org) reports

> Hello everyone!
> 
> Hope you all are doing well. GNOME Asia is happening in Kathmandu, Nepal between 1st to 3rd of December. We are glad to announce that the deadline for submitting CfPs has been extended up to 10th September. This is your chance to submit a proposal and speak at the conference!
> 
> You can submit your CfPs at: https://events.gnome.org/event/170/abstracts/ and find more info at: https://foundation.gnome.org/2023/08/08/gnomeasia-2023-cfp/.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

