---
title: "#127 Welcome News"
author: Felix
date: 2023-12-22
tags: ["glib", "tracker", "fractal"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from December 15 to December 22.<!--more-->

# Sovereign Tech Fund

[Tobias Bernard](https://matrix.to/#/@tbernard:gnome.org) reports

> As part of the [GNOME STF (Sovereign Tech Fund)](https://foundation.gnome.org/2023/11/09/gnome-recognized-as-public-interest-infrastructure/) project, a number of community members are working on infrastructure related projects.
> 
> * Adrian reworked some of homed's internals to allow admins to change user settings without the user's password https://github.com/systemd/systemd/pull/30109
> * Adrian made it possible to change homed passwords through AccountService https://gitlab.freedesktop.org/accountsservice/accountsservice/-/merge_requests/144?commit_id=d12301d8f7d6ee18bbc47a0825abb78fb89dfcb3#note_2214175
> * Adrian experimented with making the session lock through GDM. This allows homed to throw out the encryption keys to the home directory whenever the session is locked.
> {{< video src="gnome-shell-key-eviction-alt.webm" >}}
> * Joanie added a globally available Table Navigator to Orca https://gitlab.gnome.org/GNOME/orca/-/commit/1a330fc42
> * Joanie made a number of keybinding and key-grab code clean-up, bug fixes, and improvements
> * Alice worked on bottom sheet resizing, ported message dialogs to the new system, and made it possible for dialogs to always use floating/bottom sheet presentation instead of switching automatically
> {{< video src="1226c4872e42ce33cd264e5736f14149eee99dda1738219423472287744.webm" >}}
> * Ivan tested a new Tracy `dladdr()` cache for callstack sampling. While it significantly reduces contention with GJS and makes it much more viable to profile GNOME Shell with callstack sampling, it still results in a little bit of contention overhead.
> * Georges pushed v2 of the USB portal
>     - https://github.com/flatpak/flatpak/pull/5620
>     - https://github.com/flatpak/xdg-desktop-portal/pull/1238
> * Georges made the screenshot portal use GNOME Shell's screencasting UI when available https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/2999
> {{< video src="222cfe7527a290565118e56fd6297b5b4e1ad9491738258151704100864.webm" >}}
> * Julian is investigating adding scroll support to the Quick Settings popover, which would be very useful [on small laptops](https://gitlab.gnome.org/GNOME/gnome-shell/-/issues/6056)
> * Julian worked on keeping symbolic icons at their nominal size, inside a circle https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3066
> {{< video src="15fe1bd13824893773540010cf22adb0196f61351738290367779831808.webm" >}}
> ![](d7566d015b8df9fc5aa4afac3d2d67f3b06c04fc1738290391129522176.png)
> * Philip made online docs for GLib/GObject/Gio use gi-docgen https://gitlab.gnome.org/GNOME/glib/-/issues/3037
> * Tobias added [neighboring file access](https://github.com/flatpak/xdg-desktop-portal/issues/463) to the File Chooser Dialog mockup https://gitlab.gnome.org/Teams/Design/app-mockups/-/blob/master/files/file-chooser.png

# GNOME Websites

[Sophie (she/her)](https://matrix.to/#/@sophieherold:gnome.org) reports

> Our new *Welcome to GNOME* site is now available via [welcome.gnome.org](https://welcome.gnome.org/). The page helps newcomers as well as more experienced contributors to get started with working in a new area within GNOME. The pages are currently available in Brazilian Portuguese, English, Indonesian, Russian, Swedish, and Ukrainian. Contribution instructions for apps are auto-generated with information specific to more than 90 apps. You can learn more in my [introduction blog post](https://blogs.gnome.org/sophieh/2023/12/21/welcome-to-gnome-a-site-to-get-started/).
> ![](fb71b6184ae17f197d890c16cd7974e6a0e37bc11737895306432872448.png)

# GNOME Core Apps and Libraries

### Tracker [↗](https://gitlab.gnome.org/GNOME/tracker/)

A filesystem indexer, metadata storage system and search tool.

[Sam Thursfield](https://matrix.to/#/@ssssam:matrix.org) says

> In Tracker SPARQL, new optimizations landed thanks to Carlos Garnacho which will significantly reduce peak memory usage and overall memory allocations during the indexing process. See [the MR](https://gitlab.gnome.org/GNOME/tracker/-/merge_requests/641) for full details.

[Sam Thursfield](https://matrix.to/#/@ssssam:matrix.org) reports

> Carlos Garnacho landed some new functional tests in Tracker Miners that exercise failure cases in metadata extraction and potential escape routes from the sandbox. See [the MR](https://gitlab.gnome.org/GNOME/tracker-miners/-/merge_requests/502) for details.

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) reports

> Thomas Haller has been working hard to fix a latent race in `GObject`, https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3769

# Third Party Projects

[Alain](https://matrix.to/#/@a23_mh:matrix.org) says

> Planify 4.3 has been released and is available on [Flathub](https://flathub.org/es/apps/io.github.alainm23.planify)
> 
> What’s New:
> 
> * Quick Add Improvements: Now supports project selection, due date, priority, labels and pinned.
> * Sidebar filter settings: It is now possible to re-order, hide filters and the task cutter.
> * Backup support: It is now possible to create a backup copy and import it from somewhere else. Planify will import all your tasks and settings.
> * Offline support for Todoist: You were without internet, keep using Planify with Todoist, the tasks will be saved locally and synchronized when the connection returns.
> 
> Bug Fixed and Translations
> 
> * Russian translations thanks to @hachikoharuno.
> * Fix typos in welcome project #1079 thanks to @Jaques22.
> * Fixed error when displaying color in calendar events.
> * Bugs fixed #1076, #1073.
> ![](DgOZfgNBbLMViSooukYcpmuB.png)
> ![](smXYuYFrEtCPTnlwLGuwawuT.png)
> ![](IgYqJPLFuKmajNpzEPtzjMuj.png)
> {{< video src="qAAvRvWScvaKHtOZTMkJUaPD.webm" >}}

[Krafting](https://matrix.to/#/@lanseria:matrix.org) announces

> Hello everyone!
> 
> I've released version 2.0 of [Playlifin](https://gitlab.com/Krafting/playlifin-gtk), I reworked most of the code to make it less prone to errors while also adding some new features!
> 
> New features include:
> * Removed the "Logs" function. Everything is logged through the console or a file
> * Added a setting to turn off SSL certificate verification (So it can work with self-signed certs!)
> * Added a status bar with the current progress instead of the Logs
> * Added a setting to also search Video, Audio or Both (Default: Audio)
> * Now using libadwaita widgets in the header bar
> 
> Now available on [Flathub](https://flathub.org/apps/net.krafting.Playlifin)
> ![](vlfsizxiUwbKGgMeqeGvaSaV.png)

### Fractal [↗](https://gitlab.gnome.org/GNOME/fractal)

Matrix messaging app for GNOME written in Rust.

[Kévin Commaille](https://matrix.to/#/@zecakeh:tedomum.net) announces

> Let's celebrate the winter (or summer) solstice with a new Fractal beta release! Even though Fractal 5 was released only 1 month ago, development has been going at a steady pace with a few new contributors, so we want our users to benefit from our progress.
> 
> The staff's picks:
> 
> * [Restoring sessions from Secret Services other than GNOME Keyring has been fixed](https://gitlab.gnome.org/GNOME/fractal/-/issues/1324)
> * [Times follow the format (12h or 24h) from the system settings](https://gitlab.gnome.org/GNOME/fractal/-/issues/1262)
> * [Media history works in encrypted rooms](https://gitlab.gnome.org/GNOME/fractal/-/issues/1322)
> * [The accessibility of the sidebar was improved](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/1491)
> * More notifications settings were added, you can now set [global](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/1496) and [per-room](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/1502) behavior and even manage your keywords
> * A bunch of refactoring, notably [the port to the `glib::Properties` macro from gtk-rs](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests?scope=all&state=merged&search=glib+properties+macro) that helped us removed almost **3000** lines of code!
> 
> As usual, this release includes other improvements, fixes and new translations thanks to all our contributors, and our upstream projects.
> 
> It is available to install via Flathub Beta, see the [instructions in our README](https://gitlab.gnome.org/GNOME/fractal#installation-instructions).
> 
> As the version implies, there might be a slight risk of regressions, but it should be mostly stable. If all goes well the next step is the release candidate!
> 
> If you find yourself with time to spare during these end-of-year holidays, you can always try to fix one of our [issues](https://gitlab.gnome.org/GNOME/fractal/-/issues). Any help is greatly appreciated!
> ![](08fd1c7f43f8e6b62f23e1dd49bf4da6a068e9bc1737895709337714688.png)

# Miscellaneous

[Sam Thursfield](https://matrix.to/#/@ssssam:matrix.org) says

> In week 3 of the Outreachy internship around end-to-end testing, our interns Dorothy and Tanju worked on testing GNOME's accessibility features. The following tests landed in the 'master' branch of the [openqa-tests](https://gitlab.gnome.org/GNOME/openqa-tests/-/issues/86) this week:
> 
> * `a11y_seeing`: Testing the "High Contrast" and "Large Text" features
> * `a11y_hearing`: Testing the Speech Dispatcher text-to-speech system
> 
> You can see an example test run [here](https://openqa.gnome.org/tests/2451#). Several more tests are in development and will land soon. See the [project board](https://gitlab.gnome.org/GNOME/openqa-tests/-/boards/23536) to see what is coming up!
> ![](sWWNtQWHBGsxsBLBhTvLViEa.png)

# GNOME Foundation

[Caroline Henriksen](https://matrix.to/#/@chenriksen:gnome.org) reports

> Save the date! GUADEC 2024 will take place in Denver, Colorado, USA and online from July 19-24. The Call for Participation and Registration will open soon, be sure to check [guadec.org](https://events.gnome.org/event/101/) for those links early 2024.
> 
> For more information about GUADEC 2024, please visit the full news post: https://foundation.gnome.org/2023/12/20/guadec-2024-in-denver-colorado/

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
