---
title: "#140 Forty-six!"
author: Felix
date: 2024-03-22
tags: ["blueprint-compiler", "workbench", "glib", "flare", "fretboard", "kooha"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from March 15 to March 22.<!--more-->

**This week we released GNOME 46!**

![](46-banner.webp)

This new major release of GNOME is full of exciting changes, such as a new global file search, an enhanced Files app, improved online accounts with OneDrive support, remote login via RDP, improved accessibility, experimental variable refresh rate (VRR) support and so much more! See the [GNOME 46 release notes](https://release.gnome.org/46/) and [developer notes](https://release.gnome.org/46/developers/index.html) for more information. 

Readers who have been following this site will already be aware of some of the new features. If you'd like to follow the development of GNOME 47 (Fall 2024), keep an eye on this page - we'll be posting exciting news every week!

# Sovereign Tech Fund

[Sonny](https://matrix.to/#/@sonny:gnome.org) announces

> As part of the [GNOME STF (Sovereign Tech Fund)](https://foundation.gnome.org/2023/11/09/gnome-recognized-as-public-interest-infrastructure/) initiative, a number of community members are working on infrastructure related projects.
> 
> Besides helping with the GNOME 46 release (congrats everyone!); here are the highlights for the past week
> 
> This week we welcome Jerry, Tom, Neill and Jude of Codethink into the team. 
> 
> Jerry and Tom got started with finishing [sysupdate: Implement dbus service](https://github.com/systemd/systemd/pull/28134). This will allow apps such as GNOME Software, KDE Discover, ... to support [systemd-sysupdate](https://www.freedesktop.org/software/systemd/man/latest/systemd-sysupdate.html)
> 
> Neill got started making GNOME openQA more robust with
>   * [Needle cleanup script, for unused/expired needles](https://gitlab.gnome.org/GNOME/openqa-needles/-/merge_requests/68)
>   * [GNOME Shell sometimes fails to start](https://gitlab.gnome.org/GNOME/openqa-tests/-/issues/62)
> 
> Julian [implemented 9 new properties for notifications in xdg-desktop-portal](https://github.com/jsparber/xdg-desktop-portal/tree/implement_v2_notification_spec) such as icon (via fd), sound, actions, markup-body, ...
> 
> Julian worked on [making notifications in xdg-desktop-portal forward compatible by allowing unknown properties](https://github.com/flatpak/xdg-desktop-portal/pull/1303).
> 
> Dorota is working on an interface for global shortcuts in Mutter/GNOME Shell suitable for the global shortcuts portal (except listing shortcuts)
> 
> Dhanuka [has been testing](https://hedgedoc.gnome.org/s/ySwSXZnwl) the Rust DBus Secret Service provider implementation in oo7 to replace GNOME Keyring 
> 
> Jonas made improvements in audio integration [#25](https://gitlab.gnome.org/GNOME/libgnome-volume-control/-/merge_requests/25), [#26](https://gitlab.gnome.org/GNOME/libgnome-volume-control/-/merge_requests/26)
> 
> Alice resumed work on [CSS custom properties / variables support in GTK](https://gitlab.gnome.org/GNOME/gtk/-/merge_requests/6540); animations are now supported.
> 
> Andy [made a protoype](https://github.com/flatpak/xdg-desktop-portal/pull/1313) to allow opening URLs with apps. The goal is for an app such as GNOME Maps to advertise support for and handle openstreetmap.org or google.com/maps URLs.
> {{< video src="a0d71b3cd45dd83b9798753660bb29f727e5b1751771265865681993728.webm" >}}

# GNOME Core Apps and Libraries

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) announces

> Christian Hergert added support for sub-millisecond timeouts in GLib using `ppoll()` (https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3958)

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) reports

> Sudhanshu Tiwari has made a start on porting some of the GIO documentation comments to gi-docgen in https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3969

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) reports

> JSON-GLib, the library for parsing and generating JSON data, is now [capable of strict compliance](https://gitlab.gnome.org/GNOME/json-glib/-/merge_requests/67) with the [JSON specification](https://datatracker.ietf.org/doc/html/rfc8259). To avoid breaking backward compatibility, strictness must be explicitly enabled by setting the [`JsonParser:strict`](https://gnome.pages.gitlab.gnome.org/json-glib/property.Parser.strict.html) property, or using the `--strict` option for the `json-glib-validate` command line tool. To enforce strict compliance, JSON-GLib now includes a whole [JSON conformance test suite](https://github.com/nst/JSONTestSuite/).

# GNOME Incubating Apps

[Sophie (she/her)](https://matrix.to/#/@sophieherold:gnome.org) announces

> [Decibels](https://apps.gnome.org/Decibels/) has been accepted into the GNOME Incubator. The GNOME [incubation process](https://gitlab.gnome.org/GNOME/Incubator/Submission) is for apps that are designated to be accepted into [GNOME Core or GNOME Development Tools](https://gitlab.gnome.org/Teams/Releng/AppOrganization/-/blob/main/OfficialAppDefinition.md) if they reach the required maturity.
> 
> Decibels is a basic audio player that is supposed to fill the gap of GNOME currently not having a Core app that is designed to open single audio files. The incubation progress will be [tracked in this issue](https://gitlab.gnome.org/Teams/Releng/AppOrganization/-/issues/25).
> ![](748b8847728182e09591f74934a15f7bf6493a8a1769802713131909120.png)

# GNOME Circle Apps and Libraries

[Tobias Bernard](https://matrix.to/#/@tbernard:gnome.org) reports

> [Railway](https://apps.gnome.org/en/DieBahn) has been accepted into GNOME Circle. It allows you to easily look up travel information across rail networks and borders without having to use multiple different websites. Congratulations!
> ![](564acc30396be08cc06672429649112984d1b8251769823382691381248.png)

### Workbench [↗](https://github.com/sonnyp/Workbench/)

A sandbox to learn and prototype with GNOME technologies.

[Sonny](https://matrix.to/#/@sonny:gnome.org) announces

> Workbench 46 is [out on Flathub](https://flathub.org/apps/re.sonny.Workbench)! Here are the highlights
> 
> Everybody is excited about them so I'll start by saying you can try libadwaita 1.5 adaptive dialogs with the new "Dialog" and "Message Dialogs" demos in the Library.
> 
> Workbench now shows inline diagnostics for Rust and Python.
> 
> A new Library demo "Snapshot" was added to demonstrate one of GTK4 coolest feature.
> 
> 26 additional demos have been ported to Python
> 
> 5 additional demos have been ported to Vala
> 
> [The GNOME 46 release notes includes all the changes between Workbench 45 and 46.](https://release.gnome.org/46/developers/index.html#workbench)
> 
> Thank you to all contributors
> ![](7b68bbce921f0c4a18a8d108bc1652f37b91c3a21770499732490158080.png)
> ![](ae9cce39562df037d66fcc057c474e33a47f28191770499686134710272.png)
> {{< video src="751a8ecaea91860720c8676792aa885a34bf65341770499876849713152.mp4" >}}

### Fretboard [↗](https://apps.gnome.org/Fretboard)

Look up guitar chords

[Brage Fuglseth](https://matrix.to/#/@bragefuglseth:gnome.org) reports

> Happy release week! Like many other apps, Fretboard has been updated to the GNOME 46 platform, taking advantage of the many platform improvements that have happened this cycle. It also recently gained the ability to notify you when there are no available variants of a chord in its internal chord set, prompting you to reach out and help improve it.
> 
> As always, you can get Fretboard on [Flathub](https://flathub.gnome.org/apps/dev.bragefuglseth/Fretboard).
> ![](f5b35572d53fad5b6726edb172bc724ab28a401d1770924686385872896.png)

# Third Party Projects

[robert.mader](https://matrix.to/#/@robert.mader:gnome.org) announces

> Livi 0.1.0 is now available on Flathub. Bundled with Gstreamer 1.24 and build against the GTK 4.14 it is the first desktop-targeting app to enable zero-copy Video playback by default in the Wayland ecosystem. Doing so enables highly power-efficient playback, closing the gap to other OSs or embedded environments.
> We expect quite a few people to hit driver bugs in the beginning - so in order to pave the way for other apps to pick up the technology, please help testing on you devices :)
> ![](a51b50585e7e3d824cb8ba6e519d3c2340ce500b1771236878033354752.png)

[Alain](https://matrix.to/#/@a23_mh:matrix.org) reports

> Planify has received several updates this week, including bug fixes and design enhancements.
> 
> As part of the effort to apply for Gnome Circle, the user interface has been updated with new icons, design elements, and typography.
> 
> What's new:
> 
> * Performance of synchronization with Nextcloud has been improved.
> * It's now possible to select the Pinboard view as the homepage.
> * You can now add a task to the Pinboard view from the contextual menu.
> * Various reported bugs have been fixed.
> ![](IESuBebpVYGCMiomMyKjcQVE.png)
> ![](RKpwHoslIrYQurZtvtPcGsjO.png)
> ![](cUSAwYzskLZNvYAtswgiWSrS.png)

[Akshay Warrier](https://matrix.to/#/@akshaywarrier:matrix.org) reports

> Biblioteca 1.3 is now available on [Flathub](https://flathub.org/apps/app.drey.Biblioteca)!
> 
> This release comes with several additions and improvements such as:
> * Added docs for GLib/Gio/GObject
> * Added support for web content
> * Improved searching UI
> * Added support for keyboard navigation in the sidebar
> * Added zoom buttons to the primary menu
> * Added shortcuts to view open tabs and toggle sidebar
> ![](nlNHhrXmdGaxjCQIagBBuZaM.png)

[Markus Göllnitz](https://matrix.to/#/@camelcasenick:matrix.org) announces

> Rumour has it there was a recent release of Usage – complete with ~~leaked~~ release screenshots.
> 
> * So far, It looks like, it features an indicator for applications running in background.
> *  Apparently, it is even displaying individual Android applications when you run Waydroid, now. That is something.
> * On top of it, I would say, the split of the performance view into processor and memory and the subsequent use of flat header bars works quite well.
> 
> Find it at a distro near you.
> ![](SFUrwUgBRJZJpzdHHUBmOWDQ.png)
> ![](ZuyxuOyvGHERgTyXHvjvByXz.png)

[FineFindus](https://matrix.to/#/@finefindus:matrix.org) says

> I'm happy to announce the first release of Hieroglyphic, a forked and updated version of TeX-Match, which helps to find LaTeX symbols by drawing them. It's available for download on [Flathub](https://flathub.org/apps/io.github.finefindus.Hieroglyphic).
> ![](TYhkoLeEORSvrLFhbANzBqLo.png)

### Kooha [↗](https://github.com/SeaDve/Kooha)

Elegantly record your screen.

[Dave Patrick](https://matrix.to/#/@sedve:matrix.org) says

> Kooha 2.3 is [now released on Flathub](https://flathub.org/apps/io.github.seadve.Kooha)! While there are no groundbreaking new features, this release is focused more on fixes and quality-of-life improvements. 
> 
> The following features and fixes are the highlights:
> * The area selector window is now resizable, making selecting an area more flexible.
> * The previously selected area is now remembered across sessions.
> * The current video format and FPS configurations are now visible in the main view.
> * The recording done notification now shows the duration and size of the recording.
> * Progress is now shown while flushing the recording.
> * Recording in stereo rather than in mono is now more preferred.
> * Audio stutters on long recordings are now properly fixed.
> * The preferences dialog is now more descriptive and provides a more convenient FPS selection box.
> * Incorrect recording orientation on certain compositors is now fixed.
> 
> For a more detailed changelog, check out the [full release notes](https://github.com/SeaDve/Kooha/releases/tag/v2.3.0).
> ![](cXlvTzbBfeyBIfnocEUGcZTA.png)
> ![](cJaOSlLgPJxAXPyTmKgWCGAR.png)

### Flare [↗](https://flathub.org/apps/details/de.schmidhuberj.Flare)

Chat with your friends on Signal.

[schmiddi](https://matrix.to/#/@schmiddi:matrix.org) reports

> Flare version 0.14.1 was released. This release includes updating the dialogs to the new adwaita adaptive dialogs. Furthermore, we also have a new "new channel" dialog and channel information dialog. This release also contains a hotfix for newly linked devices not working with groups and another minor fix for an error in certain groups.
> ![](sdwHuSHayxgFZcfxbYWzjRRK.png)
> ![](CZEHWJSDAKSrzzJxZoTLOWjX.png)

### Blueprint [↗](https://jwestman.pages.gitlab.gnome.org/blueprint-compiler/)

A markup language for app developers to create GTK user interfaces.

[Sonny](https://matrix.to/#/@sonny:gnome.org) announces

> [Blueprint](https://jwestman.pages.gitlab.gnome.org/blueprint-compiler/); the markup language and tooling for GTK is out in version 0.12
> 
> Here are the highlights ✨
> 
> Brand-new formatter to keep files tidy
> 
> `AdwAlertDialog` are supported
> 
> Emit warnings for deprecated features in GTK, GLib, etc
> 
> New IDE integration features
> - document symbols
> - "Go to definitions"
> - Code action for importing missing namespace
> 
> We also celebrate 70 applications on Flathub built with Blueprint.

# Events

[Deepesha Burse](https://matrix.to/#/@deepeshaburse:matrix.org) reports

> The deadline for the GUADEC 2024 Call for Participation is closing soon!
> This year's conference will take place in Denver, Colorado, from July 19th to July 24th and we encourage all interested contributors, speakers, and participants to submit their proposals before the deadline on 24th March. This is an excellent opportunity to share your insights, experiences, and ideas with the GNOME community and contribute to the success of GUADEC 2024.
> Please visit guadec.org to submit your proposals. If you have any questions or need assistance, feel free to reach out to the organizing committee at guadec@gnome.org.
> We look forward to receiving your submissions and seeing you at GUADEC 2024 in Denver and online!

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!