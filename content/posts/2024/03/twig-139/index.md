---
title: "#139 Just Before the Release"
author: Felix
date: 2024-03-15
tags: ["switcheroo", "gnome-calendar", "pika-backup", "libadwaita", "impression"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from March 08 to March 15.<!--more-->

# Sovereign Tech Fund

[Sonny](https://matrix.to/#/@sonny:gnome.org) announces

> As part of the [GNOME STF (Sovereign Tech Fund)](https://foundation.gnome.org/2023/11/09/gnome-recognized-as-public-interest-infrastructure/) initiative, a number of community members are working on infrastructure related projects.
> 
> Here are the highlights for the past week:
> 
> We have been working hard on helping with and solving last minute issues for GNOME 46. This is the first GNOME release since we started the GNOME STF initiative and are very excited about our work rolling to millions of users.
> 
> Sophie opened a PR to [support git dependencies in the Cargo buildstream plugin](https://github.com/apache/buildstream-plugins/pull/62).
> This will make it much easier to work with GNOME core applications written in Rust.
> 
> Julian drafted images/sound support for notification portal V2.
> 
> * [libportal](https://github.com/flatpak/libportal/pull/147)
> * [xdg-desktop-portal](https://github.com/flatpak/xdg-desktop-portal/pull/1298)
> * [xdg-desktop-portal-gtk](https://github.com/flatpak/xdg-desktop-portal-gtk/pull/469)
> 
> Matt now has an end to end prototype for the Wayland-native accessibility stack he's been working on. [He published an update and instructions to run it.](https://toot.cafe/@matt/112079028740742715)
> 
> Jonas landed [New gestures (part 2): Introduce ClutterGesture](https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/2389). This is one of the building blocks present in the [GNOME Shell mobile](https://blogs.gnome.org/shell-dev/2022/09/09/gnome-shell-on-mobile-an-update/) project that we are working on upstreaming.
> 
> {{< video src="af2e8baa955e2f3c832dcb200828c71ab99f4ae81768723859579600896.webm" >}}
>
> Alice released [libadwaita 1.5](https://blogs.gnome.org/alicem/2024/03/15/libadwaita-1-5)
> 
> ![](52dc8aec2496ba1741bece4f3535650b3c0341ca1768722236983738368.png)
>
> Sam made a [website for Orca](https://snwh.pages.gitlab.gnome.org/orca.gnome.org/) to replace the wiki page.
>
> ![](b156439b72cf499d578b6b3f849bb0b5d1bdf5a81768722396086272000.png) 
>
> This week we welcome and thank [Codethink](https://www.codethink.co.uk/) for partnering with us. Codethink has been a long time supporter of the GNOME project and will be helping us improve developer and quality assurance tooling; with a focus on immutable / image based operating systems.

# GNOME Core Apps and Libraries

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alice (she/her)](https://matrix.to/#/@alexm:gnome.org) announces

> Libadwaita 1.5.0 is out! See the [announcement blog post](https://blogs.gnome.org/alicem/2024/03/15/libadwaita-1-5/) for details

### Calendar [↗](https://gitlab.gnome.org/GNOME/gnome-calendar/)

A simple calendar application.

[Hari Rana | TheEvilSkeleton (any/all)](https://matrix.to/#/@theevilskeleton:fedora.im) announces

> [Daniel Garcia Moreno](https://danigm.net/) submitted several merge requests to GNOME Calendar that allowed us to close 25 timezone-related issues! All of these changes are expected to land in GNOME 46.
> 
> * https://gitlab.gnome.org/GNOME/gnome-calendar/-/merge_requests/370
> * https://gitlab.gnome.org/GNOME/gnome-calendar/-/merge_requests/372
> * https://gitlab.gnome.org/GNOME/gnome-calendar/-/merge_requests/373
> * https://gitlab.gnome.org/GNOME/gnome-calendar/-/merge_requests/375

# GNOME Circle Apps and Libraries

### Switcheroo [↗](https://gitlab.com/adhami3310/Switcheroo)

Convert and manipulate images.

[Khaleel Al-Adhami](https://matrix.to/#/@adhami:matrix.org) says

> Switcheroo now supports exporting multiple images into one PDF file in update 2.1.0!

### Pika Backup [↗](https://apps.gnome.org/app/org.gnome.World.PikaBackup/)

Keep your data safe.

[Sophie (she/her)](https://matrix.to/#/@sophieherold:gnome.org) announces

> Pika Backup 0.7.1 is out. It fixes a bug that prevented backup processes from lowering their CPU priority. A UI issue with scheduled backups was fixed as well.
> 
> If you missed the 0.7 release because we missed posting it on TWIG, you can learn more about it in my [blog post](https://blogs.gnome.org/sophieh/). There is also a [great video by Dreams of Autonomy](https://www.youtube.com/watch?v=W30wzKVwCHo) that gives a wonderful introduction to Pika Backup.
> 
> You can support Pika's development on [Open Collective](https://opencollective.com/pika-backup). Note that we are *not* affected by the Open Collective Foundation shutting down since our financial host is the Open Source Collective. The same is the case for almost all other open source projects. So please continue supporting them.

### Impression [↗](https://apps.gnome.org/Impression)

Create bootable drives.

[Khaleel Al-Adhami](https://matrix.to/#/@adhami:matrix.org) reports

> Impression has received a new update 3.1.0 to support .xz compressed file format and fix a bug that was causing slow download speeds

# Third Party Projects

[Arjan](https://matrix.to/#/@amolenaar:matrix.org) says

> This week @lazka (Christoph Reiter) released [PyGObject](https://gitlab.gnome.org/GNOME/pygobject) 3.48.1.
> 
> This release contains a couple of noteworthy changes:
> 
> * This is the first release using [meson-python](https://meson-python.readthedocs.io/), and thus meson, instead of setuptools for PEP-517 installations. I.e. when installing via pip or similar.
> * PyGObject finally has proper support for fundamental types. That means that you can now work with things like GSK nodes directly from Python.
> * The documentation for PyGObject is now hosted on our GNOME hosting environment at https://gnome.pages.gitlab.gnome.org/pygobject/. We aim to have all PyGObject related documentation in one place.

[Nokse](https://matrix.to/#/@nokse22:matrix.org) says

> I have released a new version of [ASCII Draw](https://flathub.org/en-GB/apps/io.github.nokse22.asciidraw) with many improvements:
> * Greatly **improved performance**, now you can use a bigger canvases
> * **Improved design** to better match the GNOME style
> * Added **stepped line** and merged all lines and arrows in one tool
> * Added **move tool** to easily move part of your drawings
> * Improved default character list dividing them into **palettes**
> * Added **custom palettes**
> * Added primary and secondary character
> ![](ytoCkLgGOHdhuQvozImANXsd.png)
> ![](BtFhOOqloWJPswVDKmDIoOUT.png)
> ![](FPlXQlXoqyQhAZBVqjuznCDY.png)

[Guido](https://matrix.to/#/@agx:sigxcpu.org) says

> I've released [livi 0.1.0](https://gitlab.gnome.org/guidog/livi/-/releases/v0.1.0). Thanks to Robert Mader the mobile focused video player now supports DMABuf import and can use GTK's new [GraphicsOffload](https://docs.gtk.org/gtk4/class.GraphicsOffload.html) widget to render videos more efficiently (given all other components in the stack support this properly already).
> ![](ADCyqQSEuJJiGCerziNVYGnL.png)

[Aaron Erhardt](https://matrix.to/#/@aaron:matrix.aaron-erhardt.de) announces

> Version 0.8 of Relm4, an idiomatic GUI library based on gtk4-rs, was released on Wednesday with many improvements. The release includes several unifications in our API, more idiomatic abstractions and updated gtk-rs dependencies. Find out more details in our [release blog post](https://relm4.org/blog/posts/announcing_relm4_v0.7/).

[Martín Abente Lahaye](https://matrix.to/#/@tchx84:matrix.org) says

> Gameeky 0.6.0 is out! This new release comes with improved compatibility with other platforms, several usability additions and improvements like:
> 
> * An integrated development environment for Python.
> * An easier way to share projects.
> * New desktop icon thanks to [@jimmac](https://github.com/jimmac) and [@bertob](https://github.com/bertob).
> * Improved compatibility with other platforms.
> * And more...
> 
> Check the release [blog post](https://blogs.gnome.org/tchx84/2024/03/13/gameeky-0-6-0/) to learn more.
> ![](qwWjWZIKNhgsstKKtNAIQQAj.png)

# GNOME Foundation

[Rosanna](https://matrix.to/#/@zana:gnome.org) announces

> This week, in between the minutiae of everyday things, I have also been looking into some of our policies and looking into updating them. Things like the employee handbook and travel policy are high on the list of things to update, to both keep aligned with regulations and best practices as well as streamlined for practicality. 
> 
> I am currently in Pasadena, California attending SCaLE. I am going to be on a panel (https://www.socallinuxexpo.org/scale/21x/presentations/where-does-linux-desktop-go-here) on Saturday at 2:30PM. It's going to be a great time! I will also be staffing the GNOME booth there. Drop on by to discuss all things GNOME.
> 
> We also posted an opening for an Administrative Support Contractor (https://foundation.gnome.org/careers/). This person would be working with me to keep GNOME running and I am very much looking forward to reading all the applications!

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
