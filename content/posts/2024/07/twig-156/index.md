---
title: "#156 Happy Hacking"
author: Thib
date: 2024-07-12
tags: ["showtime", "gaphor", "gnome-shell", "glib", "libadwaita"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from July 05 to July 12.<!--more-->

# Sovereign Tech Fund

[Sonny](https://matrix.to/#/@sonny:gnome.org) reports

> As part of the [GNOME STF (Sovereign Tech Fund)](https://foundation.gnome.org/2023/11/09/gnome-recognized-as-public-interest-infrastructure) initiative, a number of community members are working on infrastructure related projects.
> 
> This week we express special thanks to all the people outside of the team who review our work such as Felipe Borges, Matthias Clasen, Jonas Ådahl, Christopher Davis, Ray Strode, Florian Müllner, Robert Mader, Bilal Elmoussaoui, Jordan Petridis, Carlos Garnacho, Sebastian Wick, and Lennart Poettering. Thank you!
> 
> Here are the highlights for the past couple of weeks
> 
> #### Nautilus file chooser portal
> 
> Antonio finished the implementation of using Nautilus as file chooser
> 
> {{< video src="332ecd4b1be646162c62276bbe480b971c819e0d1811837894101303296.mp4" >}}
> 
> #### App Intents
> 
> Andy revived the xdg-spec proposal for [app intents](https://gitlab.freedesktop.org/xdg/xdg-specs/-/merge_requests/81)
> 
> Andy drafted an [implementation in GLib](https://gitlab.gnome.org/GNOME/glib/-/merge_requests/4119)
> 
> #### URI handling
> 
> Andy updated the [URI handling implementation in xdg-desktop-portal](https://github.com/flatpak/xdg-desktop-portal/pull/1313) to make use of app intents.
> 
> #### Key Rack
> 
> Felix implemented [changing keyring password](https://gitlab.gnome.org/sophie-h/key-rack/-/merge_requests/23)
> 
> Felix improved [UI / keyboard accessibility](https://gitlab.gnome.org/sophie-h/key-rack/-/merge_requests/24)
> 
> Felix reworked how item attributes are getting displayed, the most common attributes are now getting shown in a more user friendly way and are translatable
> 
> #### Global Shortcuts
> 
> Dorota submitted a [GlobalShortcuts demo to ASHPD](https://github.com/bilelmoussaoui/ashpd/pull/214)
> 
> Dorota helped with the implementation in [xdg-desktop-portal-gnome](https://gitlab.gnome.org/GNOME/xdg-desktop-portal-gnome/-/tree/wip/matthiasc/global-shortcuts) and solved follow up issues
> 
> Dorota [drafted an implementation in libportal](https://github.com/flatpak/libportal/pull/153)
> 
> #### Accessibility
> 
> Adrien landed [replacing GtkTreeView in Baobab](https://gitlab.gnome.org/GNOME/baobab/-/merge_requests/74)
> 
> Joanie made several improvements to braille support in Orca.
> 
> Joanie worked on using atspi\_device\_generate\_mouse\_event in Orca (necessary, but not sufficient, for mouse event synthesis in Wayland).
> 
> Matt recorded a demo of Newton on GNOME OS and published a blog post on the current state of the Newton project [Update on Newton, the Wayland-native accessibility project](https://blogs.gnome.org/a11y/2024/06/18/update-on-newton-the-wayland-native-accessibility-project/)
> 
> Matt [documented the Newton D-Bus protocol for ATs](https://gitlab.gnome.org/mwcampbell/mutter/-/blob/newton/data/dbus-interfaces/org.freedesktop.a11y.xml)
> 
> Matt made several improvements and fixes ahead of the [integration of AccessKit in GTK](https://gitlab.gnome.org/GNOME/gtk/-/merge_requests/7401) to support accessibility on macOS and Windows
> 
> Adrien worked on the [accessibility support for the GtkEntryCompletion replacement in Nautilus](https://gitlab.gnome.org/GNOME/nautilus/-/merge_requests/1497)
> 
> #### oo7
> 
> Dhanuka is close to have keyring unlock prompt fully supported
> 
> {{< video src="45013af5f2f212c0f56e129ccf6a818d297afbfc1811838063798648832.webm" >}}
> 
> #### GNOME OS
> 
> Adrian made systemd-sysupdate [better at dealing with sysexts](https://github.com/systemd/systemd/pull/33398).
> 
> Martin [published a blog post](https://www.codethink.co.uk/articles/2024/A-new-way-to-develop-on-Linux) about the new developer tooling powered by sysexts.
> 
> Martin implemented "Build and publish system extensions for CI" in [Shell](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3390) and [Mutter](https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/3843).
> 
> #### USB Portals
> 
> Hubert is making good progress on usb portal; he added [tests](https://github.com/hfiguiere/libportal/tree/usb-portal) and [documentation](https://github.com/hfiguiere/flatpak-docs/tree/usb-portals) for it
> 
> #### Notifications
> 
> Julian continued work on the [notification portal specification v2](https://github.com/flatpak/xdg-desktop-portal/pull/1298), working with Georges and Sebastian to get the MR in shape. He also continued working on the various other parts of the new notifications, including the various portal backends and GNOME Shell.
> 
> #### GTK Platform Libraries
> 
> Alice started a [discussion about a platform library interface](https://gitlab.gnome.org/GNOME/gtk/-/issues/6821).
> 
> #### GNOME Shell Overview
> 
> Jonas opened an MR for a more [adaptive shell overview on small monitors](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3392)
>
> ![](b655fba78d09ba01a77327654e71ed0e53c712621811842388524531712.png)
> 
> #### GUADEC
> 
> A lot of the team members will be at GUADEC, we're looking forward to meet everyone. Don't hesitate to reach out.

[Martín Abente Lahaye](https://matrix.to/#/@tchx84:matrix.org) announces

> Building and testing system components is challenging, especially on immutable operating systems like GNOME OS.
> 
> To help solve the problem, at Codethink we [developed](https://www.codethink.co.uk/articles/2024/A-new-way-to-develop-on-Linux/) a lean collection of tools called [sysext-utils](https://gitlab.gnome.org/tchx84/sysext-utils) inspired by Lennart Poettering and Jordan Petridis. These tools streamline the developer workflow via system extensions, making iterating on system components safer and more effective.
> 
> More work is needed to perfect the experience so, if you are working on system components, we invite you to test these tools and [share](https://gitlab.gnome.org/tchx84/sysext-utils/-/issues) your feedback.
> 
> This project was developed in collaboration with the GNOME Foundation, through the Sovereign Tech Fund (STF).

[Sonny](https://matrix.to/#/@sonny:gnome.org) says

> We started a pilot program to reward vulnerability reports and fixes.
> 
> https://yeswehack.com/programs/gnome-bug-bounty-program
> 
> From €500 to €10,000 depending on criticality.
> For now only GLib is in scope but we will expand the list of modules and advertise as the program grows.
> 
> If you are a GNOME or freedesktop module maintainer and would like your module to join the pilot program; please [get in touch](mailto:stf@gnome.org).
> 
> In partnership with [YesWeHack](https://yeswehack.com/) and [Sovereign Tech Fund](https://www.sovereigntechfund.de/).
> 
> See [Bug Resilience Program](https://www.sovereigntechfund.de/programs/bug-resilience).

# GNOME Core Apps and Libraries



### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alice (she/her)](https://matrix.to/#/@alexm:gnome.org) reports

> libadwaita now has a new spinner widget, [`AdwSpinner`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.Spinner.html), as well as a paintable variant, [`AdwSpinnerPaintable`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.SpinnerPaintable.html). This spinner can be used at larger sizes, still works with animations disabled and the paintable version can be used in contexts like the `AdwStatusPage` icon
> {{< video src="4067bdc35fe1bf7af97eadc464f9b52cfcd8e46b1811437392997908480.mp4" >}}

### GNOME Shell [↗](https://gitlab.gnome.org/GNOME/gnome-shell)

Core system user interface for things like launching apps, switching windows, system search, and more.

[jadahl](https://matrix.to/#/@jadahl:matrix.org) reports

> This week support for mixing HDR and regular content landed in [Mutter](https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/3433) and [GNOME Shell](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3363). When the [experimental HDR mode](https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/2879) is enabled, GNOME Shell itself as well and Wayland and X11 clients connecting via Xwayland will now be displayed correctly instead of appearing too bright. While there is currently no way for clients to tag their windows as HDR, one can for the time being manually do so via [looking glass](https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/3433#note_2149277), until the Wayland bits are ready.

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) announces

> Support for network monitoring on macOS in GLib has landed, written by Leo Assini: https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3744

# GNOME Incubating Apps



### Showtime [↗](https://apps.gnome.org/Showtime/)

Watch without distraction

[kramo](https://matrix.to/#/@kramo:matrix.org) says

> Showtime is now available on Flathub. The app is a modern video player aiming to be the successor to Videos.
> 
> Download the app [here](https://flathub.org/apps/org.gnome.Showtime).
> ![](AqxIfdQsqfkDcIJibNwfhGVE.png)

# GNOME Circle Apps and Libraries

[Brage Fuglseth](https://matrix.to/#/@bragefuglseth:gnome.org) reports

> This week [Valuta](https://apps.gnome.org/Valuta) joined GNOME Circle. Valuta lets you quickly convert between currencies. Congratulations!
> ![](effbd39951c4708610ef297571cd0062639990431811834399860195328.png)


### Gaphor [↗](https://gaphor.org)

A simple UML and SysML modeling tool.

[Arjan](https://matrix.to/#/@amolenaar:matrix.org) reports

> Last week Dan Yeaw released Gaphor 2.26.0. Gaphor is a SysML/UML modeling application.
> 
> The main improvements for this release is session recovery. Gaphor is able to restore your session after a crash. Also diagrams can now have stereotypes attached, which allows you to add extra data to a diagram.
> 
> A full list of changes and fixes can be found in the [Change Log](https://github.com/gaphor/gaphor/blob/main/CHANGELOG.md).
> 
> As always you can find the latest release [on Flathub](https://flathub.org/apps/org.gaphor.Gaphor) and the Windows/macOS versions can be downloaded from [Gaphor’s download page](https://gaphor.org/download/).

# Third Party Projects

[JumpLink](https://matrix.to/#/@JumpLink:matrix.org) says

> This week, I am happy to announce the release of [ts-for-gir](https://github.com/gjsify/ts-for-gir) [v4.0.0-beta.6](https://github.com/gjsify/ts-for-gir/releases/tag/v4.0.0-beta.6). The ts-for-gir tool enables the generation of GObject Introspection types for TypeScript, facilitating the development of GJS projects using TypeScript.
> 
> Additionally, we have released the corresponding [handwritten TypeScript types](https://github.com/gjsify/gnome-shell) for Gnome Shell extensions, now available in [version 46.0.0](https://github.com/gjsify/gnome-shell/releases/tag/46.0.0).

[Casper Meijn](https://matrix.to/#/@caspermeijn:matrix.org) announces

> This week I [released](https://www.caspermeijn.nl/posts/read-it-later-0.6.0/) version 0.6.0 of Read It Later. This is a client for [Wallabag](https://www.wallabag.it/en), which allows you to save web articles and read them later. The significant changes are a redesigned “new article” user interface and faster startup times for accounts with numerous articles. Download on [Flathub](https://flathub.org/apps/details/com.belmoussaoui.ReadItLater).

# GNOME Foundation

[Caroline Henriksen](https://matrix.to/#/@chenriksen:gnome.org) reports

> : Today we’re announcing a significant leadership transition at the GNOME Foundation. Holly Million will be stepping down as Executive Director, with her last day being July 31. An Interim Executive Director, Richard Littauer, has joined the Foundation this week. Richard brings with him a wealth of open source leadership experience and will help ensure a smooth transition as we prepare for a new Executive Director search.
> 
> During the past year Holly achieved remarkable milestones, including drafting a five-year strategic plan, securing crucial fiscal sponsorships, and improving our financial operations. We extend our heartfelt gratitude for her contributions. The Board will begin the search for a permanent Executive Director soon. For more details, read the full announcement here: https://foundation.gnome.org/2024/07/12/gnome-foundation-announces-transition-of-executive-director/

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
