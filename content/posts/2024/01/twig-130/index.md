---
title: "#130 Bubble Expanding"
author: Felix
date: 2024-01-12
tags: ["loupe", "gnome-contacts", "mutter", "gaphor", "graphs", "gtk"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from January 05 to January 12.<!--more-->

# Sovereign Tech Fund

[Tobias Bernard](https://matrix.to/#/@tbernard:gnome.org) reports

> As part of the [GNOME STF (Sovereign Tech Fund)](https://foundation.gnome.org/2023/11/09/gnome-recognized-as-public-interest-infrastructure/) project, a number of community members are working on infrastructure related projects.
> 
> - Evan worked on adding the gir compiler sources to glib https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3796#note_1957818 and investigated updating the gobject-introspection GIR changes for async to be compatible with testsuite changes https://gitlab.gnome.org/GNOME/gobject-introspection/-/merge_requests/404
> - Matt's project to prototype a new free desktop accessibility stack is now code-named "Newton". The first draft of the Wayland protocol for applications/toolkits is available here: https://gitlab.freedesktop.org/mwcampbell/wayland-protocols/-/blob/accessibility/staging/accessibility/accessibility-v1.xml?ref_type=heads and the implementation of the toolkit side in AccessKit is available here: https://github.com/AccessKit/accesskit/tree/unix2-prototype
> - Tobias worked on some initial mockups for URI handling settings https://gitlab.gnome.org/Teams/Design/settings-mockups/-/blob/master/applications/apps-reset-handlers.png
> - Julian worked on notification bubble expanding
> {{< video src="notifications.webm" >}}
> - Joanie continued working on improving Orca:
>     - Clear the cache recursively for table cells
>     - Code cleanups and fixes specific to the new keyhandling/key grabs
>     - Set up feature bindings on demand rather than in init
>     - Various other cleanups and fixes
> - Sam landed a number of cleanups and improvements to the GNOME Shell stylesheet:
>     - Cleaned up color definitions and the high contrast conditional so it can be more readily worked on https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3097
>     - Big refactor of button and entry drawing code so all elements of that type will appropriately inherit focus and high contrast styles https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3109
>     - Fixed missing focus state visuals in Calendar popover so focus is properly tracked https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3102
> - Work and discussions around redesigning the Orca settings are also still ongoing https://gitlab.gnome.org/Teams/Design/os-mockups/-/issues/240
> - Jonas resumed working on new GNOME Shell gestures with Carlos https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/2389
> - Jonas is looking into optimizing the shell overview for lower resolutions (1366×768 still is the second most popular screen resolution out there)
> - Adrian reimagined and reimplemented his approach for adding bulk directories to systemd-homed https://github.com/systemd/systemd/pull/30840
> - We started working with Eitan Isaacson of libspiel and are excited about the future of TTS https://blog.monotonous.org/2024/01/10/Introducing-Spiel

# GNOME Core Apps and Libraries

### Mutter [↗](https://gitlab.gnome.org/GNOME/mutter/)

A Wayland display server and X11 window manager and compositor library.

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) reports

> Mutter has been receiving some documentation love recently
> 
> * A brand new website made by Jakub Steiner https://mutter.gnome.org/
> * Re-organised and improved [contributing guidelines](https://gitlab.gnome.org/GNOME/mutter/-/tree/main#contributing) by swick
> 
> Hopefully that will make it easier for you to send your first patch!

### Image Viewer (Loupe) [↗](https://apps.gnome.org/app/org.gnome.Loupe/)

Browse through images and inspect their metadata.

[Sophie (she/her)](https://matrix.to/#/@sophieherold:gnome.org) says

> Thanks to the design work by Jakub and Calvin, Loupe has now a new animation for switching images via keyboard and buttons. We got repeated feedback that the previous animation can cause dizziness for some people (including me) on larger screens. Therefore we no longer animate the image moving over the complete screen like on smartphones. Instead, in simplified terms, we only animate the image moving a small and fixed amount of pixels over the old image. On touchscreens and with touchpads the image can still be moved directly as before. Thanks to everyone who helped with feedback and testing.

### GTK [↗](https://gitlab.gnome.org/GNOME/gtk)

Cross-platform widget toolkit for creating graphical user interfaces.

[Matthias Clasen](https://matrix.to/#/@matthiasc:gnome.org) reports

> The GTK 4.13.5 release includes not one, but two new renderers. Their names are vulkan and ngl, and we also call them __unified__ renderers, since they are built from the same sources.
> The new renderers can handle many corner cases correctly that the current gl renderer does not handle, and they offer advantages such as antialiasing and supersampled gradients.
> 
> The ngl renderer does not currently support GLES 2.
> 
> The new renderers are still considered experimental, and GTK will only use them if they are explicitly selected using the
> GSK\_RENDERER environment variable. The default renderer is still the current gl renderer.
> 
> As part of this work, the GSK include files have been rearranged. It is no longer necessary to include renderer-specific headers for ngl and vulkan (and doing so will trigger deprecation warnings), and their constructors are always available.
> 
> The previously available experimental GdkVulkanContext APIs and the old Vulkan renderer have been removed.
> 
> Vulkan support is now enabled by default, and Linux distributions should build GTK with Vulkan. This requires the glslc shader compiler as a new dependency.
> 
> Vulkan is now also used for dmabuf support.

### GNOME Contacts [↗](https://gitlab.gnome.org/GNOME/gnome-contacts)

Keep and organize your contacts information.

[nielsdg](https://matrix.to/#/@nielsdg:gnome.org) reports

> [Contacts](https://gitlab.gnome.org/GNOME/gnome-contacts) can now import multiple vCard files at once and preview the contacts you're about to import to avoid any mistakes. Already part of the 46.alpha release, so grab it while it's hot!
> ![](e26b8eb9840bf8f40600c48d6e6c3fda6496ba7f1745863383346839552.png)

# GNOME Circle Apps and Libraries

### Gaphor [↗](https://gaphor.org)

A simple UML and SysML modeling tool.

[Dan Yeaw](https://matrix.to/#/@Yeaw:matrix.org) announces

> [Gaphor](https://flathub.org/apps/org.gaphor.Gaphor), the simple modeling tool, version 2.23.0 is now out! Thanks to the amazing work by Arjan, this version features the following enhancements for our use of GTK:
> * Move from Gtk.TreeView to Gtk.ColumnView and ListView for our Property Editors
> * Move from Gtk.FileChooserNative to Gtk.FileDialog
> * Move from our own custom Gtk.Revealer to Adw.Toast for our in-app notifications
> * Remove the scary filesystem access warning in FlatHub by using the Filesystem Portal for Flatpak
> 
> We also made other feature enhancements:
> * Support types for parameters
> * Restore windows in maximized and full-screen state
> * Fine grained CSS: elements in a presentation item can be changed from CSS (experimental)
> * Very long element names are now wrapped
> ![](JGSWyFApskgUQSPlOMPibwhC.png)

# Third Party Projects

[Sophie (she/her)](https://matrix.to/#/@sophieherold:gnome.org) announces

> [Key Rack](https://flathub.org/apps/app.drey.KeyRack) is an app to view passwords and other secret stored by other apps. With the newly released version 0.3 it not only supports secrets stored in Flatpaks but also global secrets on the host. The new version also bring support for showing binary secrets in hex representation.
> ![](7d5e87c2d1eeb29b0e59c5d596a1cbde1f42ca9f1744110851041787904.png)

[Alain](https://matrix.to/#/@a23_mh:matrix.org) says

> Planify 4.4 is here, design improvements and new features available.
> 
> What’s New:
> 
> * Improved drag-and-drop sorting: Added a new animation and a new way of sorting making it cleaner and easier.
> * Support for sub-projects: Create a sub-project just by dragging and dropping or by using the magic button and dropping into the parent project.
> * Magic Button: Just drag and drop to add tasks wherever you want and in the order you want.
> * Board View: Have an overview of your tasks without losing sight of the details.
> * Using Adw.OverlaySplitView to have a collapsible sidebar.
> * Sections are now created from a Dialog and support color and a description.
> * Parent and child tasks: It is now easier to create subtasks, just drag the child task to the parent task and drop it.
> * New preference to configure whether to add the task at the beginning or end of the list.
> * Quick Add now allows you to select the target section.
> * Improved integration with Todoist, now allows you to create tasks with strange character content.
> * The description of a project is now editable instead of using a Dialog.
> * The design of the reminder creation has been improved.
> 
> Bug Fixed and Translations:
> 
> * Portugués translations thanks to @godinhoana54.
> * Russian translation update thanks to @hachikoharuno.
> * Turkish translation update thanks to @sabriunal.
> * Bugs fixed #1089, #1081, #1099, #1087, #1009, #1008, #1007.
> ![](CEQMXQhCUfQeWueZfQCxJGqO.png)
> ![](RXAbCrZXuUGElDkUacaYSoNf.png)
> {{< video src="SrxGAgwCguNAeQjYkbORobbY.webm" >}}
> {{< video src="BqPKccPnyvmBdZVqUWXcBmrX.webm" >}}
> {{< video src="jrowxFClFCsvhvCJDWrIssOH.webm" >}}

[Izzy Jackson (she/they)](https://matrix.to/#/@fizzyizzy05:matrix.org) says

> [Binary](https://flathub.org/apps/io.github.fizzyizzy05.binary) is an app to quickly convert between two different number bases, currently between Binary and Decimal. 0.1.3 is the latest release of the app.

### Graphs [↗](https://graphs.sjoerd.se)

Plot and manipulate data

[Sjoerd Stendahl](https://matrix.to/#/@sjoerdb93:matrix.org) says

> This week we have finally released Graphs 1.7.0. It's been a long process since the summer, but it's worth the wait. To say this is our biggest release yet would be an understatement, just as reference, at the last release we were at a total of 1474 commits in the repository while this release is at 2653 commits.
> 
> These changes may sound somewhat familiar for those who've read the beta announcement two weeks ago, but here's just a few of the major highlights of this release:
> 
> * A major UI overhaul with a new default theme and new libadwaita widgets from GNOME 45.
> * A completely revamped style editor with previews for each style
> * Curve fitting functionality
> * Touchpad gesture support for navigating the canvas
> * Many small quality-of-life improvements, such as persistent settings, better equation handling, and streamlined preferences.
> 
> These are just the major highlights, but a more complete changelog can be found on [GitHub](https://github.com/Sjoerd1993/Graphs/releases/tag/v1.7.0). To install the latest version of Graphs, we recommend you to get it [at Flathub](https://flathub.org/apps/se.sjoerd.Graphs)! I'd like to thank everyone involved for making this release possible. Particularly my co-developer Christoph Kohnen, as well as Tobias Bernard, and Sophie (she/her)  for all the feedback in the GNOME Circle application.
> ![](SyxskdZoPGeFkvRbptjDlmiz.png)
> ![](RpXTZsGAaiHyaTmpBakgWKHi.png)
> ![](tMcdawlRReSqDqieikdkXMhI.png)

# Documentation

[Maximiliano 🥑](https://matrix.to/#/@msandova:gnome.org) says

> gi-docgen [learned](https://gitlab.gnome.org/GNOME/gi-docgen/-/merge_requests/201) how to load favicons, see its [docs](https://gnome.pages.gitlab.gnome.org/gi-docgen/project-configuration.html#the-extra-section) for more info.
> 
> I also [added](https://gitlab.gnome.org/World/design/emblem/-/merge_requests/40) a way to generate favicons for gi-docgen to Emblem!
> 
> There is a reference pull request at https://github.com/flatpak/libportal/pull/138.

# Miscellaneous

[eeejay](https://matrix.to/#/@eeejay:matrix.org) says

> I wrote an introductory blog post to libspiel, a new xdg speech API and framework https://blog.monotonous.org/2024/01/10/Introducing-Spiel/

# GNOME Foundation

[Caroline Henriksen](https://matrix.to/#/@chenriksen:gnome.org) says

> The GUADEC 2024 Call for Participation is now open! This year's conference will take place in-person Denver, USA from July 19-24 and will be streamed online for remote attendees. We are looking for both remote and in-person submissions, workshop and BoF requests may also be submitted during this call. All submissions are due by Feb 18. https://foundation.gnome.org/2024/01/10/guadec-2024-call-for-participation-opens/

[Caroline Henriksen](https://matrix.to/#/@chenriksen:gnome.org) announces

> The GNOME Foundation is looking for volunteers to help staff our booth at [FOSDEM](https://fosdem.org/2024/). If you are attending in Brussels (Feb 3-4) and are interested in helping please take a look at our post on Discourse for more details: https://discourse.gnome.org/t/call-for-volunteers-gnome-booth-at-fosdem/18931

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

