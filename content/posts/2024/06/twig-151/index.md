---
title: "#151 Pride Month"
author: Felix
date: 2024-06-07
tags: ["gtk-rs", "railway", "keypunch", "gtk"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from May 31 to June 07.<!--more-->

![A GNOME window with a pride flag background and a raised first on top of it](pride.png)

This Week in GNOME is dedicated to the struggles of all lesbian, gay, trans, inter, bi, pan, asexual, aromantic, non-binary, and queer people.

LGBTQIA+ people have always been an integral part of our project, across all different roles. We wish to take this opportunity to thank you all from the bottom of our hearts for all your contributions to GNOME and to our community as whole.

We as a project have a fundamental responsibility to ensure the safety and well-being of everyone in our community. This can only be accomplished by prioritizing marginalized people’s safety over privileged people’s comfort. To accomplish this we expect everyone to follow our [Code of Conduct](https://conduct.gnome.org/), we moderate our chat rooms via cross-community moderation lists and we believe in speaking up whenever something is not right. This takes a remarkable effort every single day, and we still have a lot to learn and to improve. Let us learn and grow together, to become the best we can be.

Our thoughts go out to all the people that have to fight for their right to exist. We stand with you and we fight with you.

# Sovereign Tech Fund

[Sonny](https://matrix.to/#/@sonny:gnome.org) reports

> As part of the [GNOME STF (Sovereign Tech Fund)](https://foundation.gnome.org/2023/11/09/gnome-recognized-as-public-interest-infrastructure) initiative, a number of community members are working on infrastructure related projects.
> 
> Here are the highlights for the past two weeks:
> 
> # Tooling and QA
> 
> Martin published a post on discourse: [Towards a better way to hack and test your system components](https://discourse.gnome.org/t/towards-a-better-way-to-hack-and-test-your-system-components/21075/1). Feedback welcome!
> 
> Martin made an [initial implementation of sysext-utils](https://gitlab.gnome.org/tchx84/sysext-utils/-/merge_requests/1) based on the proposal ahead.
> 
> Abderrahim [made a GNOME OS single installer image](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/merge_requests/2923) that can install both the ostree and the sysupdate variants. The ostree variant can be dropped when sysupdate is no longer considered experimental.
> 
> # Platform
> 
> Alice [wrote a blog post about all of the CSS work](https://blogs.gnome.org/alicem/2024/06/07/css-happenings/)
> 
> Alice landed [AdwMultiLayoutView](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1125) in libadwaita.
> 
> Sophie added option to [apply image orientation](https://gitlab.gnome.org/sophie-h/glycin/-/merge_requests/84) to texture data in glycin.
> 
> Sophie [split C bindings](https://gitlab.gnome.org/sophie-h/glycin/-/merge_requests/86) into libglycin and libglycin-gtk4 and added necessary features such that projects that don't want/need to depend on GTK (liker mutter) can use glycin without depending on it.
> 
> # Flatpak
> 
> Dorota [added an internal API to GNOME Shell](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3343) that provides additional functionality for managing key grabs, suitable for the GlobalShortcuts portal.
> 
> Hub [fixed a leak](https://github.com/flatpak/flatpak/issues/5816) in Flatpak that was spoiling the CLI output.
> 
> Hub wrote the Flatpak docs chapter on [fallback devices](https://github.com/flatpak/flatpak-docs/pull/478).
> 
> António landed many refactors and the part 1 of [FileChooser portal implementation](https://gitlab.gnome.org/GNOME/nautilus/-/merge_requests/1502) for Nautilus.
> 
> # Accessibility
> 
> Matt implemented support for the GtkTextView multi-line text widget in [the GTK AccessKit implementation](https://gitlab.gnome.org/mwcampbell/gtk/tree/accesskit).
> 
> Matt [fixed a bug](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1134) in libadwaita that caused the accessibility tree under an AdwTabView or AdwViewStack to be inconsistent, leading to a crash in AccessKit.
> 
> Matt started key grabbing protocol support in Mutter/Orca to improve screen reader compatibility in Wayland.
> 
> * [Mutter](https://gitlab.gnome.org/mwcampbell/mutter/tree/newton)
> * [Orca](https://gitlab.gnome.org/mwcampbell/orca/tree/newton)
> 
> Georges fixed a bug in WebKit where accessible events aren't notified
> 
> * Multidisciplinary fix, pending reviews but the relevant people are MIA
> * WebKit: https://github.com/WebKit/WebKit/pull/29052
> * Flatpak: https://github.com/flatpak/flatpak/pull/5828
> * xdg-dbus-proxy: https://github.com/flatpak/xdg-dbus-proxy/pull/61
> 
> # Secrets
> 
> Felix [added file monitor](https://gitlab.gnome.org/sophie-h/key-rack/-/merge_requests/16) to Key Rack to watch for Flatpak app keyring changes
> 
> Felix [landed a major refactor](https://gitlab.gnome.org/sophie-h/key-rack/-/merge_requests/15) in Key Rack to merge logic for system and flatpak items.
> 
> Dhanuka implemented the secret prompt interface in oo7
> 
> * [Implemented org.gnome.keyring.Prompter interface](https://github.com/bilelmoussaoui/oo7/pull/73/commits/4530a0bb89aa3a5ead41204f61956992bf144b92)
> * [Implemented org.freedesktop.Secret.Prompt interface](https://github.com/bilelmoussaoui/oo7/pull/73/commits/22e1f8604c88adcb1c02a72ca8cf213323f9bc44)
> * [Integrated secret prompt into oo7-daemon](https://github.com/bilelmoussaoui/oo7/pull/73/commits/fc9bc209d43aeca15fbdad50459b13a1529bf98a)
> 
> # Interoperability
> 
> Andy landed tons of things on GNOME Online Account
> 
> * [SRV lookup](https://gitlab.gnome.org/GNOME/gnome-online-accounts/-/merge_requests/227)
> * [Parallel SRV lookups for DAV](https://gitlab.gnome.org/GNOME/gnome-online-accounts/-/merge_requests/227)
> * [IMAP/SMTP autoconfig](https://gitlab.gnome.org/GNOME/gnome-online-accounts/-/merge_requests/239)
> * [Kerberos KEYRING support (big battery improvements)](https://gitlab.gnome.org/GNOME/gnome-online-accounts/-/merge_requests/211)
> * [Port to AdwDialog](https://gitlab.gnome.org/GNOME/gnome-online-accounts/-/merge_requests/232)
> * [Settings: don't show account details when setup completes](https://gitlab.gnome.org/GNOME/gnome-control-center/-/merge_requests/2625)

[Neill Whillans](https://matrix.to/#/@neillydun:matrix.org) says

> More visible CVE reports for gnome-build-meta                                                                                                                                                                                                        Recently we have made a few changes to the generation and accessibility of CVE reports for gnome-build-meta.
> 
> These changes include the generation of additional CVE reports for both the vm and vm-secure manifests, for master and all future, supported release branches. We also make use of Gitlab Pages to automatically publish not only the CVE reports for the master branch, but also reports for each currently supported release branch. A list of these individual reports can be obtained through a newly added badge that can be found at the top of gnome-build-meta’s README.
> 
> This work was carried out as part of Codethink’s collaboration with the GNOME Foundation, through the Sovereign Tech Fund (STF).
> ![](mdFeoyAZqjMuBrdEAWuuepNT.png)
> ![](HmIeWxjGNEHJhWWZjtMHoApb.png)

# GNOME Core Apps and Libraries

### GTK [↗](https://gitlab.gnome.org/GNOME/gtk)

Cross-platform widget toolkit for creating graphical user interfaces.

[Alice (she/her)](https://matrix.to/#/@alexm:gnome.org) reports

> I wrote a blog post about the recent work in GTK to modernize its CSS engine, and the implications for apps using GTK and libadwaita: https://blogs.gnome.org/alicem/2024/06/07/css-happenings/

# GNOME Incubating Apps

[Sophie (she/her)](https://matrix.to/#/@sophieherold:gnome.org) reports

> [Showtime](https://gitlab.gnome.org/GNOME/Incubator/showtime) has been accepted into the GNOME Incubator. The GNOME [incubation process](https://gitlab.gnome.org/GNOME/Incubator/Submission) is for apps that are designated to be accepted into [GNOME Core or GNOME Development Tools](https://gitlab.gnome.org/Teams/Releng/AppOrganization/-/blob/main/OfficialAppDefinition.md) if they reach the required maturity.
> 
> Showtime is slated to replace Totem, GNOME's current video app. The reason is that Totem has now been unmaintained for nearly a year and still uses GTK 3.
> 
> You can help improve Showtime by testing [the nightly version](
> https://welcome.gnome.org/app/Showtime/#installing-a-nightly-build) and contributing. The incubation progress will be [tracked in this issue](https://gitlab.gnome.org/Teams/Releng/AppOrganization/-/issues/29).
> ![](61a57394f46e80abca4747619b07995911b2834e1799029095082754048.png)

# GNOME Circle Apps and Libraries

### Railway [↗](https://gitlab.com/schmiddi-on-mobile/railway)

Travel with all your train information in one place.

[Markus Göllnitz](https://matrix.to/#/@camelcasenick:matrix.org) says

> We have some great news about Railway! @schmiddi:matrix.org has done a tremendous job at boring up the backend code of the application, as well as its library counterpart. Railway is no longer limited to the HAFAS API. 🎉 That is not just a theoretical improvement: As the first newly supported network, you can test the Swiss SBB through search.ch. 
> 
> If you use Railway and want to test this new network, or want to help us make sure we did not introduce obscure regressions, be sure to test our Railway beta release on Flathub beta: `flatpak install https://flathub.org/beta-repo/appstream/de.schmidhuberj.DieBahn.flatpakref`. If you do test it: Thank You! And feel free to share feedback with us over at [#railwayapp:matrix.org](https://matrix.to/#/#railwayapp:matrix.org) or [at our issue tracker](https://gitlab.com/schmiddi-on-mobile/railway/-/issues).
> 
> Aaaand … @schmiddi:matrix.org even went so far as to implement support for MOTIS. You never heard of that? It is used by an open data based routing project: the [Transitous](https://transitous.org/) project. As they say about themselves, “Transitous is a community-run provider-neutral international public transport routing service.” If you hate the need to change the network in Railway, care about FLOSS server-side as much as client-side, and want to see open data in open formats, such as GTFS, excel, you will love this. In the beta release, you can find it as “Worldwide (beta) using Transitous,” but bear in mind that official providers are still more accurate, more up-to-date, and will stay for a reason. If you want to see that changed, you can get involved with said project.

### gtk-rs [↗](https://gtk-rs.org/)

Safe bindings to the Rust language for fundamental libraries from the GNOME stack.

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) says

> It took a bit too long but we have finally released a blog post that covers the changes that landed in the latest Rust bindings release. https://gtk-rs.org/blog/2024/06/01/new-release.html

# Third Party Projects

[Nokse](https://matrix.to/#/@nokse22:matrix.org) reports

> I have released [Exhibit](https://flathub.org/apps/io.github.nokse22.Exhibit), an app to view 3D models, powered by [F3D](https://f3d.app/).
> 
> Some of the features are:
> 
> * Support for many file formats, including glTF, USD, STL, STEP, PLY, OBJ, FBX, Alembic and many more
> * Many rendering and visual settings
> * HDRI or colored background
> * Export render to image
> * Drag and drop files
> * It installs mimetype and icon for supported file types
> 
> For now it only works with X11/Xwayland because the library can only be compiled to work with one compositor at a time.
> ![](pEbgvbXvRfkFGwJPScWSDBuP.png)
> ![](VCrHmfZDCVLWsQqtkrqScHIx.png)
> ![](hWzgXozeoEHsZuicmzNkiFhQ.png)
> {{< video src="hXvTrFoiTiDribnIpOeBgTDc.mp4" >}}

[Daniel Wood](https://matrix.to/#/@dubstar_04:matrix.org) announces

> Design - 2D CAD for GNOME has recieved an update with lots of changes. This update forms a foundation for more advanced drawing entities like dimensions. Highlights include:
> 
> * Manage text and line styles from the preferences window
> * Update text and line style from the properties window
> * Highlight design entities on mouse hover
> * Fix opening files as arguments e.g. 'open with' from file browser
> * Improve Trim and Extend command selection highlighting
> * Fix behaviour when no commands have been performed
> * Prevent selection of items on locked or frozen layers
> * Fix ortho and polar snaps when typed point data is entered
> * Update rotate command to match typical workflow
> * Underline shortcut letter of command options
> * Use consistent prompt for command with options
> * Remove custom headerbar buttons from layers window
> * Use RGB colours internally for all colour definitions
> * Support DXF true colours
> * Fix issue with layers being updated with incorrect state when editing layer data
> * Draw entities using correct colour when part of a block, supporting colour 'ByBlock'
> * Add Block command, enabling creation of blocks
> * Add Explode command, enabling decomposition of blocks into primitive entities
> * Add Hatch command and common hatch patterns
> * Update to the latest GNOME dev kit icons
> * Add Purge command, clearing unused, blocks, layers and line types
> * Show suggestions for invalid command input
> 
> Get it on Flathub: https://flathub.org/apps/io.github.dubstar_04.design
> ![](qNwxAWctYhwPwiKlppYoJgbK.png)
> ![](dSFANpESdYaOMhfKOSUZBHnL.png)

### Keypunch [↗](https://github.com/bragefuglseth/keypunch)

Practice your typing skills

[Brage Fuglseth](https://matrix.to/#/@bragefuglseth:gnome.org) reports

> How fast can you type? Find out with my new app, Keypunch!
> 
> I've worked on this for the last couple of months, and it's finally out. Keypunch lets you practice your typing skills with automatically generated pseudo-text in your language of choice. Alternatively, you can supply it with your own text, such as song lyrics, Wikipedia articles, and quotes. Get ready to accelerate your typing speed!
> 
> Get it on Flathub: https://flathub.org/apps/dev.bragefuglseth.Keypunch
> ![](aba33d137708706fed025fd0821fb55ddd2870ff1798410037652094977.png)

# Shell Extensions

[Arca](https://matrix.to/#/@arcaege:matrix.org) says

> **Day Progress**: an extension that shows a progress bar of your day to help you track your time.
> 
> I actually released this last week, but it took another week for it to become stable enough to call it a stable release. Here's how it works: you set the start and reset (end) times in the extension preferences (for example, the start and end of your workday) and it displays a progress bar of the current time in the top panel.
> 
> Features:
> * Customisable start and end times, including the ability to wrap around midnight
> * Option to show time elapsed instead of remaining
> * Customisable bar width and height
> * Option to display a bar with smooth, curved ends (experimental)
> * Customisable panel position and index
> * Shows percentage and time elapsed and remaining in the tooltip (see image below)
> 
> You can download the extension at: https://extensions.gnome.org/extension/7042/day-progress/
> The GitHub repository is at: https://github.com/ArcaEge/day-progress
> 
> ![screenshot image](https://extensions.gnome.org/extension-data/screenshots/screenshot_7042_utrZmJN.png)

# Internships

[Pablo Correa Gomez](https://matrix.to/#/@pabloyoyoista:matrix.org) announces

> as part of GSoC camelCaseNick landed support for floating zoom buttons in Papers! This design pattern was part of the mockups for more than 5 years, but was never implemented! This is a first step to make the top bar fitting on mobile!
> ![](qvlaseGaiFiINnZBRrjiWqqh.png)

# Events

[Asmit Malakannawar](https://matrix.to/#/@asmit2952:matrix.org) reports

> Hello everyone,
> 
> We're thrilled to announce that the call for volunteers and the call for BoFs for GUADEC 2024 is now open!
> 
> If you're passionate about making GUADEC 2024 a success and want to lend a helping hand, please fill out the volunteer form.
> 
> For those interested in presenting a workshop or leading a Birds of a Feather (BoF) session, we'd love to hear from you! Please fill out the BoFs form to submit your proposal.
> 
> [Volunteer Form Link](https://events.gnome.org/event/209/surveys/124)
> [BoFs Form Link](https://events.gnome.org/event/209/surveys/123)

# GNOME Foundation

[Rosanna](https://matrix.to/#/@zana:gnome.org) reports

> My week has been very busy with financial things. I had meetings with our bookkeeper and our accountant. With their support, our books have never looked as good as they do now. In addition, working within our board-approved balanced budget has meant that our numbers look healthier as well. This point was made more apparent to me when our accountant asked if we had considered finding higher interest accounts for our funds. More research on this is now on my to-do list.
> 
> The Foundation's Strategic Plan Draft https://foundation.gnome.org/strategicplan/ is still open for feedback. Please submit using the link on that page. While we value all feedback, having it all in one place makes it easier for us to make sure we don't lose any of your views.
> 
> If you haven't registered for GUADEC yet, please do so: https://events.gnome.org/event/209/registrations/! The earlier we know how many people are coming, the easier it will be for us to plan. And don't forget to register for the baseball game as well. Go Red Sox!
> 
> We are still looking for more sponsors for GUADEC. If you or your employer are interested, please let us know!

[Caroline Henriksen](https://matrix.to/#/@chenriksen:gnome.org) reports

> In honor of Pride Month, we'd like to give a special shout out to our LGBTQIA+ community. Thank you for contributing your voices and efforts to GNOME, we appreciate all of your hard work!
> 
> We’d also like to invite everyone to join us in celebrating Pride Month at a special GNOME Social Hour on June 24 at 17:30 UTC. Come chat with contributors and Foundation members, and share ideas on how we can better support our LGBTQIA+ community. https://discourse.gnome.org/t/celebrate-pride-month-at-the-gnome-social-hour-june-24/21421

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
