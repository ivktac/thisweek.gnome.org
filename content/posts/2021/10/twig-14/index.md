---
title: "#14 Well-Rounded"
author: Felix
date: 2021-10-15
tags: ["pika-backup", "tracker", "share-preview", "solanum", "gnome-text-editor", "sysprof", "webfont-kit-generator", "libadwaita", "gnome-shell"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from October 08 to October 15.<!--more-->

# Core Apps and Libraries

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Tobias Bernard](https://matrix.to/#/@tbernard:gnome.org) announces

> Alexander Mikhaylenko landed a number of styling updates, including a [new style for scrollbars](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/287) and [increased border radii](https://gitlab.gnome.org/GNOME/libadwaita/-/issues/265) across the entire stylesheet. Scrollbars are now rounded and no longer attached to the window edge, which makes for nicer visuals and a smoother state transition.
> ![](559d7f1462366a313a068d467aaf9b4ffb401bba.png)
> {{< video src="aa3114e4a70cb33f52d6c40395e8bef2e2b70d4b.webm" >}}

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) says

> libadwaita demo is now published in the GNOME Nightly Flatpak repository.
> ![](c46b4c8684f088f30f4726031de8fda5701ac606.png)

### Tracker [↗](https://gitlab.gnome.org/GNOME/tracker/)

A filesystem indexer, metadata storage system and search tool.

[Sam Thursfield](https://matrix.to/#/@ssssam:matrix.org) announces

> Tracker SPARQL database now provides much more helpful error messages for app developers who are writing their own ontologies. This work was done by Abanoub Ghadban as part of Google Summer of Code 2021. See https://gitlab.gnome.org/GNOME/tracker/-/merge_requests/452

### Sysprof [↗](https://gitlab.gnome.org/GNOME/sysprof)

A profiling tool that helps in finding the functions in which a program uses most of its time.

[Ivan Molodetskikh](https://matrix.to/#/@yalter:gnome.org) announces

> The development version of Sysprof [is now better](https://gitlab.gnome.org/GNOME/sysprof/-/merge_requests/45) at finding debug-info files for Flatpak apps. Combined with the introduction of frame pointers to `org.freedesktop.Platform` 21.08 and, recently, to the nightly [org.gnome.Platform](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/merge_requests/1330), this means that you can now use Sysprof to profile GNOME Flatpak apps and get correct stack traces. Just make sure that the app uses the nightly `org.gnome.Platform` and that you have installed `org.freedesktop.Sdk.Debug//21.08`, `org.gnome.Sdk.Debug//master` and `com.example.YourApp.Debug`. The stack traces should also work with Sysprof built into GNOME Builder ("Run with Profiler") once it is updated.
> ![](e6f57dfe9a1bf07474ed7e59432d541fff13445d.png)

### Text Editor [↗](https://gitlab.gnome.org/GNOME/gnome-text-editor/)

Text Editor is a simple text editor that focus on session management.

[Tobias Bernard](https://matrix.to/#/@tbernard:gnome.org) reports

> Christian Hergert landed a [new color scheme chooser](https://gitlab.gnome.org/GNOME/gnome-text-editor/-/issues/182#note_1288305) with inline previews.
> ![](b6a0054620d53af2975975a235708fe253c5689f.png)

### GNOME Shell [↗](https://gitlab.gnome.org/GNOME/gnome-shell)

Core system user interface for things like launching apps, switching windows, system search, and more.

[Ivan Molodetskikh](https://matrix.to/#/@yalter:gnome.org) says

> The work-in-progress [new GNOME Shell screenshot UI](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/1954) saw a number of fixes and updates this week. The panel now shows beautiful new icons for area, screen and window selection made by Tobias Bernard. Upon opening the UI, you're presented with a pre-selected starting area which you can now drag around and resize in all 8 directions, while the panel fades out of the way. You can also draw a new area by holding Ctrl or with the right mouse button. Capturing the screenshot no longer freezes the screen for a moment as PNG compression now happens on a separate thread. New screenshots appear in Recent items in the file manager. Also, you can now take screenshots of GNOME Shell pop-up menus without them glitching out the UI.
> 
> Additionally, I [opened a merge request](https://gitlab.gnome.org/GNOME/gnome-settings-daemon/-/merge_requests/267) to remove the screenshot hotkey handling from gnome-settings-daemon. "Take a screenshot" and "Take a screenshot of a window" hotkeys will be handled by GNOME Shell itself, bringing same-frame screenshots and notifications to window and full-screen screenshot hotkeys. For example, Alt-PrtSc now works for GTK 4 application pop-up menus.
> {{< video src="150affb5ccf48b2e72c78975535d858548a981c2.webm" >}}

### GWeather [↗](https://wiki.gnome.org/Projects/LibGWeather)

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) says

> GWeather, the library for gathering weather information used by various GNOME applications, has been updated to allow its use by GTK4 applications: https://gitlab.gnome.org/GNOME/libgweather/-/issues/151

# Circle Apps and Libraries

### Webfont Kit Generator [↗](https://github.com/rafaelmardojai/webfont-kit-generator)

Create @font-face kits easily.

[Rafael Mardojai CM](https://matrix.to/#/@rafaelmardojai:matrix.org) announces

> Webfont Kit Generator was ported to GTK4 and libadwaita with some small design improvements.
> ![](DTkrESLsXbAQLONcMUdbUMor.png)

### Solanum [↗](https://gitlab.gnome.org/World/Solanum)

Balance working time and break time.

[Chris 🌱️](https://matrix.to/#/@brainblasted:gnome.org) says

> Solanum now has preferences to set the various timer lengths and how many laps to go before a long break.
> ![](19af902113cc3b2002c6df9aae9d06a92795942f.png)

### Share Preview [↗](https://github.com/rafaelmardojai/share-preview)

Test social media cards locally.

[Rafael Mardojai CM](https://matrix.to/#/@rafaelmardojai:matrix.org) says

> Share Preview's metadata inspector has been moved to a separate dialog, and now also allows you to inspect document body images.
> ![](amApvjKoiQUzdEwbfdBduluK.png)

### Pika Backup [↗](https://wiki.gnome.org/Apps/PikaBackup)

Simple backups based on borg.

[Sophie Herold](https://matrix.to/#/@sophieherold:gnome.org) announces

> Pika Backup got ported to GTK 4 and libadwaita.
> 
> Observing readers might also spot a “Schedule” page in the screenshot. More about that in a future update.
> ![](29c20602cbfacd4a707dbcab28b32467f6a54be5.png)

# Third Party Projects

[Aaron Erhardt](https://matrix.to/#/@admin:matrix.aaron-erhardt.de) announces

> The second stable version of Relm4, an idiomatic GUI library based on gtk4-rs, was released and makes GUI development with Rust and GTK even better! 
> 
> Most notably, Relm4 now offers better error messages, improved macros and an even better integration of libadwaita. The full release article can be found [here](https://aaronerhardt.github.io/blog/posts/announcing_relm4_v0.2/). Also special thanks to [tronta](https://github.com/tronta) for adding the math trainer example.
> ![](CAlaZhWTLIGuxGiZiIlLnpYN.png)

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) says

> Maximiliano & I ported [Contrast](https://flathub.org/apps/details/org.gnome.design.Contrast) to GTK 4 & libadwaita.
> ![](12d3a25137db4b5699a4966e3fbdbe3b603a2bae.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

