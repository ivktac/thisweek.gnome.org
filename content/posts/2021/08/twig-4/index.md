---
title: "#4 Building..."
author: Felix
date: 2021-08-06
tags: ["highscore", "portfolio", "gnome-shell", "libadwaita", "epiphany", "kooha", "gnome-builder", "gjs", "fractal"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from July 30 to August 06.<!--more-->

# Core Apps and Libraries
### GNOME Builder [↗](https://gitlab.gnome.org/GNOME/gnome-builder)

IDE for writing GNOME-based software.

[James Westman](https://matrix.to/#/@flyingpimonster:matrix.org) says

> One of Builder's more obscure features--building and deploying a flatpak app to another device--got an upgrade. Once a device is set up, clicking "Run" now automatically builds, deploys, and runs the app, which is super useful for mobile development. [This blog post explains in more detail.](https://www.jwestman.net/2021/08/06/finishing-gnome-builder-mobile-support.html)

[gwagner](https://matrix.to/#/@gwagner:gnome.org) says

> GNOME Builder can now handle [flatpak make-args and make-install-args](https://gitlab.gnome.org/GNOME/gnome-builder/-/merge_requests/414). This makes it possible to develop applications like tuxpaint, which uses a pure Make Buildsystem, in a flatpak environment.

[vanadiae](https://matrix.to/#/@vanadiae:matrix.org) announces

> Builder [got a rework and redesign of its Replace in Files panel](https://gitlab.gnome.org/GNOME/gnome-builder/-/merge_requests/419). Now the search can be done too from the panel, search options can be changed again and overall the panel is no longer one-time-use. There's also a persistent project-wide panel that can be quickly accessed using Ctrl+Shift+F. Both should avoid having search panels piling up like they did previously.
> ![](cOUslsnPKRkZyjkSIdrsulDW.png)

[gwagner](https://matrix.to/#/@gwagner:gnome.org) says

> GNOME Builder uses [now](https://gitlab.gnome.org/GNOME/gnome-builder/-/merge_requests/424) the cmake codemodel to extract targets. This enables Builder to run CMake projects.

[Ryuukyu](https://matrix.to/#/@ryuukyu:matrix.org) announces

> Builder has received a new renderer for markdown documentation, which greatly improves readability in many cases.
> 
> [Merge Request](https://gitlab.gnome.org/GNOME/gnome-builder/-/merge_requests/416)
> ![](JVhyKPtjNAnkoxrpEagNaukb.png)

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Nahuel Gomez](https://matrix.to/#/@nahuelwexd:matrix.org) reports

> Libadwaita now introduces a new base class for Adwaita apps: [AdwApplication](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/177). It saves repetitive code when loading your custom styles, while handling library initialization.

### GNOME Shell [↗](https://gitlab.gnome.org/GNOME/gnome-shell)

Core system user interface for things like launching apps, switching windows, system search, and more.

[Ivan Molodetskikh](https://matrix.to/#/@yalter:gnome.org) says

> The work-in-progress [new GNOME Shell screenshot UI](https://imolodetskikh.wordpress.com/2021/06/29/gsoc-2021-gnome-shell-screenshot-ui/) can now optionally capture the mouse pointer. You can toggle the mouse pointer on and off in the UI after capturing the screenshot. Read more about it in [my GSoC update blog post](https://imolodetskikh.wordpress.com/2021/08/06/gsoc-2021-screenshots-with-pointer/)!
> {{< video src="acea4b6a0cf9aae435eaa00a5b3168949503b88d.webm" >}}

[Allan Day](https://matrix.to/#/@aday:gnome.org) announces

> Florian Müllner added the new power modes to GNOME Shell's system menu - https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/1907

### GJS [↗](https://gitlab.gnome.org/GNOME/gjs)

Use the GNOME platform libraries in your JavaScript programs. GJS powers GNOME Shell, Polari, GNOME Documents, and many other apps.

[ptomato](https://matrix.to/#/@ptomato:gnome.org) announces

> In GJS, Marco Trevisan reduced the memory usage of GObjects by 40 bytes per object. Evan Welsh fixed two crashes. I wrote some updated documentation for new contributors.

### Web [↗](https://gitlab.gnome.org/GNOME/epiphany)

Web browser for the GNOME desktop.

[philnOoO](https://matrix.to/#/@pnormand:igalia.com) reports

> The GNOME Flatpak nightly repo now hosts a Canary flavor of GNOME Web. This new flatpak includes developer snapshots of WebKitGTK and the developer version of GNOME Web. This flavor can be used to test WebKitGTK unstable features in GNOME Web. More details: https://base-art.net/Articles/introducing-the-gnome-web-canary-flavor/  https://gitlab.gnome.org/GNOME/epiphany/-/merge_requests/989/

# Circle Apps and Libraries
### Kooha [↗](https://github.com/SeaDve/Kooha)

A simple screen recorder with a minimal interface. You can simply click the record button without having to configure a bunch of settings.

[SeaDve](https://matrix.to/#/@sedve:matrix.org) says

> [Kooha](https://github.com/SeaDve/Kooha) can now handle multiple sources, such as recording multiple monitors or windows at once. There is also an opt-in hardware accelerated encoding for more efficient recording through VAAPI.

# Third Party Projects
### Fractal [↗](https://gitlab.gnome.org/GNOME/fractal)

Matrix messaging app for GNOME written in Rust.

[Alexandre Franke](https://matrix.to/#/@afranke:matrix.org) announces

> The Fractal interns kept busy, with Alejandro landing work on display names and avatars ([!801](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/801) and [!802](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/802)), and Kai working on room sorting in the sidebar.
> 
> New contributor 🎉 enterprisey [brought back unlocking of the secret service collection](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/804) and [fixed the use of RUST_BACKTRACE in our manifest](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/807).

### Highscore [↗](https://gitlab.gnome.org/World/highscore/)

Retro gaming application for GNOME.

[Adrien Plazas](https://matrix.to/#/@adrien.plazas:gnome.org) announces

> In Highscore, I added a high-definition rendering option, currently supported by Dreamcast, GameCube, Nintendo 3DS and Nintendo 64 games. https://gitlab.gnome.org/World/highscore/-/merge_requests/12
> ![](8f4fd37caf9228d69609a891ac56bff008f5275a.png)
> ![](fadfa6df03477bedf36c2059e7a3fbf3334cacd5.png)

### Portfolio [↗](https://github.com/tchx84/Portfolio)

A minimalist file manager for those who want to use Linux mobile devices.

[Martín Abente Lahaye](https://matrix.to/#/@tchx84:matrix.org) reports

> Portfolio 0.9.11 released! New icon, trash folder, speedups and [more](https://blogs.gnome.org/tchx84/2021/07/31/portfolio-0-9-11/).
> ![](jsiLPnOhszqkgxExPXceRTsE.png)

# Miscellaneous
[Allan Day](https://matrix.to/#/@aday:gnome.org) reports

> in Patterns, the new GNOME platform demo app, Alexander Mikhaylenko added demos for windows and utility panes, improved search, and added a welcome page. He also worked on scaffolding by adding CI, metadata, and an about dialog.
> ![](f07e8dd905e1f161f13c4b3322770270dac6cd9b.png)

[Sophie Herold](https://matrix.to/#/@sophieherold:gnome.org) reports

> I published a first preview for an [“Apps for GNOME” website.](https://sophie-h.pages.gitlab.gnome.org/malamute/) I hope that the new app pages can get more users engaged with an app's community and the development process! The website should also help in promoting the best apps in the GNOME ecosystem. I'm also trying to provide all of this with as much localization as possible.
> 
> You can read more about the “Apps for GNOME” project in [my latest blog post.](https://blogs.gnome.org/sophieh/2021/08/05/apps-for-gnome/)
> ![](c7b04374b23d987b6a00d23837cbf374108f61ea.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

