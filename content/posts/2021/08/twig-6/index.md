---
title: "#6 Sharing, Caring"
author: Thib
date: 2021-08-20
tags: ["gjs", "gnome-shell", "fractal", "gnome-podcasts", "gnome-calendar"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from August 13 to August 20.<!--more-->

# Core Apps and Libraries

### GNOME Shell [↗](https://gitlab.gnome.org/GNOME/gnome-shell)

Core system user interface for things like launching apps, switching windows, system search, and more.

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) reports

> Just in time for the 41.beta release, Settings received two new panels: Multitasking, and Cellular. The Multitasking panel provides functionality useful to improve your productivity when working with many apps at once. The Cellular panel allows configuring various aspects of mobile connections and modems.
> ![](af964f417fdb785e241ead463b5b97d6ff07bc2a.png)

[Ivan Molodetskikh](https://matrix.to/#/@yalter:gnome.org) says

> The work-in-progress [new screenshot UI](https://imolodetskikh.wordpress.com/2021/06/29/gsoc-2021-gnome-shell-screenshot-ui/) for GNOME Shell has got initial support for screen recording! I also created a [preliminary draft merge request](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/1954) where it's possible to follow the development.
> {{< video src="159aa1880ef6812d3935933f4c64b5ec6be9957f.webm" >}}

### Calendar [↗](https://gitlab.gnome.org/GNOME/gnome-calendar/)

A simple calendar application.

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) says

> GNOME Calendar is now able to open ICS files and import their events
> ![](3bf7fc41533435866368da41e729350845404ab0.png)

### GJS [↗](https://gitlab.gnome.org/GNOME/gjs)

Use the GNOME platform libraries in your JavaScript programs. GJS powers GNOME Shell, Polari, GNOME Documents, and many other apps.

[ptomato](https://matrix.to/#/@ptomato:gnome.org) reports

> In GJS, Evan Welsh added a `console` global object with methods such as `console.log()` that developers who have written JavaScript before will probably know. We hope this will make GJS feel more familiar, and allow reusing code more easily between GNOME and other JavaScript environments. PS. Before you ask, we are not planning to add `alert()` 😉
> 
> Daniel van Vugt added `setDeviceOffset()` and `setDeviceScale()` methods to `Cairo.Surface`, which wrap Cairo's similarly named C APIs. I added the corresponding `getDeviceOffset()` and `getDeviceScale()` as well.

# Circle Apps and Libraries

### Cozy [↗](https://github.com/geigi/cozy/)

An audiobook reader and manager.

[geigi](https://matrix.to/#/@geigi7:matrix.org) announces

> Cozy 1.1.2 has been released! It features improved support for mobile devices and fixes multiple small issues that occurred in the redesigned UI.
> ![](qdgKUwbKsNeBVCHHeVixxXfo.png)

### Podcasts [↗](https://gitlab.gnome.org/podcasts)

Podcast app for GNOME.

[nee](https://matrix.to/#/@nee:tchncs.de) reports

> [Podcasts](https://wiki.gnome.org/Apps/Podcasts) now has an [episode description page](https://gitlab.gnome.org/World/podcasts/-/merge_requests/178) where users can [read the notes](https://gitlab.gnome.org/World/podcasts/-/issues/93) for an episode, [full titles of long episode names](https://gitlab.gnome.org/World/podcasts/-/issues/203) and [share the episode url](https://gitlab.gnome.org/World/podcasts/-/issues/202).
> {{< video src="af46ce8d4f8e2dbb780dc6e5016fe46288c36807.mp4" >}}

[alatiera](https://matrix.to/#/@alatiera:matrix.org) announces

> Podcasts [0.5.0-beta](https://gitlab.gnome.org/World/podcasts/-/tags/0.5.0-beta) is out. Highlights include remembering the position you left off an episode, and the new episode description page! Get the new release from [flathub-beta](https://dl.flathub.org/beta-repo/appstream/org.gnome.Podcasts.flatpakref)! 


# Third Party Projects

### Relm4 [↗](https://github.com/AaronErhardt/relm4)

A GUI library inspired by Elm.

[Aaron Erhardt](https://matrix.to/#/@admin:matrix.aaron-erhardt.de) reports

> The first beta version of Relm4 was released. Relm4 is an idiomatic GUI library inspired by [Elm](https://elm-lang.org/) and based on [gtk4-rs](https://crates.io/crates/gtk4). It's goal is to make developing GTK4 apps in Rust simpler and more productive. More information can be found in the [release blog post](https://aaronerhardt.github.io/blog/posts/relm_beta/) and the [repository](https://github.com/AaronErhardt/relm4).

### Telegrand [↗](https://github.com/melix99/telegrand)

A Telegram client optimized for the GNOME desktop.

[Marco Melorio](https://matrix.to/#/@melix99:gnome.org) announces

> [Telegrand](https://github.com/melix99/telegrand) development has seen a great improvement over this week! I added the ability to load older messages and I also implemented the parser to correctly show the formatted text of the messages. Newbytee has added the ability to send messages by pressing enter.
> ![](3ae156dc25936e9d6ae9eeb7bb447c1a63c9939b.png)

### Fractal [↗](https://gitlab.gnome.org/GNOME/fractal)

Matrix messaging app for GNOME written in Rust.

[Alexandre Franke](https://matrix.to/#/@afranke:matrix.org) says

> Kai was on fire this week 🔥 with 5 (❗️) new merge requests that all landed! The highlights are that [rooms are now sorted by activity](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/810) and [room members now have a `power-level` property](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/811). The other ones ([!815](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/815), [!816](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/816), [!817](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/817)) are maintenance and code quality related.
> 
> Julian Hofer also contributed a code quality change by [making every subclass use `Default`](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/818).
> 
> Shout out to Julian Sparber, who’s still busy with [his upstream work in the Matrix Rust SDK](https://github.com/matrix-org/matrix-rust-sdk/pull/288), and managed to review and merge all the above contributions.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
