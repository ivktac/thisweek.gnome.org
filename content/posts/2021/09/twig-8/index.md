---
title: "#8 Fresh Sketches"
author: Felix
date: 2021-09-03
tags: ["libadwaita", "tracker", "health", "apostrophe"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from August 27 to September 03.<!--more-->

# Core Apps and Libraries

[Jakub Steiner](https://matrix.to/#/@jimmac:gnome.org) reports

> [Bilal has merged](https://gitlab.gnome.org/GNOME/gnome-tour/-/merge_requests/51) the fresh new GNOME 41 Tour, giving new users a nice welcome to their OS. Instead of screenshots here's some sketches from the design phase not to spoil the experience. You'll be able to enjoy the Tour when GNOME releases in September.
> ![](215eea2866786ff0e66a323d0d678afa49252c1e.png)
> ![](d9a5febf483ae8f33c43d652a0b3c2bd9dc38fa5.png)

### Tracker [↗](https://gitlab.gnome.org/GNOME/tracker/)

A filesystem indexer, metadata storage system and search tool.

[Sam Thursfield](https://matrix.to/#/@ssssam:matrix.org) announces

> Tracker (database and search engine) has adopted Hotdoc for documentation, the new-look reference manual is avaiable [here](https://gnome.pages.gitlab.gnome.org/tracker/docs/developer/)

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) says

> libadwaita has seen a few API changes this week, make sure to update your apps if you were using them:
> * `AdwWindow:child` and `AdwApplicationWindow:child` have been renamed  to `content` to avoid a name clash with `GtkWindow:child`
> * `AdwValueObject` has been removed, in most cases it can be replaced with `GtkStringList`
> * `AdwEnumValueObject` has been renamed to `AdwEnumListItem` to better reflect how it's used
> * `AdwViewSwitcherBar:policy` and `AdwViewSwitcherTitle:policy` have been removed.
> * `AdwViewSwitcherPolicy` has lost its `auto` value. `AdwViewSwitcherTitle` still provides the same behavior, and it can also be achieved using 2 view switchers in an `AdwSqueezer`
> * `AdwViewSwitcher:narrow-ellipsize` has been removed, this property should have never been public
> * `AdwSqueezer` has gained API to switch children based on their natural size instead of minimum size, mirroring `AdwLeaflet` and `AdwFlap`

# Circle Apps and Libraries

### Health [↗](https://gitlab.gnome.org/World/Health)

Collect, store and visualise metrics about yourself.

[Cogitri](https://matrix.to/#/@cogitri:cogitri.dev) announces

> I’ve added a new SpinButton-like widget that displays what unit users are entering values in and automatically adjust its value to unit system changes (e.g. metric -> imperial). Also, Health has been added to Damned Lies recently, so I’ve fixed some of Health’s source strings so they work in languages other than English. Health has already been translated to a few languages thanks to the translation team over on https://l10n.gnome.org/module/health/

### Apostrophe [↗](https://gitlab.gnome.org/World/apostrophe)

A distraction free Markdown editor.

[Manuel Genovés](https://matrix.to/#/@somas95:gnome.org) says

> I've released Apostrophe 2.5, which features a new Sepia Mode, and the exported HTML is now responsive thanks to a contribution from Martin Abente. Under the hood some dependencies have been updated, the metadata optimized for [Apps for GNOME](https://apps.gnome.org/), and the repository no longer uses master for the principal branch, but main. Get it [here](https://flathub.org/apps/details/org.gnome.gitlab.somas.Apostrophe).
> ![](eca28a15fc2b0d4795233e742376f4875d019c23.png)

# Third Party Projects

[Guido](https://matrix.to/#/@guido.gunther:talk.puri.sm) reports

> I've released version [0.13.1](https://gitlab.gnome.org/World/Phosh/phosh/-/releases/v0.13.1) of phosh (a graphical shell for mobile devices). It adds a "close all" button for notifications, the feedback quick setting cycles through all feedback profiles and we improved encrypted media mounts and fractional scaling support a bit.

# Documentation

[Julian 🍃](https://matrix.to/#/@julianhofer:gnome.org) says

> I have just published another chapter of *GUI development with Rust and GTK 4*.
> It explains how to create a simple To-Do app step by step.
> Click [here](https://gtk-rs.org/gtk4-rs/stable/latest/book/todo_app_1.html) to read the chapter.
> Thanks a lot to Ivan Molodetskikh , Bilal Elmoussaoui and Sabrina for the reviews.
> {{< video src="25c659fd79d1ac966076d7b84134a7afe4b5f499.webm" >}}

# Miscellaneous

[Sophie Herold](https://matrix.to/#/@sophieherold:gnome.org) reports

> [Apps for GNOME](https://apps.gnome.org) has seen some bug fixes, design updates, and several new translations this week. The most visible change is the new landing page illustration by Jakub Steiner.
> ![](1661997cb983bc747ce949ac7af4d2ea22f0a8cf.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
